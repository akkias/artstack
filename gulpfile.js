var elixir = require('laravel-elixir');
var webpack = require('webpack');
var uglifycss = require('gulp-uglifycss');
var uglify = require('gulp-uglify');
var pump = require('pump');
var cleanCSS = require('gulp-clean-css');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
 elixir(function(mix) {
  //mix.webpack('bootstrap.js');
  mix.copy(
        'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        'resources/assets/js/bs'
    ).copy(
        'node_modules/jquery/dist/jquery.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/masonry-layout/dist/masonry.pkgd.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/sweetalert2/dist/sweetalert2.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/jquery-sticky/jquery.sticky.js',
        'resources/assets/js'
    ).copy(
        'node_modules/moment/min/moment.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/jquery-hashtags/js/jquery.hashtags.min.js',
        'resources/assets/js'
    ).copy(
        'node_modules/autosize/src/autosize.js',
        'resources/assets/js'
    ).copy(
        'node_modules/noty/js/noty/packaged/jquery.noty.packaged.js',
        'resources/assets/js'
    ).scripts([
      "jquery.min.js",
      //"../../../public/js/bootstrap.js",
      "/bs/bootstrap.js",
      'sweetalert2.min.js',
      "jquery.infinitescroll.min.js",
      "masonry.pkgd.min.js",
      "imagesloaded.pkgd.min.js",
      "moment.min.js",
      "autosize.js",
      "jquery.hashtags.min.js",
      "jquery.linky.min.js",
      "jquery.noty.packaged.js",
      "jquery.barrating.min.js",
      "base.js",
      "artstack-common.js"
     ])
    .sass(
      'app.scss'
    );
});
elixir(function(mix) {
    mix.version('css/app.css');
});