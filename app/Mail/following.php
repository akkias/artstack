<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class following extends Mailable
{
    use Queueable, SerializesModels;
    public $to_user;
    public $from_user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $to_user,User $from_user)
    {
        //
        $this->to_user = $to_user;
        $this->from_user = $from_user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('ArtMojo | you got a new follower')->view('emails.following')
        ->with([
            'to_user' => $this->to_user,
            'from_user' => $this->from_user,
            ]);;
    }
}
