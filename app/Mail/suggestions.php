<?php

namespace App\Mail;

use App\Suggestion;
use App\User;
use App\Art;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class suggestions extends Mailable
{
    use Queueable, SerializesModels;
    public $suggestion;
    public $user;
    public $art;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Art $art, Suggestion $suggestion)
    {
        //
        $this->user = $user;
        $this->art = $art;
        $this->suggestion = $suggestion;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('ArtMojo | you have received new suggestion on your art')->view('emails.suggestions')
        ->with([
            'user' => $this->user,
            'art' => $this->art,
            'suggestion' => $this->suggestion,
            ]);
    }
}