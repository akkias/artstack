<?php
use App\Follow;
use App\User;
use App\Like;
use App\Comment;
use App\Activity_map;
use App\Activity;
use App\Notification_map;
use App\Notification;
use App\Reaction_map;
use App\Reaction;
use App\View;
use App\Art;
use Illuminate\Support\Facades\Mail;
use App\Mail\following;
use App\Mail\welcome;
use App\Mail\suggestions;
use App\Suggestion;

function getCommentsByArtId($artId){

}

function nextArtId($currentArtId){
	$art = DB::table('arts')->where('id','>', $currentArtId)->orderBy('id','asc')->first();
	if($art)
		return $art->id;
	return null;
}

function prevArtId($currentArtId){
	$art = DB::table('arts')->where('id','<', $currentArtId)->orderBy('id','desc')->first();
	if($art)
		return $art->id;
	return null;
}

function checkIfAlreadyUpvoted($userid, $upvoteString){
    $arr = explode(",",$upvoteString);
    return in_array($userid , $arr);
}

function getArtStatistics($artId){
        try{
//            $artId = $request->input('artId');
            return View::where("artId", $artId)
              ->selectRaw('country, z COUNT(*) as views, sum(liked) as likes')
              ->groupBy('country')
              ->get();        
        }
        catch(Exception $ex){
            return -1;
        }
    }

function suggestionEmailNotification($artId, $suggestId){
	try{
		$art = Art::find($artId);
		$suggest = Suggestion::find($suggestId);
		$to_user = User::find($art->user_id);

		Mail::to($to_user->email)->queue(new suggestions($to_user, $art, $suggest));

	}
	catch(Exception $ex){
		return;
	}
//		return "mailed";
}

function followEmailNotification($from, $to){
		$to_user = User::find($to);
		$from_user = User::find($from);
		Mail::to($to_user->email)->queue(new following($to_user,$from_user));
}

function isValidUserToDeleteComment($art_id, $comment_id){
	try{
		$user = Auth::user();
		if(!$user)
			return false;
		$comment = Comment::find($comment_id);
		if($comment && $comment->user_id == $user['id'])
			return true;

		$art = Art::find($art_id);
		if($art && $art->user_id == $user['id'])
			return true;
		return false;
	}
	catch(Exception $ex){
		return false;	
	}

}

function isValidUserToDeleteSuggestion($art_id, $suggest_id){
	try{
		$user = Auth::user();
		if(!$user)
			return false;
		$suggestion = Suggestion::find($suggest_id);
		if($suggestion && $suggestion->user_id == $user['id'])
			return true;

		$art = Art::find($art_id);
		if($art && $art->user_id == $user['id'])
			return true;
		return false;
	}
	catch(Exception $ex){
		return false;	
	}

}

function isCookieEnabled(){
	try{
		if(count($_COOKIE) > 0) {
		    return true;
		} else {
		    return false;
		}	
	}
	catch(Exception $ex){
		return false;
	}
}

function setUserCookie(){
	if(isCookieEnabled()){

	}	
}

function getUserCookie(){
	if(isCookieEnabled()){

	}	
}

function isFollowingTo($fromid, $toid){
	try{
		return Follow::where('follower_id', $fromid)->where('following_id', $toid)->count();
	}
	catch(Exception $ex){
		return $ex;
	}
}

function followClick($fromid, $toid){
	try{
		$check = Follow::where('follower_id', $fromid)->where('following_id', $toid)->get();
		if(sizeof($check) == 0){		// doesnt follow
			$follow = new Follow;
			$follow->follower_id = $fromid;
			$follow->following_id = $toid;
			$saveResult = $follow->save();
			if($saveResult){	//return "Successfully followed";
			//	activityLog(3, $fromid);
				//return "push";
				pushNotification($toid, $toid, 3);
				//followEmailNotification($fromid, $toid);
				return 1;
			}
			else{		//return "Could not follow, internal problem";
				return -1;
				//return "Could not follow, internal problem";
			}
		}
		else{		//already follows
			$follow = Follow::find($check[0]->id);
			$follow->delete();
			//activityLog(12, $fromid);
			return 2;
		}
	}
	catch(Exception $ex){
		return -1;
	}
}

function isArtLiked($userid, $artid){
	$checkData = View::where('userId' , $userid)->where('artId', $artid)->get();
	$len = sizeof($checkData);
	if($len > 0){
		return $checkData[0]->liked;
	}
	return false;
}


function isArtBookmarked($userid, $artid){
	$checkData = View::where('userId' , $userid)->where('artId', $artid)->get();
	$len = sizeof($checkData);
	if($len > 0){
		return $checkData[0]->bookmark;
	}
	return false;
}

function likeClick($userid, $artid){
	try{
		//return "like clicked";
		if(isArtLiked($userid, $artid)){
			$checkId = View::where('userId' , $userid)->where('artId', $artid)->pluck('id');
			$view = View::find($checkId);
			$view->liked = false;
			$saved = $view->save();
			if($saved){

				DB::table('arts')->where('id', $artid)->decrement('likes_count');
				try{
				//	activityLog(11, $artid);
					
				}
				catch(Exception $ex){
					return 3;	
				}
				return  2;
			}
			return  0;
		}
		else{
			$checkId = View::where('userId' , $userid)->where('artId', $artid)->pluck('id');
			if(count($checkId) == 0)
				addPageView($userid, $artid);
			$checkId = View::where('userId' , $userid)->where('artId', $artid)->pluck('id');
			$view = View::find($checkId);
			$view->liked = true;
			$saved = $view->save();
			if($saved){
				DB::table('arts')->where('id', $artid)->increment('likes_count');
				$userid = Art::find($artid)->user_id;
				try{
		//			activityLog(1, $artid);
					pushNotification($userid, $artid, 1);
				}
				catch(Exception $ex){
					return $ex;
				}
				return  1;
			}
			return 0;
		}
	}
	catch(Exception $ex){
		return -1;
	}
}

function approveClick($userid, $suggestionid){
	try{

			$suggest = Suggestion::find($suggestionid);
			$suggest->approved = true;
			$suggest->save();
			pushNotification($suggest->user_id, $suggest->art_id, 6);
			return 1;
	}
	catch(Exception $ex){
		return -1;
	}
}



function commentClick($commentText, $artid){
	try{
		$userid = Auth::user()['id'];
		$comment = new Comment;
		$comment->user_id = $userid;
		$comment->art_id = $artid;
		$comment->text = $commentText;
		$saveResult = $comment->save();

		if($saveResult){	//return "Successfully followed";
	//		activityLog(2, $artid);
			$userid = Art::find($artid)->user_id;
			pushNotification($userid, $artid, 2);
			$commentId = Comment::where('user_id', Auth::user()['id'])->where('art_id', $artid)->where('text', $commentText)->orderBy('created_at','desc')->pluck('id')->first();
			DB::table('arts')->where('id', $artid)->increment('comments_count');
			//return Auth::user();
			//return $artid.$userid;
			//return Comment::where('user_id', $userid)->where('art_id', $artid)->get();
			return $commentId;
		}
		else{		//return "Could not follow, internal problem";
			return 0;
		}
	}
	catch(Exception $ex){
		return -1;
	}
}

function addPageView($userid, $artid){
	try{
		$checkCount = View::where('artId', $artid)->where('userId', $userid)->count();
		if($checkCount == 0){
			$view = new View;
			$view->userId = $userid;
			$view->artId = $artid;
			$saveResult = $view->save();
			if($saveResult){	//return "Successfully followed";
				// add activity log
				// add notification
				DB::table('arts')->where('id', $artid)->increment('view_count');
				return 1;
		//		return "view added";
			}
			else{		//return "Could not follow, internal problem";
				return 0;
		//		return "Could not comment on the art, internal problem";
			}
		}
		else{
			return 2;
		}
	}
	catch(Exception $ex){
		return -1;
	}
}


function getProfilePic(){
	try{
		$loggedInBy = Auth::user()['loginBy'];
		if($loggedInBy == "fb" ){
			$size = getimagesize(Auth::user()['fb_pic']);
			return $size ? Auth::user()['fb_pic'] : "/images/avatar.jpg";
		}
		else{
			$size = getimagesize(Auth::user()['g_pic']);
			return $size ? Auth::user()['g_pic'] : "/images/avatar.jpg";
		}
	}
	catch(Exception $ex){
		return -1;	
	}

}



function suggestClick($userid, $artid){
	try{
		$comment = new Comment;
		$comment->user_id = $userid;
		$comment->art_id = $artid;
		$saveResult = $comment->save();
		if($saveResult){	//return "Successfully followed";
			// add activity log
			// add notification
			return 1;
		}
		else{		//return "Could not follow, internal problem";
			return 0;
		}
	
	}
	catch(Exception $ex){
		return -1;
	}
}

function activityLog($activityMapId, $CommonId){		// common id = art or user
	try{
		$name = Auth::user()['name'];
		$userId = Auth::user()['id'];
		$activityMap = Activity_map::find($activityMapId);
		$activity = new Activity;
		$activity->user_id = $userId;
		$activity->activity = $activityMap->type;
		$activity->common_id = $CommonId;
		$activity->save();
		return 1;
		//return $name." ".$activityMap->type;
	}
	catch(Exception $ex){
		return $ex;
	}
}


function reactionClick($userId, $reactionMapId, $artId){		// common id = art or user
	try{
		$reactionMap = Reaction_map::find($reactionMapId);
		$reaction = new Reaction;
		$reaction->user_id = $userId;
		$reaction->art_id = $artId;
		$reaction->reaction = $reactionMap->reaction;
		$reaction->save();
	}
	catch(Exception $ex){

	}
}


function accountViewPay($userId){		// common id = art or user
	try{
		// join Views/Arts table 
		// check if user id and current logged in User Id matches
		// check total view count/ like count / dislike count
		// prepare algorithm/formula to calculate the amount 
		// and return the total amount
	}
	catch(Exception $ex){

	}
}

function isFirstLogIn($userId){		// common id = art or user
	try{
		$userCheck = User::find($userId);
		if($userCheck->visited){
			return false;
		}
		else{
			$userCheck->visited = true;
			$userCheck->save();
			return true;
		}
	}
	catch(Exception $ex){

	}
}

function pushNotification($userId, $commonId, $notificationId){		// common id = art or user
	try{
		$name = Auth::user()['name'];
		$fromId = Auth::user()['id'];
		if($fromId == $commonId)
			return 2;
		$pic = Auth::user()['fb_pic'] != null ? Auth::user()['fb_pic'] : Auth::user()['g_pic']; 
		$notifyExists =  Notification::where('notification_id', $notificationId)->where('from', $fromId)->where('common_id', $commonId)->count();
		if(!$notifyExists){
			$notificationMap = Notification_map::find($notificationId);
			$notification = new Notification;
			$notification->user_id = $userId;
			$notification->user_name = $name;
			$notification->user_pic = $pic;
			$notification->text = $notificationMap->type;
			$notification->common_id = $commonId;
			$notification->from = $fromId;
			$notification->notification_id = $notificationId;
			$notification->link = $notificationId == 3 || $notificationId == 4 ? "profile/".$fromId : "suggest/".$commonId;	
			$notification->read = false;
			$notification->save();
			//return "two";
		}
		return 2;
	}
	catch(Exception $ex){
		return -1;
	}
}

function getNotifications(){		// common id = art or user
	try{
		$notifications = DB::table('notifications')->where('user_id', Auth::user()['id'])->where('read', false)->join('users','users.id', '=','notifications.user_id')->select('notifications.id', 'notifications.user_id', 'users.name', 'notifications.text', 'notifications.common_id', 'notifications.from')->get();
		return $notifications;
	}
	catch(Exception $ex){
		return -1;
	}
}

function userNotificationPresent(){
	try{
		$notificationsPresent = Notification::where('user_id', Auth::user()['id'])->where('read', false)->count();
		if($notificationsPresent)
			return $notificationsPresent;
		return false;
	}
	catch(Exception $ex){
		return false;
	}
}

function sendEmail($userId){		// common id = art or user
	try{
		$user = User::findOrFail($userId);
        Mail::to($user->email)->send(new welcome);
        return 1;
        // replace welcome with the classname 
	}
	catch(Exception $ex){
		return -1;
	}
}

function internalError(){
	return 'Internal server error. Please try again later!';
}


function getProfilePicById($userId){
		try{
			$user = User::find($userId);
		//	$size = getimagesize($user->fb_pic);
			return $user->fb_pic ? $user->fb_pic : $user->g_pic;
		}
		catch(Exception $ex){
			return -1;
		}
		
}

function thousandsCurrencyFormat($num) {
  if($num == 0)
  	return 0;	
  $x = round($num);
  $x_number_format = number_format($x);
  $x_array = explode(',', $x_number_format);
  $x_parts = array('k', 'm', 'b', 't');
  $x_count_parts = count($x_array) - 1;
  $x_display = $x;
  $x_display =  $x_count_parts > 1 ? $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '') : $x_array[0];
  if($x_count_parts == 0)
  	return 	$x_array[0];
  $x_display .= $x_parts[$x_count_parts - 1];
  return $x_display;
}

function getNewArtRating($totalRating, $count, $oldUserRating, $newUserRating){
	//4.9 1 0 3.9 
	if($count == 0)
		return $newUserRating;
	
	if($oldUserRating){
		return ((($totalRating * $count) - $oldUserRating) + $newUserRating) / $count;
	}
	else{
		// user rating for the first time
		return ((($totalRating * $count) + $newUserRating) / ($count + 1));
	}
}

function setArtRating($userid, $artid, $rating){
	try{
		$art = Art::find($artid);
		$viewData = View::where("userId", $userid)->where("artId", $artid)->get();
		//return $viewData;
		if(count($viewData) == 0){
			return -1;
		}
		else{
			$view = View::find($viewData[0]->id);
			$userRating = $view->rating;
			$totalRating = $art->rating;
			$ratingCount = $art->rating_count; 		
			$newRating = getNewArtRating($totalRating, $ratingCount, $userRating, $rating);
			
			$art->rating = $newRating;
			$art->rating_count = $userRating ? $ratingCount  : $ratingCount + 1;
			$art->save();

			$view->rating = $rating;
			$view->save();
			return number_format($newRating,1);
		}
		return 0;

	}
	catch(Exception $ex){
		return -1;		
	}
}