<?php
use App\Follow;
use App\User;
use App\Like;
use App\Comment;
use App\Activity_map;
use App\Activity;
use App\Reaction_map;
use App\Reaction;
use App\Art_view;


function push(){
	return "pushed";
}

function userLogOut(){
//	Auth::logout();
	try {
		return redirect("/");		
	}
	catch(Exception $ex){

	}

}



function followCLick($fromid, $toid){
	try{
		$check = Follow::where('follower_id', $fromid)->where('following_id', $toid)->get();
		if(empty($check)){		// doesnt follow
			$follow = new Follow;
			$follow->follower_id = $fromid;
			$follow->following_id = $toid;
			$saveResult = $follow->save();
			if($saveResult){	//return "Successfully followed";
				// add activity log
				// add notification
			}
			else{		//return "Could not follow, internal problem";
				return "Could not follow, internal problem";
			}
				
		}
		else{		//already follows
			$follow = Follow::find($check->id);
			$follow->delete();
			// add activity log
			//check if succefully deleted
		}


	}
	catch(Exception $ex){

	}
}

function likeClick($userid, $artid){
	try{
		$check = Like::where('user_id' , $userid)->where('art_id', $artid)->get();
		if(empty($check)){		// doesnt follow
			$like = new Like;
			$like->user_id = $fromid;
			$like->art_id = $toid;
			$saveResult = $like->save();
			if($saveResult){	//return "Successfully followed";
				// add activity log
				// add notification
			}
			else{		//return "Could not follow, internal problem";
				return "Could not like the art, internal problem";
			}
		}
		else{		//already follows
			$like = Like::find($check->id);
			$like->delete();
			// add activity log
			//check if succefully deleted
		}
	}
	catch(Exception $ex){

	}
}


function commentClick($userid, $artid){
	try{
		echo "commenting started";
		$comment = new Comment;
		$comment->user_id = $userid;
		$comment->art_id = $artid;
		$saveResult = $comment->save();
		if($saveResult){	//return "Successfully followed";
			// add activity log
			// add notification
		}
		else{		//return "Could not follow, internal problem";
			return "Could not comment on the art, internal problem";
		}
		echo "commenting done";
	
	}
	catch(Exception $ex){
		echo "commenting exception";

	}
}


function suggestClick($userid, $artid){
	try{
		$comment = new Comment;
		$comment->user_id = $userid;
		$comment->art_id = $artid;
		$saveResult = $comment->save();
		if($saveResult){	//return "Successfully followed";
			// add activity log
			// add notification
		}
		else{		//return "Could not follow, internal problem";
			return "Could not comment on the art, internal problem";
		}
	
	}
	catch(Exception $ex){

	}
}

function activityLog($userId, $activityMapId, $CommonId){		// common id = art or user
	try{
		$activityMap = Activity_map::find($activityMapId);
		$activity = new Activity;
		$activity->user_id = $userId;
		$activity->activity = $activityMap->type;
		$activity->common_id = $CommonId;
		$acivity->save();
	}
	catch(Exception $ex){

	}
}


function reactionClick($userId, $reactionMapId, $artId){		// common id = art or user
	try{
		$reactionMap = Reaction_map::find($reactionMapId);
		$reaction = new Reaction;
		$reaction->user_id = $userId;
		$reaction->art_id = $artId;
		$reaction->reaction = $reactionMap->reaction;
		$reaction->save();
	}
	catch(Exception $ex){

	}
}


function isFirstLogIn($userId){		// common id = art or user
	try{
		$userCheck = User::find($userId);
		if($userCheck->visited){
			return false;
		}
		else{
			$userCheck->visited = true;
			$userCheck->save();
			return true;
		}
	}
	catch(Exception $ex){

	}
}

