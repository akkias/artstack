<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Socialite;
use App\User;
use App\Notification;
use Auth;

class SocialAuthController extends Controller
{
    public function redirectGoogle()
    {
      // $loginUser = User::find(2);
      // Auth::login($loginUser);
      // return redirect("/");
      return Socialite::driver('google')->redirect();
    }

    public function callbackGoogle()
    {
        try { 
          $authUser = Socialite::with('google')->stateless()->user();
          if(empty($authUser)){
             return redirect("/");
            // pass message about No user data from oauth 
          }
          else{
            $user = User::where("email", $authUser->email)->get(); 
            $newUser = false;
            $oldUser = false;
            if($user->count() > 0){   //user already present
              $oldUser = true;
              $loginUser = User::find($user[0]->id);
              $loginUser->loginBy = "g";
              if(empty($user[0]->fg_id)){
                $loginUser->g_id = $authUser->id;
                $loginUser->g_pic = isset($authUser->avatar) ? $authUser->avatar : "images/avatar.png";
                $loginUser->loginBy = "g";
                $loginUser->save();
                $saveResult = $loginUser->save();   
              }
              $loginUser->save();
              Auth::login($loginUser);
            }
            else{
                $newUser = true;
                $User = new User;
                $User->g_id = $authUser->id;
                $User->g_pic = isset($authUser->avatar) ? $authUser->avatar : "images/avatar.png";
                $User->name = $authUser->name;
                $User->email = $authUser->email;
                $User->gender = isset($authUser->user['gender']) ? $authUser->user['gender'] : null;
                $saved = $User->save();
                if($saved){
                  $savedUser = User::where("email", $authUser->email)->get();
                  $passedUser = User::find($savedUser[0]->id);
                  $passedUser->loginBy = "g";
                  $passedUser->save();
                  Auth::login($passedUser);
                }
                else{
                  return redirect("/");
                  // pass message about not able to save the usesr data  
                }
            }
            return redirect("/oauthLoginSuccess");
          }
        } 
        catch(RequestException $e) {
          //$response = $e->getResponse()->json(); //Get error response body
          return redirect("/");
          // pass message about not able to save the usesr data 
        }
        catch(Exception $e){
            return redirect("/");
            // pass message about not able to save the usesr data 
        }
  }

    public function redirect()
    {
          //   $loginUser = User::find(2);
          // Auth::login($loginUser);
          // return redirect("/");
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {
              try { 
              $authUser = Socialite::with('facebook')->stateless()->user();
              if(empty($authUser)){
                 return redirect("/");
            // pass message about No user data from oauth  
              }
              else{
                $user = User::where("email", $authUser->email)->get(); 
                $newUser = false;
                $oldUser = false;
                if($user->count() > 0){   //user already present
                  $oldUser = true;
                  $loginUser = User::find($user[0]->id);
                  $loginUser->loginBy = "fb";
                  if(empty($user[0]->f_id)){
                    $loginUser->fb_id = $authUser->id;
                    $loginUser->fb_pic = isset($authUser->avatar) ? $authUser->avatar : "images/fav.png";
                  }            
                  $loginUser->save();      
                  Auth::login($loginUser);
                }
                else{
                    $newUser = true;
                    $User = new User;
                    $User->fb_id = $authUser->id;
                    $User->fb_pic = isset($authUser->avatar) ? $authUser->avatar : "images/fav.png";
                    $User->name = $authUser->name;
                    $User->email = $authUser->email;
                    $User->gender = null;
                    $saved = $User->save();
                    if($saved){
                      $savedUser = User::where("email", $authUser->email)->get();
                      $passedUser = User::find($savedUser[0]->id);
                      $passedUser->loginBy = "fb";
                      $passedUser->save();
                      Auth::login($passedUser);
                    }
                    else{
                       return redirect("/");
                  // pass message about not able to save the usesr data   
                    }
                }
                return redirect("/oauthLoginSuccess");
              }
        } 
        catch(RequestException $e) {
          //$response = $e->getResponse()->json(); //Get error response body
          return redirect("/");
          // pass message about not able to save the usesr data 
        }
        catch(Exception $e){
            return redirect("/");
            // pass message about not able to save the usesr data 
        }
        // when facebook call us a with token
    }
}
