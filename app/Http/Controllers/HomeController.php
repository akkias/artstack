<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\suggestions;
use Log;
use Route;
use Mail;
use Location;
use Auth;
use DB;
use App\View;
use App\Art;
use App\User;
use App\Category_map;
use Carbon\Carbon;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    } 

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::check())
            return view('/explore');
        return redirect("/");
    }
    public function how()
    {
        return view('how');
    }
    public function terms()
    {
        return view('terms');
    }
    public function privacy()
    {
        return view('privacy');
    }
    public function features()
    {
        return view('features');
    }
    public function home()
    {
        if(Auth::check())
            return redirect("/explore");
       return view('home');
    }

    public function afterOauthLoginSuccess(){
        return view("oauth");
    }

    public function generateSiteMap(){
        $sm = Sitemap::create();
        $sm->add(url("/"));
        $sm->add(url("/home"));
        $sm->add(url("/explore"));
        $sm->add(url("/trending"));
        $sm->add(url("/features"));
        $sm->add(url("/privacy-policy"));
        $sm->add(url("/quest"));

        $allArts = Art::all();
        for($i = 0 ; $i < $allArts->count(); $i++){
            $sm->add(url("suggest/".$allArts[$i]->id));
        }
        $allUsers = User::all();
        for($i = 0 ; $i < $allUsers->count(); $i++){
            $sm->add(url("profile/".$allUsers[$i]->id));
        }

       //$sm->writeToFile("/sitemaps/sitemap.xml");
       $sm->writeToFile(public_path()."/sitemaps/sitemap.xml");

    }

    public function showCountries(){

        return View::where("artId", 1)
  ->selectRaw('country,  COUNT(*) as count, sum(liked) as likes')
  ->groupBy('country')
  ->get();
    }

    public function showLandingPage($category = null){
        try{
            $allArts = ArtController::getLandingPageArts();
            return view('index', ['arts' => $allArts]);            
        }
        catch(Exception $ex){
            return redirect("/");
        }
    }

    public function trending($category = null){
        try{
            $allArts = ArtController::getTrendingArts();
            $unreadNotifications = ProfileController::getCurrentUserUnreadNotifications();

            //Mail::to('akshaysonawaane@gmail.com')->queue(new suggestions(1));
            return view('index', ['arts' => $allArts]);
        }
        catch(Exception $ex){
            return redirect("/");
        }
    }
}
