<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Art;
use App\View;
use DB;
use App\Comment;
use App\Suggestion;
use App\Category_map;
use Auth;
use Laravel\Scout\Searchable;

class ArtController extends Controller
{
    //
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function showCategoryPage($category = null){
        try{
            if($category == null){
                return redirect("/");
            }
            $check = Category_map::where("category", $category)->count();
            if(!$check){
                return redirect("/");
            }
            else{
                $allArts = $this->getCategoryPageArts($category);
                return view('index', ['arts' => $allArts]);
            }
        }
        catch(Exception $ex){
            return redirect("/");
        }
    }

    public function postLikeArt(Request $request){
        // save the Art data
        try{
            $loggedInUserId = Auth::user()['id']; 
            $artid = $request->input('artId'); 
            $ret = likeClick($loggedInUserId, $artid);
            $userId = Art::find($artid)['user_id'];

            $userData = DB::table('users')->where('id', $loggedInUserId)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();
            $pusher = App::make('pusher');
            if($loggedInUserId != $userId && $ret == 1) {
                $pusher->trigger( 'notifications-'.$userId,
                  'like',
                  array('text' => 'liked your art','userdata' => $userData, 'artId' => $artid));
            }
            if($ret){
               //return activityLog(1, $artid);
              // pushNotification($userId, $artid, 1);
            }
            return $ret;
        }
        catch(Exception $ex){
            return -1;
        }
    }
    
    public function postSuggestionApprove(Request $request){
        try{
            $loggedInUserId = Auth::user()['id']; 
            $artid = $request->input('artId'); 
            $suggestionid = $request->input('suggestionId'); 
            $userId = Art::find($artid)['user_id'];
            $ret = approveClick($loggedInUserId, $suggestionid);
            //$userId = Art::find($artid)['user_id'];
            if($ret){
               //return activityLog(1, $artid);
              // pushNotification($userId, $artid, 1);
                $userData = DB::table('users')->where('id', $userId)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();
                $pusher = App::make('pusher');
                $pusher->trigger( 'notifications-'.$userId,
                  'approvedRating',
                  array('text' => 'has approved and upvoted your suggestion','userdata' => $userData, 'artId' => $artid));


            }
            return $ret;
        }
        catch(Exception $ex){
            return -1;
        }
    }


    public function postCommentArt(Request $request){
        try{
            $loggedInUserId = Auth::user()['id']; 
            $commentArea = $request->input('commentArea');
            $artId = $request->input('artId'); 
            $ret = commentClick($commentArea, $artId);
            $userId = Art::find($artId)['user_id'];

            $userData = DB::table('users')->where('id', $loggedInUserId)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();

            if($ret > 0){
                pushNotification($userId, $artId, 2);

                $pusher = App::make('pusher');
                if($loggedInUserId != $userId) {   
                    $pusher->trigger( 'notifications-'.$userId,
                      'comment',
                      array('text' => 'commented on your art','userdata' => $userData, 'artId' => $artId));
                }
                return '<div class="comment" id = "commentDiv'.$ret.'">
                <div class="avatar">
                    <img class="img-circle" src="'.getProfilePic().'" alt="" height="24" width = "24">       
                </div>
                <div class="comment-data">
                    <p><a href="">'.Auth::user()['name'].'</a>
                        <small class="text-muted comment-timestamp">just now</small></p>'.nl2br(e($commentArea)).'
                    </div>
                    <button class="delete" onClick ="commentDeleteClick('.$ret.')"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg></button>
                </div>';
               // activityLog(2, $artid);
            }
            return $ret."ret";
        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function postArtRating(Request $request){
        try{
            $userid = Auth::user()['id'];  
            $artid = $request->input('artId');
            $artUserId = Art::find($artid)['user_id'];
            $rating = $request->input('rating');
            //return $userid.$artid.$rating;
            $userData = DB::table('users')->where('id', $userid)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();
            $pusher = App::make('pusher');
            if($userid != $artUserId) {   
                $pusher->trigger( 'notifications-'.$artUserId,
                  'rating',
                  array('text' => 'rated your art','userdata' => $userData, 'artId' => $artid));
            }
            return setArtRating($userid, $artid, $rating);
        }
        catch(Exception $ex){
            return -1;
        }
    }

    

    public function postArtStatistics(Request $request){
        try{
            $artId = $request->input('artId');
            return View::where("artId", $artId)
            ->selectRaw('country,  COUNT(*) as count, sum(liked) as likes')
            ->groupBy('country')
            ->get();        
        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function getCountry($address_components){
        $country = null;
        foreach($address_components as $component){
            if($component["types"][0] == "country"){
                $country = $component["long_name"] ;
                break;
            }
        }
        return $country;
    }

    public function isPageViewAdded($artid, $userid){
        return View::where('artId', $artid)->where('userId', $userid)->count();
    }

    public function addArtViewLocation(Request $request){
        try{
            if(!Auth::user())
                return 3;   // user not logged in

            $string = $request->input('suggestArtId');
            $output = explode("/",$string);

            $suggestArtId = $output[count($output)-1];
            $latitude = $request->input('Latitude');
            $longitude = $request->input('Longitude');


            if(!$suggestArtId)
                return 4; // art id not loaded and its null


            if(!$this->isPageViewAdded($suggestArtId, Auth::user()['id'])){
                return 0;
            }
            else{
                return $this->addCountry($latitude, $longitude, $suggestArtId);
                //return 1;
            }

        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function addCountry($lat, $lng, $suggestArtId){
       //return "hh".$suggestArtId;
        $contentsInArr=json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=".$lat.",".$lng),true);
        $resultPresent = $contentsInArr["results"] != [] ? true : false;

        if(!$resultPresent)
            return 0;

        $address_components = $contentsInArr["results"][1]["address_components"];
        $country = $this->getCountry($address_components);
        if(!$country)
            return 0;

        $viewData = View::where('artId', $suggestArtId)->where('userId', Auth::user()['id'])->pluck("id");


        $viewId = $viewData == [] ? null : $viewData[0];
        if(!$viewId)
           return 0; 
       $view = View::find($viewId);
       $view->country = $country;
       $view->save();
       return 1;
        //$formatted_address = $contentsInArr["results"][1]["formatted_address"];

   }

   public function postCreateArt(Request $request){
    try{
        $postMode = $request->input('postMode');
        $art = "";
        if($postMode == "edit"){
            $art = Art::find($request->input('artId'));
        }
        else{
            $art = new Art;
        }
        $art->images = $request->input('imageUrl');
        $art->image_md_url = $request->input('imageMDUrl');
        $art->image_lg_url = $request->input('imageLGUrl');
        $art->user_id = Auth::user()['id'];
        $art->description = $request->input('artDesc');
            //$art->category = $request->input('artType');
        $art->nsfw = $request->input('artNSFW') == "on" ? true : false;
        $art->enableSuggestions = $request->input('enableSuggestions') == "on" ? true : false;
        $art->suggestions = false;
        $artSaveCheck = $art->save();
        if($artSaveCheck){
               // activityLog(7, $artid);
            $pusher = App::make('pusher');
            $pusher->trigger( 'postNotificationGlobal', 'postNotification', array('text' => 'View new art'));

            return  "/suggest/".Art::where('user_id' , Auth::user()['id'])->orderBy('created_at', 'desc')->first()->id;
        }
        else{
            return  0;
        }
    }
    catch(Exception $ex){
        return -1;
    }
}

public static function getCategoryPageArts($category){
    $categoryMapId = Category_map::where("category", $category)->pluck('id');
    $allArts = DB::table('arts')->where('category', $categoryMapId)->join('users','users.id', '=','arts.user_id')->select('arts.id', 'arts.user_id','arts.rating', 'users.name','users.g_pic','users.fb_pic','users.loginBy', 'arts.images', 'arts.description', 'arts.category', 'arts.likes_count', 'arts.comments_count','arts.view_count','arts.created_at','arts.nsfw')->orderBy('arts.created_at', 'DESC')->paginate(40);;
    return $allArts;
}

public static function getLandingPageArts(){
    try{
        $allArts = DB::table('arts')->join('users','users.id', '=','arts.user_id')->select('arts.id','arts.rating' ,'arts.user_id', 'users.name','users.g_pic','users.fb_pic','users.loginBy', 'arts.images', 'arts.description', 'arts.category', 'arts.likes_count','arts.suggest_count', 'arts.view_count','arts.comments_count','arts.created_at','arts.nsfw')->orderBy('arts.created_at', 'DESC')->paginate(40);;
        return $allArts;
    }
    catch(Exception $ex){
        return [];
            // return 'Could not fetch arts, internal server error. Please try again!'
    }
}

public static function getTrendingArts(){
    try{
        $allArts = DB::table('arts')->join('users','users.id', '=','arts.user_id')->select('arts.id','arts.rating', 'arts.user_id', 'users.name','users.g_pic','users.fb_pic','users.loginBy', 'arts.images', 'arts.description', 'arts.category', 'arts.likes_count', 'arts.view_count','arts.comments_count','arts.created_at','arts.nsfw')->orderBy('arts.view_count', 'DESC')->paginate(40);
        return $allArts;
    }
    catch(Exception $ex){
        return [];
            // return 'Could not fetch arts, internal server error. Please try again!'
    }
}



public function getUserProfileArts($userId){
    try{
        $allArts = Art::where('user_id', $userId)->get();
        return $allArts;
    }
    catch(Exception $ex){
        return [];
            // return 'Internal server error. Please try again later!'
    }   
}

public function getArtData($artId){
    try{
        $artData = Art::find($artId);
        return $artData;
    }
    catch(Exception $ex){
        return null;
    }
}

public function getCategoryArts($category){
    try{
        $categoryArts = Art::where('category', $category)->get();
        return $categoryArts;
    }
    catch(Exception $ex){
        return [];
            // return internalError();
    }
}

public function showCreateArtPage(){
    return view('artwork.create');
}

public function showViewArtPage($pageId){
    try{
        $artData = DB::table('arts')->where('arts.id', $pageId)->join('users','users.id', '=','arts.user_id')->select('arts.id', 'arts.user_id', 'users.name', 'arts.images', 'arts.description',  'arts.category', 'arts.likes_count', 'arts.view_count', 'users.g_pic', 'arts.created_at','arts.nsfw','arts.enableSuggestions')->orderBy('arts.created_at', 'DESC')->paginate(40);
        $ret = addPageView(Auth::user()['id'], $pageId);
        $comments = DB::table('comments')->where('art_id', $pageId)->join('users','users.id', '=','comments.user_id' )->select('comments.id', 'comments.user_id', 'users.name', 'users.fb_pic', 'users.g_pic', 'comments.text',  'comments.created_at')->orderBy('comments.created_at', 'ASC')->get();;
        $profilePic = getProfilePic();
        $loginBy = Auth::user()['loginBy'];

        return view('artwork.index', [ 'loginBy' => $loginBy , 'art' => $artData[0], 'comments' => $comments, 'profilePic' => $profilePic]);
    }
    catch(Exception $ex){
       return null; 
   }
}

public function showEditArtPage($pageId){
    try{
        $artData = DB::table('arts')->where('arts.id', $pageId)->join('users','users.id', '=','arts.user_id')->select('arts.id', 'arts.user_id', 'users.name', 'arts.images', 'arts.nsfw', 'arts.description',  'arts.category', 'arts.likes_count', 'arts.view_count', 'users.g_pic', 'arts.created_at','arts.nsfw','arts.enableSuggestions')->orderBy('arts.created_at', 'DESC')->paginate(40);
        if(Auth::user())
            $ret = addPageView(Auth::user()['id'], $pageId);
        $comments = DB::table('comments')->where('art_id', $pageId)->join('users','users.id', '=','comments.user_id' )->select('comments.id', 'comments.user_id', 'users.name', 'users.fb_pic', 'users.g_pic', 'comments.text',  'comments.created_at')->orderBy('comments.created_at', 'ASC')->get();;
        $profilePic = getProfilePic();
        $loginBy = Auth::user()['loginBy'];
            //return $artData;
        return view('artwork.edit', [ 'loginBy' => $loginBy , 'art' => $artData[0], 'comments' => $comments, 'profilePic' => $profilePic]);
    }
    catch(Exception $ex){
        return null;
    }
}


public function showSuggestArtPage($pageId){
    try{

        $art = Art::find($pageId);
        if(!$art)
            return redirect("/");

        $artData = DB::table('arts')->where('arts.id', $pageId)->join('users','users.id', '=','arts.user_id')->select('arts.id', 'arts.user_id', 'users.name', 'arts.images','arts.rating_count', 'arts.rating','arts.image_md_url', 'arts.description',  'arts.category', 'arts.likes_count', 'arts.view_count', 'users.g_pic', 'arts.created_at','arts.nsfw','arts.enableSuggestions')->orderBy('arts.created_at', 'DESC')->paginate(40);
        $ret = null;
        if(Auth::user())
            $ret = addPageView(Auth::user()['id'], $pageId);
        $comments = DB::table('comments')->where('art_id', $pageId)->join('users','users.id', '=','comments.user_id' )->select('comments.id', 'comments.user_id', 'users.name', 'users.fb_pic', 'users.g_pic', 'comments.text',  'comments.created_at')->orderBy('comments.created_at', 'ASC')->get();;
        $profilePic = getProfilePic();
        $loginBy = Auth::user()['loginBy'];
        $viewRating = 0;
        if($ret == 1 || $ret == 2){
            $rating = View::where("userId",Auth::user()['id'])->where("artId", $pageId)->pluck("rating");
            $viewRating = $rating ? $rating[0] : 0;
        }
        $suggests = DB::table('suggestions')->where('art_id', $pageId)->join('users','users.id', '=','suggestions.user_id' )->select('suggestions.id', 'suggestions.user_id', 'users.name', 'users.fb_pic', 'users.g_pic',  'suggestions.text', 'suggestions.x', 'suggestions.y', 'suggestions.art_id',  'suggestions.created_at', 'suggestions.approved', 'suggestions.upvotes', 'suggestions.upvoteCount')->get();;
        return view('artwork.suggest', ['suggests' => $suggests , "rating" => $viewRating,'loginBy' => $loginBy , 'art' => $artData[0], 'comments' => $comments, 'profilePic' => $profilePic]);
    }
    catch(Exception $ex){
        return null;
    }
}

public function getArtComments($artId){
    try{
        $comments = Comment::where('art_id', $artId)->get();
        return $comments;
    }
    catch(Exception $ex){
        return [];
    }
}

public function postDeleteArtComment(Request $request){
    try{
        $commentId = $request->input('commentId');
        $commentToDelete = Comment::find($commentId);
        $delCheck = $commentToDelete->delete();
        if($delCheck){
            DB::table('arts')->where('id', $commentToDelete->art_id)->decrement('comments_count');
        }
        return $delCheck == 0 ? 0 : 1;
    }
    catch(Exception $ex){
            // return internalError();  
        return -1;
    }
    return 0;
}

public function postDeleteArtSuggest(Request $request){
    try{
        $suggestId = $request->input('suggestId');
        $suggestToDelete = Suggestion::find($suggestId);
        $delCheck = $suggestToDelete->delete();
        if($delCheck){
            $art = Art::find($suggestToDelete->art_id);
            $art->decrement('suggest_count');
            $art->save();
            //DB::table('arts')->where('id', $commentToDelete->art_id)->decrement('comments_count');
        }
        return $delCheck == 0 ? 0 : 1;
    }
    catch(Exception $ex){
            // return internalError();  
        return -1;
    }
    return 0;
}


public function postDeleteArt(Request $request){
    try{
        $artId = $request->input('artId');
        $artToDelete = Art::find($artId);
        $delCheck = $artToDelete->delete();
        return $delCheck == 0 ? 0 : 1;
    }
    catch(Exception $ex){
            // return internalError();  
        return -1;
    }
    return 0;
    
}


public function postUpvoteSuggestion(Request $request){
        try{

            $loggedInUserId = Auth::user()['id']; 
            $suggestId = $request->input('suggestId');
            $suggest = Suggestion::find($suggestId);
            if(!$suggest)
                return -1;        
            $upvotes = $suggest->upvotes;
            $check = checkIfAlreadyUpvoted($loggedInUserId, $upvotes);
            if($check){
                return 2;
            }
            else{
                if($upvotes == null){
                    $upvotes = $loggedInUserId;
                    $suggest->upvoteCount = 1;    
                }
                else{
                    $upvotes = $upvotes.",".$loggedInUserId;    
                    $suggest->increment('upvoteCount');            
                }
                $suggest->upvotes =  $upvotes;
                return $suggest->save() ? 1 : -1;
            }
        }
        catch(Exception $ex){
            return -1;
        }

    }


public function postArtSuggestion(Request $request){
    try{
        $artId = $request->input('artId');
        $x = $request->input('x');
        $y = $request->input('y');
        $suggestion = $request->input('text');
        $suggest = new Suggestion;
        $suggest->art_id = $artId;
        $suggest->x = $x;
        $suggest->text = $suggestion;
        $suggest->y = $y;
        $suggest->user_id = Auth::user()['id'];
        $check = $suggest->save();
        $ret = Suggestion::where('user_id', Auth::user()['id'])->where('art_id', $artId)->where('text', $suggestion)->orderBy('created_at','desc')->pluck('id')->first();
            //$artToDelete = Art::find($artId);
            //$delCheck = $artToDelete->delete();

        if($check){
            try{
                suggestionEmailNotification($artId,$ret);
            $userData = DB::table('users')->where('id', $suggest->user_id)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();
            $pusher = App::make('pusher');
            $pusher->trigger( 'notifications-'.Art::find($artId)->user_id,
              'feedback',
              array('text' => 'has given feedback on your art','userdata' => $userData, 'artId' => $artId, 'suggestion' => $suggestion));
            pushNotification(Art::find($artId)->user_id, $artId, 5);
            }
            catch(Exception $ex){
            }
                $art = Art::find($artId);
                $art->increment('suggest_count');
                $art->save();

            return '<div class="comment" id = "suggestDiv'.$ret.'" onmouseover ="artSuggestClick('.$suggest->x.",".$suggest->y.')">
            <div class="avatar">
                <img class="img-circle" src="'.getProfilePic().'" alt="" height="24" width = "24">       
            </div>
            <div class="comment-data">
                <p><a href="">'.Auth::user()['name'].'</a>'."&nbsp;<small class='text-muted'>just now</small></p>".nl2br(e($suggestion)).'
            </div>
            <button class="delete" onClick ="suggestDeleteClick('.$ret.')"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg></button>
        <div class="comment-actions suggest-approve-section">

<span id = "upvoteSpan'.$ret.'" hidden>
                                                            <svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
                                                            C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
                                                            c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
                                                            c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
                                                            C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
                                                            <path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
                                                            s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
                                                            <small class="is-upvoted">
                                                                <span id = "upvoteSpanCount'.$ret.'">0</span>
                                                            </small>
                                                    </span>

<a href="#" id = "upvote'.$ret.'" onclick = "upvoteSuggestionClick('.$ret.')"> 
                                                        <svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                                                            <path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
                                                            C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
                                                            c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
                                                            c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
                                                            C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
                                                            <path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
                                                            s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
                                                        <small class="">
                                                                <span id = "upvoteSpanCount'.$ret.'">0</span>
                                                            </small>&nbsp;      
                                                    <small class="middle">Upvote</small>
                                                </a>
                                                </div></div>
        ';
    }
    return 0;
}
catch(Exception $ex){
    return -1;
}
return 0;
}
}
