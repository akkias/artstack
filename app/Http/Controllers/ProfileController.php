<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Mail\following;
use Auth;
use Mail;
use DB;
use App\User;
use App\Suggestion;
use App\Art;
use App\Setting;
use App\Notification;
use App\Follow;

class ProfileController extends Controller
{
    //
    public function __construct()
    {
  //      $this->middleware('auth');
    }

    function getUserData($userId){
        $user = User::find($userId);
        if(!$user)
            return null;        

        $profileName = $user->name;
        $profilePic = getProfilePicById($userId);
        $following = Follow::where('follower_id', $userId)->count();
        $followers = Follow::where('following_id', $userId)->count();
        $arts = Art::where('user_id', $userId)->get();
        $views = $arts->sum('view_count');
        $likes = $arts->sum('likes_count');
        $approvedCount = Suggestion::where('user_id', $userId)->where('approved' , true)->get();
        $posts = $arts->count();

        return [ $profileName, $profilePic , $following, $followers, $views, $likes , $approvedCount, $posts, $userId];
        
    }

    public function showProfilePage($id = null, $followType = null){
        // get user id as a param and get tha data fro the same and pass it to the blade
       //return view('profile.followers');
        $user = null;
        $showFollowing = false;$showFollowers = false;
        if($id == null && $followType == null){
            return redirect()->back();
        }
        else if($id != null && $followType == null){
            $user = User::find($id);
            if(!$user)
                return redirect()->back();
        }
        else if($id == null && $followType != null){
            return redirect()->back();
        }
        else if($id != null && $followType != null){
            $user = User::find($id);
            if(!$user)
                return redirect()->back();

            if($followType == "following")
                $showFollowing = true;
            else if($followType == "followers")
                $showFollowers = true;
            else
                return redirect()->back();                
        }   
        else{
            return redirect()->back();
        }

        if(!$user)
             return redirect()->back();   

        $userProfileData =  $this->getUserData($id); 
        
        if(!$userProfileData)
             return redirect()->back();   

        $profileName = $userProfileData[0];
        $profilePic = $userProfileData[1];
        $following = $userProfileData[2];
        $followers = $userProfileData[3];
        $views = $userProfileData[4];
        $likes = $userProfileData[5];
        $approvedCount = $userProfileData[6];
        $posts = $userProfileData[7];
        $arts = Art::where('user_id', $id)->get();;

        if($showFollowing){
            $followIds = Follow::where('follower_id', $id)->select('following_id')->get();
            $totalCount = count($followIds);
            $followData = [];
            for($i = 0 ; $i < $totalCount; $i++){
                array_push($followData, $this->getUserData($followIds[$i]->following_id));
            }
            return view('profile.followers', ["userId" => $id,"likes" => $likes,"arts" => $arts,"profileName" => $profileName, "posts" => $posts, "followers" => $followers, "following" => $following, "apporvedSuggestionsCount" => count($approvedCount) ,"views" => $views, "profilePic" => str_replace("type=normal", "type=large",$profilePic), "followData" => $followData]);
        }

        if($showFollowers){
            $followIds = Follow::where('following_id', $id)->select('follower_id')->get();
//            return $followIds;
            $totalCount = count($followIds);
            $followData = [];
           
            for($i = 0 ; $i < $totalCount; $i++){
                array_push($followData, $this->getUserData($followIds[$i]->follower_id));
            }
           
            return view('profile.followers', ["userId" => $id,"likes" => $likes,"arts" => $arts,"profileName" => $profileName, "posts" => $posts, "followers" => $followers, "following" => $following, "apporvedSuggestionsCount" => count($approvedCount) ,"views" => $views, "profilePic" => str_replace("type=normal", "type=large",$profilePic), "followData" => $followData]);
        }

        return view('profile.index', ["userId" => $id,"likes" => $likes,"arts" => $arts,"profileName" => $profileName, "posts" => $posts, "followers" => $followers, "following" => $following, "apporvedSuggestionsCount" => count($approvedCount) ,"views" => $views, "profilePic" => str_replace("type=normal", "type=large",$profilePic)]);
    }

    public function postUpdateSettings(Request $request){
        // get user id as a param and get tha data fro the same and pass it to the blade
         try{
           // return $request->input('nsfw') == "on" ? 'true' : 'false';
            $setting = new Setting;
            $setting->user_id = Auth::user()['id'];
            $setting->nsfw = $request->input('nsfw');
            $setting->notifications = $request->input('notifications');
            $settingsSaveCheck = $setting->save();
            if($artSaveCheck){
                return  "Settings saved successfully";
            }
            else{
                return  "Could not save the settings, Please try again!";
            }
        }
        catch(Exception $ex){
            // return internalError();  
        }
    }

    public function userLogOut(){
        Auth::logout();
        //redirectBack();
    }

    public static function userLogIn(){
        $user = User::find(1);
        Auth::login($user, true);
        return redirect()->Back();
    }

    public function getUserProfile($userId){

    }

    public function getUserSettings($userId){
        try{
            $setting = Setting::where('user_id', Auth::user()['id'])->get();
            return $setting[0];
        }
        catch(Exception $ex){
            // return internalError();  
        }
    }

    // public function getUserFollowers($userId){

    // }

    // public function getUserFollowing($userId){

    // }

    public function postFollowUser(Request $request){
        try{
            $fromId = Auth::user()['id'];
            $toId = $request->input('to'); 
            $ret = followClick($fromId, $toId);

            $userData = DB::table('users')->where('id', $fromId)->select('users.id','users.name','users.fb_pic','users.g_pic','users.name')->get();
            $pusher = App::make('pusher');
            $pusher->trigger( 'notifications-'.$toId,
                      'feedback',
                      array('text' => 'has started following you','userdata' => $userData));
            return $ret;
        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function getCurrentUsersAllNotifications(){
        try{
            //Auth::User()['id']
            $notifications = Notification::where('user_id', Auth::user()['id'])->latest()->get();
            Notification::where('user_id', Auth::user()['id'])->where('read', false)->update(['read' => true]);
            return $notifications;
        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function postReadAllNotifications(Request $request){
        try{
            $returnValue = 1;
            $notifications = getNotifications();
            for($i = 0 ; $i < sizeof($notifications); $i++){
                $readCheck = $this->postReadNotification($notifications[$i]->id);
                if(!$readCheck){
                    $returnValue = 0;                        
                }
            } 
            return $returnValue;
        }
        catch(Exception $ex){
            return -1;
        }
    }

    public function postReadNotification($notifId){
        try{
            //Auth::User()['id']
            $notification = Notification::find($notifId);
            $notification->read = true;
            $res = $notification->save();
            //return $res;
        }
        catch(Exception $ex){
            return -1;
        }   
    }

    public static function getCurrentUserUnreadNotifications(){
        try{
            //Auth::User()['id']
            $notifications = Notification::where('user_id', 1)->where('read', false)->get();
            return $notifications;
        }
        catch(Exception $ex){
            return -1;
            // return internalError();  
        }
    }

    public function getCurrentUserActivityLog(){

    }

}
