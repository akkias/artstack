<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;
use Laravel\Scout\Searchable;

class Art extends Model
{
    //
	use Searchable;

	public function toSearchableArray()
	{
		$array = $this->toArray();
		return $array;
	}
}
