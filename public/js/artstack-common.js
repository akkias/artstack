artUploaded = false;

function editArt(id){
  window.location = "/edit/art/"+id;
}

function deleteArt(artId){
  var confirmed = confirm("Are you sure you want to delete this post?")
  if(confirmed){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    var formData = new FormData();
    formData.append('artId', artId);

    $.ajax({
      url: "/deleteArt",
      type: "POST",
      data:  formData,
      contentType: false,
      cache: true,
      processData:false,
      success: function(data){
        alert(data);
        if(data == 1){
          alert("Art deleted succesfully");
          window.location = "/";
        }
        else {

        }
      },
      error: function(){

      }           
    });
  }
  else{

  }
  
}



function textUserFollowClick(toId,toName){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('to', toId);
  $.ajax({
    url: "/followUser",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        $("#follow"+toId).addClass("ed").removeClass('btn-twitter');
        $("#follow"+toId).html("Following");
        noty({
          layout      : "topRight",
          text        : "You have started following "+toName,
          type        : 'success',
          theme       : 'relax',
          dismissQueue: true,
          animation   : {
            open  : 'animated bounceInRight',
            close : 'animated bounceOutRight'
          }
        });
      }
      else if(data == 2){
        $("#follow"+toId).removeClass("ed").addClass('btn-twitter');
        $("#follow"+toId).html("Follow");
        noty({
          layout      : "topRight",
          text        : "You have unfollowed "+toName,
          type        : 'warning',
          theme       : 'relax',
          dismissQueue: true,
          animation   : {
            open  : 'animated bounceInRight',
            close : 'animated bounceOutRight'
          }
        });
      }
      else{

      }

    }
    ,
    error: function(){
      noty({
        layout      : "topRight",
        text        : "Error occured",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }           
  });

}

function userFollowClickIcon(toId,toName){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('to', toId);
  $.ajax({
    url: "/followUser",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        $(".follow"+toId).addClass("hidden");
            //$("#follow"+toId).html("Following");
            noty({
              layout      : "topRight",
              text        : "You have started following "+toName,
              type        : 'success',
              theme       : 'relax',
              dismissQueue: true,
              animation   : {
                open  : 'animated bounceInRight',
                close : 'animated bounceOutRight'
              }
            });
          }
          else if(data == 2){
            $("#follow"+toId).removeClass("ed");
            $("#follow"+toId).html("Follow");
          }
          else{

          }

        }
        ,
        error: function(){
          noty({
            layout      : "topRight",
            text        : "Error occured",
            type        : 'error',
            theme       : 'relax',
            dismissQueue: true,
            animation   : {
              open  : 'animated bounceInRight',
              close : 'animated bounceOutRight'
            }
          });
        }           
      });
}

function artLikeClick(id){
  count = parseInt($('#love'+id).text());
  $('#love'+id).addClass('is-animating');
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('artId', id);
  $.ajax({
    url: "/likeArt",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 2){
         // alert(data);
         $('#love'+id).removeClass('is-animating').removeClass('ed');
         $('#love'+id+' .count').text(count-1);
       }
       else if(data == 1){
        $('#love'+id).removeClass('is-animating').addClass('ed');
        $('#love'+id+' .count').text(count+1);
      }
      else{
        noty({
          layout      : "topRight",
          text        : "Could not perform action, please try again!",
          type        : 'error',
          theme       : 'relax',
          dismissQueue: true,
          animation   : {
            open  : 'animated bounceInRight',
            close : 'animated bounceOutRight'
          }
        });
      }
    },
    error: function(){
      noty({
        layout      : "topRight",
        text        : "Error occured",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }
  });
}



function commentDeleteClick(id){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  var formData = new FormData();
  formData.append('commentId', id);

  $.ajax({
    url: "/deleteArtComment",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 0){
      }
      else if(data == 1){
       $("#commentDiv"+id).remove(); 
       $(".commentBadge").text(parseInt($(".commentBadge").text()) - 1);
     }
     else{

     }
   }
   ,
   error: function(){
    noty({
      layout      : "topRight",
      text        : "Error occured",
      type        : 'error',
      theme       : 'relax',
      dismissQueue: true,
      animation   : {
        open  : 'animated bounceInRight',
        close : 'animated bounceOutRight'
      }
    });
  }           
});
}
document.addEventListener("turbolinks:load", function() {
 $('#likeForm').submit(function(event){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "likeArt",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      alert(data);
    }
    ,
    error: function(){
      noty({
        layout      : "topRight",
        text        : "Error occured",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }           
  });

});

 $('#bookmarkForm').submit(function(event){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    url: "bookmarkArt",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      alert(data);
    }
    ,
    error: function(){
      noty({
        layout      : "topRight",
        text        : "Error occured",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }           
  });
});


 $('#commentArea').on('keyup',function() {
  var hasText = $('#commentArea').val();
  if(hasText != '') {
    $('#commentSubmit').attr('disabled' , false);
  }else{
    $('#commentSubmit').attr('disabled' , true);
  }
});

 $('#commentForm').submit(function(event){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "/commentArt",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
       // alert(data);
      // $('input[type="submit"]').attr('disabled' , true);
      $(data).appendTo(".comments-list");
      $("#commentArea").val("").css('height','41px');
      $(".commentBadge").text(parseInt($(".commentBadge").text()) + 1);

    }
    ,
    error: function(){
      noty({
        layout      : "topRight",
        text        : "Error occured",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }           
  });
});


 $('#artForm').submit(function(event){
   event.preventDefault();
   $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
   if($('#fileName').val() != "" && $('#fileName').val() != undefined){
     if($('#artDescriptionTextArea').val() == ""){
      noty({
        layout      : "topRight",
        text        : "Provide description and few hashtags for your post to make it more discoverable",
        type        : 'error',
        theme       : 'relax',
        dismissQueue: true,
        animation   : {
          open  : 'animated bounceInRight',
          close : 'animated bounceOutRight'
        }
      });
    }
    else{
      $.ajax({
        url: "/createArt",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: true,
        processData:false,
        success: function(data){
          if(data == 1){
              //alert("sachie");
              $('#artDescriptionTextArea').val('').keyup();
              if($("#mode").val() == "create")
              {
                $('.attached-image').hide();
              }
              analytics.track('Art has been posted by {{Auth::User()->name}}');
              noty({
                layout      : "topRight",
                text        : "Art saved succesfully",
                type        : 'success',
                theme       : 'relax',
                dismissQueue: true,
                animation   : {
                  open  : 'animated bounceInRight',
                  close : 'animated bounceOutRight'
                }
              });
              window.location = "/";
            }
            else if(data == 0){
              noty({
                layout      : "topRight",
                text        : "Art could not be saved succesfully",
                type        : 'error',
                theme       : 'relax',
                dismissQueue: true,
                animation   : {
                  open  : 'animated bounceInRight',
                  close : 'animated bounceOutRight'
                }
              });
            }
            else{

            }
          }
          ,
          error: function(){
            noty({
              layout      : "topRight",
              text        : "Error occured",
              type        : 'error',
              theme       : 'relax',
              dismissQueue: true,
              animation   : {
                open  : 'animated bounceInRight',
                close : 'animated bounceOutRight'
              }
            });
          }           
        });
    }

  }
  else{
    noty({
      layout      : "topRight",
      text        : "Upload your art first.",
      type        : 'error',
      theme       : 'metroui',
      timeout     : 4000,
      progressBar: true,
      dismissQueue: true,
      animation   : {
        open  : 'animated bounceInRight',
        close : 'animated bounceOutRight'
      }
    });
  }
});
});
