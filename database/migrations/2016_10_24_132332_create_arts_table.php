<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('images')->nullable();
            $table->string('description')->nullable();
            $table->string('category')->nullable();
            $table->boolean('nsfw')->default(false);
            $table->boolean('suggestions')->default(false);
            $table->string('likes_count')->default(0);
            $table->string('view_count')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arts');
    }
}
