<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// GET APIs

//Route::get('/{category?}', 'HomeController@showLandingPage');

Route::get("/oauthLoginSuccess", "HomeController@afterOauthLoginSuccess");

Route::get('/category/{category?}', 'ArtController@showCategoryPage');

Route::get('/', 'HomeController@home');
//Route::get('explore', 'HomeController@showLandingPage');
Route::get('explore', 'HomeController@showLandingPage');

Auth::routes();

Route::get('/profile/{id?}/{followType?}', 'ProfileController@showProfilePage');

Route::get('/artwork/{id?}', 'ArtController@showViewArtPage');

Route::get('/edit/art/{id?}', 'ArtController@showEditArtPage');

Route::get('/suggest/{id?}', 'ArtController@showSuggestArtPage');

//Route::get('/artwork/{id?}', 'ArtController@showViewArtPage');

//Route::get('goauth', 'Auth\AuthController@redirectToProvider');

//Route::get('redirect/google', 'Auth\AuthController@handleProviderCallback');
Route::get('/smcreate', 'HomeController@generateSiteMap');
Route::get('/sitemap', function() { include public_path().'/sitemaps/sitemap.xml'; });
Route::get('/home', 'HomeController@home');
Route::get('how', 'HomeController@how');
Route::get('terms-of-service', 'HomeController@terms');
Route::get('privacy-policy', 'HomeController@privacy');
Route::get('features', 'HomeController@features');

// Route::get('/redirect', function (){

// });

Route::get('/redirect', 'SocialAuthController@redirect');

Route::get('/callback', 'SocialAuthController@callback');	


Route::get('/redirect/g', 'SocialAuthController@redirectGoogle');

Route::get('/redirect/google', 'SocialAuthController@callbackGoogle');

Route::get('/post/art', 'ArtController@showCreateArtPage')->middleware('auth');

Route::get("/countries/1", 'HomeController@showCountries');



//POST APIssettingsUpdate
Route::post('/upvoteSuggestion', 'ArtController@postUpvoteSuggestion');

Route::post('/getArtStatistics', 'ArtController@postArtStatistics');

Route::post('/settingsUpdate', 'ProfileController@postUpdateSettings');

//addArtViewLocation
Route::post('/addArtViewLocation', 'ArtController@addArtViewLocation');

Route::post('/createArt', 'ArtController@postCreateArt');

Route::post('/setArtRating', 'ArtController@postArtRating');

Route::post('/createArtSuggestion', 'ArtController@postArtSuggestion');

//getCurrentUsersAllNotifications

Route::post('/markAllNotifications', 'ProfileController@postReadAllNotifications');

Route::get('/getAllNotifications', 'ProfileController@getCurrentUsersAllNotifications');

Route::post('/likeArt', 'ArtController@postLikeArt');

Route::post('/followUser', 'ProfileController@postFollowUser');

Route::post('/commentArt', 'ArtController@postCommentArt');

Route::post('/deleteArtSuggest', 'ArtController@postDeleteArtSuggest');

Route::post('/deleteArtComment', 'ArtController@postDeleteArtComment');

Route::post('/bookmarkArt', 'ArtController@postBookmarkArt');

Route::post('/deleteArt', 'ArtController@postDeleteArt');

Route::post('/suggestionApprove', 'ArtController@postSuggestionApprove');

Route::post('/like/{artid}', 'ArtController@likeUnlikeArt');

Route::post('/follow/{userid}', 'ProfileController@followUnfollowUser');


Route::get('quest', function () {
	return View::make('quests/index');
});


Route::get('/search', function () {
	try{
		$query = Request::input('q');
		$data = App\Art::search($query)->paginate(40);
		//return $data;
		return view('search.index', ['arts' => $data, 'query' => $query]);
	}
	catch(Exception $ex){
		return $ex;		
	}
});
