<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],
    'sendinblue' => [
        'secret' => 'qM294ZFQtRKAGrDj',
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '373046166427120',
        'client_secret' => '57729c681bb3e4604f931111c6e8a9bc',
        'redirect' => 'https://artstack.in/callback',
    ],

    'google' => [
        'client_id' => '574399515768-pt3n5namlu79i5l84smg8rkprqkb0hbu.apps.googleusercontent.com',
        'client_secret' => 'ldkS9noTtJuUgyR6Gcx0sPgW',
        'redirect' => 'https://artstack.in/redirect/google',
    ],

    'twitter' => [
        'client_id' => 'U1r6o4zNEnevfVlxazfa5xfBg',
        'client_secret' => 'JnK2vRKh1q0YmFLMYYxqWdOldqygH82RQy63WVQoe3AZcAxKmH',
        'redirect' => 'https://artstack.in/callback/twitter',
    ],

    'rollbar' => [
        'access_token' => '1244ea2b7e9e44309a4e7cb73f215fc9',
        'level' => 'debug',
    ],
];
