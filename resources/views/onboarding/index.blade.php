@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
<div class="artwork-container clearfix">
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	<section class="artwork" id="artwork">
		<div class="tbl">
			<div class="tblr">
				<div class="tbltd">
					<?php
					$uri  = $art->images;
					$uriPieces = explode("/", $uri);
					?>
					<img class="art" src="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,width:2048/{{$uriPieces[3]}}" alt="">
				</div>
			</div>
		</div>
		<div class="overlay"></div>
	</section>
	<aside class="aside">
		<div class="clearfix user">
			<div class="avatar-wrapper">
				<img class="img-circle" src="{{$art->g_pic}}" alt="" height="48">
			</div>
			@if (Auth::user() && Auth::user()->id == $art->user_id)
				<button class="btn btn-twitter btn-sm pull-right" onClick = "deleteArt({{$art->id}})">Delete art</button>
				<button class="btn btn-twitter btn-sm pull-right" onClick = "editArt({{$art->id}})">Edit art</button>
			@endif
			
			<div class="details">
				<p><a href="/profile/{{$art->user_id}}">{{$art->name}}</a></p>
				@if (Auth::user() && Auth::user()->id != $art->user_id)
				@if(!isFollowingTo(Auth::user()->id , $art->user_id))
				<button class="btn sub-btn" type="" id = "follow{{$art->user_id}}" onClick="textUserFollowClick({{$art->user_id}})">Follow</button>
				@endif
				@endif
			</div>
		</div>
		<div class="social-share-btns clearfix">
			@if(isArtLiked($art->user_id, $art->id))
			<a data-toggle="tooltip" data-placement="top" title="Love" href="#" class="love ed" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}}, love{{$art->id}})">
				@else
				<a data-toggle="tooltip" data-placement="top" title="Love" href="#" class="love" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}}, love{{$art->id}})">
					@endif	
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M349.6 64c-36.4 0-70.718 16.742-93.6 43.947C233.117 80.742 198.8 64 162.4 64 97.918 64 48 114.22 48 179.095c0 79.516 70.718 143.348 177.836 241.694L256 448l30.164-27.21C393.28 322.44 464 258.61 464 179.094 464 114.22 414.082 64 349.6 64zm-80.764 329.257l-4.22 3.873-8.616 7.773-8.616-7.772-4.214-3.868c-50.418-46.282-93.96-86.254-122.746-121.994C92.467 236.555 80 208.128 80 179.095c0-22.865 8.422-43.93 23.715-59.316C118.957 104.444 139.798 96 162.4 96c26.134 0 51.97 12.167 69.11 32.545L256 157.66l24.49-29.115C297.63 108.167 323.464 96 349.6 96c22.603 0 43.443 8.445 58.686 23.778C423.578 135.164 432 156.228 432 179.095c0 29.033-12.467 57.46-40.422 92.17-28.784 35.74-72.325 75.71-122.742 121.992z"/></svg>
				</a>
				<a data-trigger="click" data-toggle="tooltip" data-placement="top" title="Share on Facebook" class="fb" href="https://www.facebook.com/sharer/sharer.php?u=http://artstack.frb.io/artwork/1" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 512 512"><path d="M426.8 64H85.2C73.5 64 64 73.5 64 85.2v341.6c0 11.7 9.5 21.2 21.2 21.2H256V296h-45.9v-56H256v-41.4c0-49.6 34.4-76.6 78.7-76.6 21.2 0 44 1.6 49.3 2.3v51.8h-35.3c-24.1 0-28.7 11.4-28.7 28.2V240h57.4l-7.5 56H320v152h106.8c11.7 0 21.2-9.5 21.2-21.2V85.2c0-11.7-9.5-21.2-21.2-21.2z"/></svg>
				</a>
				<a data-toggle="tooltip" data-placement="top" title="Share on Twitter" class="tw" href="https://twitter.com/intent/tweet?url=http://artstack.frb.io/artwork/1&text=Title&via=akkias&hashtags=nature,sunset" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M492 109.5c-17.4 7.7-36 12.9-55.6 15.3 20-12 35.4-31 42.6-53.6-18.7 11.1-39.4 19.2-61.5 23.5C399.8 75.8 374.6 64 346.8 64c-53.5 0-96.8 43.4-96.8 96.9 0 7.6.8 15 2.5 22.1-80.5-4-151.9-42.6-199.6-101.3-8.3 14.3-13.1 31-13.1 48.7 0 33.6 17.2 63.3 43.2 80.7-16-.4-31-4.8-44-12.1v1.2c0 47 33.4 86.1 77.7 95-8.1 2.2-16.7 3.4-25.5 3.4-6.2 0-12.3-.6-18.2-1.8 12.3 38.5 48.1 66.5 90.5 67.3-33.1 26-74.9 41.5-120.3 41.5-7.8 0-15.5-.5-23.1-1.4C62.8 432 113.7 448 168.3 448 346.6 448 444 300.3 444 172.2c0-4.2-.1-8.4-.3-12.5C462.6 146 479 129 492 109.5z"/></svg>
				</a>
				<a data-toggle="tooltip" data-placement="top" title="Share on Pinterest" class="pin" href="https://pinterest.com/pin/create/button/?{{ http_build_query([ 'url' => 'http://artstack.frb.io/artwork/1', 'media' => '/images/promotion/1.jpg', 'description' => '$description' ]) }}" target="_blank">
					<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M256 32C132.3 32 32 132.3 32 256c0 91.7 55.2 170.5 134.1 205.2-.6-15.6-.1-34.4 3.9-51.4 4.3-18.2 28.8-122.1 28.8-122.1s-7.2-14.3-7.2-35.4c0-33.2 19.2-58 43.2-58 20.4 0 30.2 15.3 30.2 33.6 0 20.5-13.1 51.1-19.8 79.5-5.6 23.8 11.9 43.1 35.4 43.1 42.4 0 71-54.5 71-119.1 0-49.1-33.1-85.8-93.2-85.8-67.9 0-110.3 50.7-110.3 107.3 0 19.5 5.8 33.3 14.8 43.9 4.1 4.9 4.7 6.9 3.2 12.5-1.1 4.1-3.5 14-4.6 18-1.5 5.7-6.1 7.7-11.2 5.6-31.3-12.8-45.9-47-45.9-85.6 0-63.6 53.7-139.9 160.1-139.9 85.5 0 141.8 61.9 141.8 128.3 0 87.9-48.9 153.5-120.9 153.5-24.2 0-46.9-13.1-54.7-27.9 0 0-13 51.6-15.8 61.6-4.7 17.3-14 34.5-22.5 48 20.1 5.9 41.4 9.2 63.5 9.2 123.7 0 224-100.3 224-224C480 132.3 379.7 32 256 32z"/></svg>
				</a>
			</div>
			<div class="ad" style="font-size: 48px;color: #eee;text-align: center;border-bottom: 1px solid #eeeff2;">
				<!-- ArtStack -->
				<ins class="adsbygoogle"
				style="display:block"
				data-ad-client="ca-pub-5825528316059310"
				data-ad-slot="1783073983"
				data-ad-format="auto"></ins>
				<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
			<div class="art-details">

				<p class="art-description">{{$art->description}}</p>
			</div>
			<div class="reactions hidden clearfix">
				<button data-toggle="tooltip" data-placement="top" title="Add to favourites" type="button" class="fav ed">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
				<button data-toggle="tooltip" data-placement="top" title="Love" type="button" class="loved">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
				<button data-toggle="tooltip" data-placement="top" title="Amazing" type="button" class="amazing">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
				<button data-toggle="tooltip" data-placement="top" title="Master" type="button" class="master">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
				<button data-toggle="tooltip" data-placement="top" title="Wow" type="button" class="wow">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
				<button data-toggle="tooltip" data-placement="top" title="Rock" type="button" class="rock">
					<span></span>
					<i class="img-circle ticked"></i>
				</button>
			</div>
			<div class="comments">
					@if(Auth::user())
			
				<div class="comment-form clearfix">
					<div class="avatar">
						<img class="img-circle" src="{{$profilePic}}" alt="" height="24">
					</div>
					
					<form id = "commentForm" accept-charset="utf-8">
						<input type = "hidden" name = "artId" value = "{{$art->id}}"/>
						<textarea autofocus rows="1" type="text" id = "commentArea" class="comment-form-textarea autoheight" name="commentArea" placeholder="Add a comment"></textarea>
						<input class="btn btn-twitter btn-sm pull-right" type = "submit" value = "Submit"  />
					</form>
					

				</div>
					@endif

				<div class="comments-list">
					@foreach($comments as $comment)
					<div class="comment" id = "commentDiv{{$comment->id}}">
						<div class="avatar">
							@if($loginBy == "g")
							<img class="img-circle" src="{{$comment->g_pic}}" alt="" height="24">		
							@else
							<img class="img-circle" src="{{$comment->fb_pic}}" alt="" height="24">		
							@endif
						</div>
						<div class="comment-data">
							<p><a href="">{{$comment->name}}</a> {!!nl2br(e($comment->text))!!}</p>
							<small class="text-muted comment-timestamp">{{$comment->created_at}}</small>
						</div>
						<button class="delete" onClick ="commentDeleteClick({{$comment->id}})">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg>
						</button>
					</div>
					@endforeach
				</div>
			</div>
		</aside>
	</div>
	<div class="ad" style="font-size: 48px;color: #eee;text-align: center;border-bottom: 1px solid #eeeff2;">
		<!-- ArtStack -->
		<ins class="adsbygoogle"
		style="display:block"
		data-ad-client="ca-pub-5825528316059310"
		data-ad-slot="1783073983"
		data-ad-format="auto"></ins>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
	<div class="related-artwork">
		<div class="page-header">
			<h4>RELATED ARTS</h4>
		</div>
	</div>
@endsection