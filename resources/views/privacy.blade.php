@extends('layouts.app')
@section('title', 'Privacy policy | Artmojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property = "og:description" content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals"/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@artmojoin" />
<meta name = "twitter:description"  content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals" />
<meta property = "og:type" content =  "ArtMojo:craft" />
<meta property  =  "og:title" content = "ArtMojo | Community of artists | Showcase your work and become popular" />
<meta property  =  "og:image" content = "https://artmojo.in/images/fb_banner.png" />
<meta property="fb:app_id" content="373046166427120" />
@endsection
@section('header')
@parent
<nav class="subnav clearfix" id="sticker">
	<ul class="list-inline list-unstyled clearfix">
		<li class="active"><a onclick="ga('send', 'event', 'Privacy Policy clicked')" href="/privacy-policy">Privacy Policy</a></li>
		<li><a onclick="ga('send', 'event', 'Terms of Service clicked')" href="/terms-of-service">Terms of Service</a></li>
	</ul>
</nav>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="terms-container">
				<h1>Privacy Policy</h1>
				<p>Welcome,</p>
				<p>
					Please read this agreement carefully before accessing or using this site. Thank you for visiting the <a href="www.artmojo.in">www.artmojo.in</a> website. At iVision Web Studio the provider of the Product Website (referred to as "us" or "we"), we understand that the need to be discreet about the use of your information which you provide to us in the course of navigation or use of our website is critical to our business objectives and reputation. By accessing or using the site, you agree to be bound by this agreement. The information and services on this site are provided by artmojo.in and its suppliers, subject to your agreement to the terms and conditions below.
				</p>
				<p>We are firmly committed to protecting the privacy of our users, because your privacy is critically important to us. We value the trust you place on us, and our goal is to provide you with a secure online experience and to ensure that any information you submit to us is private and used/disclosed only for the purposes and in the means described below. This Privacy Policy discloses the privacy practices for the ArtMojo websites (collectively, the "Website").</p>
				<p>Therefore the following fundamental principles will apply:</p>
				<ol>
					<li>We will not ask you for personal information unless we truly need it.</li>
					<li>We don’t share your personal information with anyone except to comply with the
						law, develop our products, or protect our rights.
					</li>
					<li>We may not store personal information on our servers unless required for the
						on-going operation of one of our services.</li>
					</ol>
					<p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in Indian privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your (the "User" or the "End User") Personally Identifiable Information in accordance with our website. If you have questions or concerns regarding this statement, you should first contact IVISION WEB STUDIO Support at <a href="admin@artmojo.in">admin@artmojo.in</a>.</p>
					<p>PLEASE READ THIS PRIVACY POLICY CAREFULLY. BY ACCESSING OR USING OUR WEBSITE, YOU ACKNOWLEDGE THAT YOU HAVE READ, UNDERSTAND, AND AGREE TO BE BOUND TO ALL THE TERMS OF THIS PRIVACY POLICY AND OUR WEBSITE TERMS OF USE. IF YOU DO NOT AGREE TO THESE TERMS, EXIT THIS PAGE AND DO NOT ACCESS OR USE THE WEBSITE.</p>
					<h4>INFORMATION COLLECTED</h4>
					<p>Generally, you are not required to provide your personal information to use our website. However you may be required to provide such when you register, submit or contact us for further information or participate in our Surveys.</p>
					<p>Finally, ArtMojo collects information that falls under two general categories:</p>
					<ul>
						<li>Personally Identifiable Information (PII) and;</li>
						<li>Non-Personally Identifiable information (Non-PII).</li>
					</ul>
					<p>PII consists of any information which can be used to specifically identify you as an individual, whereas non-PII consists of aggregate information or any information that does not reveal your identity. We will only collect personal information, such as your name, telephone number or email address etc, when it is voluntarily submitted to us on the Site. We will use this information to comply with your request for information or as otherwise disclosed to you when you submit your information.</p>
					<p>From time to time, we may refer to that information to better understand your needs and how we can improve our services. We may use that information to contact you. We will not otherwise transfer the personal information you provide on the Site to any third party unless otherwise disclosed to you. We, and/or our authorized Third Party Service Providers and Advertisers, may automatically collect this Non-Personally Identifiable information when you visit our Website through the use of electronic tools like Cookies and Web beacons or Pixel tags, as described below.</p>
					<p>ArtMojo will also collect potentially personally-identifying information like Internet Protocol (IP) addresses. ArtMojo does not use such information to identify its visitors, however, and does not disclose such information, other than under the same circumstances that it uses and discloses personally-identifying information, as described below.</p>
					<p><strong>Cookies:</strong> Cookies are very small files placed on your computer, and they allow us to count the number of visitors to our website and distinguish repeat visitors from new visitors. We use “cookies” to keep track of some types of information while you are visiting our website or using our services. They also allow us to save user preferences and track user trends. We rely on cookies for the proper operation of our website; therefore if your browser is set to reject all cookies, the website will not function properly. Users who refuse cookies assume all responsibility for any resulting loss of functionality. We do not link the cookies to any PII.</p>
					<p><strong>Web Beacons:</strong> “Web beacons” (also known as “clear gifs” and “pixel tags”) are small transparent graphic images that are often used in conjunction with cookies in order to further personalize our website for our users and to collect a limited set of information about our visitors. We may also use web beacons in email communications in order to understand the behavior of our customers. We do not link the web beacons to any PII.</p>
					<p><strong>Log Files:</strong> Any time you visit any of our websites, our servers automatically gather information from your browser (such as your IP addresses, browser type, Internet service provider (ISP), referring/exit pages, platform type, date/time stamp, and number of clicks) to analyze trends, administer the site, prevent fraud, track visitor movement in the aggregate, and gather broad demographic information. For example, we may log your IP address for system administration purposes. IP addresses are logged to track a user’s session. This gives us an idea of which parts of our site users are visiting. We do not share the log files externally. Others may include: We also collect information about you when you register or subscribe to any of our services.</p>
					<p><strong>Others:</strong> Demographic Data, Online Survey Data, Information Regarding Your Friends, Public Forums etc are still part of the information that we may collect and gather from you. In addition to some of the above stated uses, by using our site, you agree to allow us to anonymously use the information from you and your experiences to continue our research into successful relationships. This research, conducted by psychologists and behavior research scientists, may be published in academic journals. However, all of your responses will be kept anonymous, and no PII will be published.</p>
					<h4>USE OF INFORMATION</h4>
					<p>In general, we may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>
					<ul>
						<li>To personalize user's experience and to allow us to deliver the type of content and product offerings in which you are most interested.</li>
						<li>To improve our website in order to serve you better.</li>
						<li>To allow us to better service you in responding to your customer service requests.</li>
						<li>To administer a contest, promotion, survey or other site feature.</li>
						<li>To quickly process your transactions.</li>
						<li>To ask for ratings and reviews of services or products.</li>
						<li>To follow up with them after correspondence (live chat, email or phone inquiries) • Other uses may include: to analyze trends, gather demographic information,
							comply with applicable law, and cooperate with law enforcement activities</li>
						</ul>
						<h4>SECURITY POLICY</h4>
						<p>We take security seriously and take numerous precautions to protect the security of Personally Identifiable Information. Any Personally Identifiable Information resides on a secure server that only selected personnel and contractors have access to. We encrypt certain sensitive information using Secure Socket Layer (SSL) technology to ensure that your Personally Identifiable Information is safe as it is transmitted to us. Unfortunately, no data transmission over the Internet or any wireless network can be guaranteed to be 100% secure. As a result, while we employ commercially reasonable security measures to protect data and seek to partner with companies which do the same, we cannot guarantee the security of any information transmitted to or from the Website, and are not responsible for the actions of any third parties that may receive any such information. Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make your visit to our site as safe as possible. Note that your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology. We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information. All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>
						<h4>DO WE USE 'COOKIES'?</h4>
						<p>Yes, we use cookies to facilitate and customize your experience with the Website; we store cookies on your computer. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your view. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about
							site traffic and site interaction so that we can offer better site experiences and tools in the future. We use cookies to:</p>
							<ul>
								<li>Understand and save user's preferences for future visits.</li>
								<li>Compile aggregate data about site traffic and site interactions in order to offer
									better site experiences and tools in the future. We may also use trusted third- party services that track this information on our behalf.</li>
								</ul>
								<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies. If you disable cookies, some features will be disabled it will turn off some of the features that make your site experience more efficient and some of our services will not function properly. However, you can still make use of the site anonymously.</p>
								<h4>DISCLOSURE OF PERSONALLY IDENTIFIABLE INFORMATION</h4>
								<p>We will not share your Personally Identifiable Information with other parties except as provided below:</p>
								<ul>
									<li>We may share your information with Authorized Third Party Service Providers. We provide some of our services and products through third parties. These "Third Party Service Providers" perform functions on our behalf, like sending out and distributing our administrative and promotional emails. We may share your Personally Identifiable Information with such Service Providers to send postal or email, remove repetitive information on customer lists, analyze data, provide marketing assistance, provide search results and links, operate the Website, troubleshoot, and provide customer service. We may also collect personal information from individuals and companies ("Affiliates") with whom we have business relationships and may have to also share their information with Service Providers to accomplish our administrative tasks. We encourage Third Party Service Providers to adopt and post privacy policies. However, the use of your Personally Identifiable Information by such parties is governed by the privacy policies of such parties and is not subject to our control.</li>
									<li>We may share your information in a Business Transfer. As with any other business, we could merge with, or be acquired by another company. If this
										occurs, the successor company would acquire the information we maintain, including Personally</li>
										<li>Identifiable Information. However, Personally Identifiable Information would remain subject to this Privacy Policy.</li>
										<li> We may share your information for our Protection and the Protection of Others. We may also disclose Personally Identifiable Information when we believe release is appropriate to comply with the law or a court order; enforce or apply this Privacy Policy, our Website Terms of Use or other agreements; or protect the rights, property or safety of the Website, its Users or others.</li>
									</ul>
									<h4>DISCLOSURE OF NON-PERSONALLY IDENTIFIABLE INFORMATION</h4>
									<p>We may disclose or share Non-Personally Identifiable Information with Partners, Affiliates and Advertisers. For example, we may share aggregated demographic information (which does not include any Personally Identifiable Information) with "Third Party Advertisers" or "Third Party Advertising Companies" and other parties as provided below: We use Third Party Advertising Companies to serve ads when you visit our Website. These companies may use Non-Personally Identifiable Information about your visits to this and other websites in order to provide, through the use of network tags, advertisements about goods and services that may be of interest to you. We also use Third Party Service Providers to track and analyze Non-Personally Identifiable usage and volume statistical information from our Users to administer our Website and constantly improve its quality. We may also publish this information for promotional purposes or as a representative audience for Advertisers. Please note that this is not Personally Identifiable Information, only general summaries of the activities of our Users. Such data is collected on our behalf, and is owned and used by us.</p>
									<h4>THIRD-PARTY LINKS</h4>
									<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website, by way of various applications. The privacy policies and practices of such sites in connection with information you disclose on such sites may differ from the practices of ArtMojo as set forth in this privacy statement, and you should review their policies and practices to ensure that the privacy of the information you submit on their site does not conflict with and is consistent with how you wish your information to be treated. Such sites may also place their own cookies or other files on your computer, collect data or solicit personal information from you. We therefore have no
										responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.
									</p>
									<ul>
										<li>Google: Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. <a href="https://support.google.com/adwordspolicy/answer/1316548?hl=en">https://support.google.com/adwordspolicy/answer/1316548?hl=en</a></li>
									</ul>
									<p>We have not enabled Google AdSense on our site but we may do so in the future.</p>
									<h4>AGREEMENT</h4>
									<p>We agree to the following:</p>
									<ul>
										<li>Users can visit our site anonymously.</li>
										<li>Once this privacy policy is created, we will add a link to it on our home page or as a minimum on the first significant page after entering our website.</li>
										<li>Our Privacy Policy link includes the word 'Privacy' and can easily be found on the page specified above.</li>
										<li>Users will be notified of any privacy policy changes on our Privacy Policy Page</li>
									</ul>
									<h4>UPDATING AND CORRECTING INFORMATION</h4>
									<p>We believe that you should have the ability to access and edit the Personally Identifiable Information you provide us. You may change any of your Personally Identifiable Information by logging into your user account created on <a href="www.artmojo.in">www.artmojo.in</a> and update your profile details sent to us. We encourage you to promptly update your Personally Identifiable Information if it changes. In addition, in may be impossible for us to completely delete all of your information because we periodically backup information.</p>
									<h4>CHOICE/OPT-OUT</h4>
									<p>As discussed above, you can always choose not to provide information, although it may be required to engage in a certain activity on the Website. We will send you information regarding updates about our products and promotional offers. You cannot opt-out of Administrative Emails.</p>
									<p><em>"Administrative Emails"</em> relate to a User's activity on the Website, and include emails regarding a particular User's account and requests or inquiries. If you do not wish to receive them, you have the option to deactivate your account. In contrast to
										Administrative Emails, however, you do have a choice with respect to Promotional Emails. Promotional Emails advertise our products and services, and/or the products and services of our Advertisers and Affiliates. If you do not want to receive Promotional Emails from us, you may select to opt-out of receiving Promotional Emails at any time after registering by hitting the "unsubscribe" button at the bottom of any of our e-mails. When contacting us, please indicate your name, address, email address, and what Promotional Emails you do not want to receive.</p>
										<p>You may use the following options for removing your information, including an unauthorized profile, from our e-mail database if you wish to opt out of receiving promotional e-mails and newsletters.</p>
										<p>Click on the “unsubscribe” link on the bottom of the e-mail sent to you;</p>
										<p>Or</p>
										<p>Send mail to the following postal address letting us know which promotional e-mails you wish to opt-out of:</p>
										<address>
											ArtMojo<br>
											iVision Web Studio<br>
											<a href="www.artmojo.in">www.artmojo.in</a><br>
											<a href="mailto:admin@artmojo.in">admin@artmojo.in</a>
										</address>
										<h4>DO NOT TRACK SIGNALS?</h4>
										<p>We honor do not track signals and do not track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.
										</p>
										<h4>PRIVACY POLICY AMENDMENTS/UPDATES</h4>
										<p>artmojo.in reserves the right to revise, amend, or modify this Privacy Policy from time to time in any manner. However, prior notification will be sent to you or revised policy date will be updated at the bottom or top of this page. Therefore we encourage users to continually visit this page for any changes or amendments. We also, encourage you to periodically review this page for the latest information on our privacy practices</p>
										<h4>AGE RESTRICTIONS</h4>
										<p>Minors under the age of 18 may not use the Website. We do not collect or maintain information from anyone known to be under the age of 18, and no part of the Website is designed to attract anyone under the age of 18. If you are under 18, you may use ArtMojo only with the involvement of a parent or guardian. This in line with The Federal Trade Commission, the nation's consumer protection agency, which spells out what operators of websites and online services must do to protect children's privacy and safety online.</p>
										<h4>CAN SPAM ACT</h4>
										<p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations. We collect your email address in order to send information, respond to inquiries, and/or other requests or questions. To be in accordance with CANSPAM we agree to the following:</p>
										<ul>
											<li>NOT use false or misleading subjects or email addresses.</li>
											<li>Identify the message as an advertisement in some reasonable way.</li>
											<li>Include the physical address of our business or site headquarters.</li>
											<li>Monitor third-party email marketing services for compliance, if one is used.</li>
											<li>Honor opt-out/unsubscribe requests quickly.</li>
											<li>Allow users to unsubscribe by using the link at the bottom of each email.</li>
										</ul>
										<h4>COPYRIGHT AND TRADEMARK NOTICE</h4>
										<p>The contents of this site are the exclusive property of <a href="www.artmojo.in">www.artmojo.in</a> and its users or members, and may not be copied, distributed, displayed, altered or modified, or reproduced or transmitted, in any form or by any means including, but not limited to, electronic, photocopy, or otherwise, without the prior written permission of <a href="www.artmojo.in">www.artmojo.in</a>. The corporate and product names, slogans, logos and other content contained on this site are the registered or unregistered trademarks or service marks of <a href="www.artmojo.in">www.artmojo.in</a> or are used by <a href="www.artmojo.in">www.artmojo.in</a> with the permission of their owners. The use, copying or alteration of any Trademark without the express prior written consent of <a href="www.artmojo.in">www.artmojo.in</a> or, where applicable, the registered owner’s express prior written consent is strictly prohibited. The Trademarks are protected under the Trademark Act and by state or common law.</p>
										<h4>DATA RETENTION</h4>
										<p>Because many users tend to use our service at different points, we retain your personal information for continued service and convenience purposes until you advise us not to do so. Notwithstanding the foregoing, we may retain personal information in order to comply with applicable laws, keep accurate records, resolve disputes, prevent fraud, enforce our Terms and Conditions of Service or other agreements, or for any other legally permissible purpose.</p>
										<h4>ACCEPTANCE OF PRIVACY STATEMENT</h4>
										<p>Your use of our website(s), including any dispute concerning privacy, is subject to this privacy statement and the applicable Terms and Conditions of Service. BY USING OUR WEBSITE, YOU ARE ACCEPTING THE PRACTICES SET OUT IN THIS PRIVACY STATEMENT AND THE APPLICABLE TERMS AND CONDITIONS OF SERVICE. If we decide to change our privacy policy, we will post those changes to this privacy statement page and any other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy statement at any time, so please review it frequently. If we make material changes to this policy, we will notify you here, or by other means, such as e- mail, at our discretion. Your continued use of any portion of our website following posting of the updated privacy statement will constitute your acceptance of the changes.</p>
										<h4>CONTACT</h4>
										<p>If there are any questions regarding this privacy policy you may contact us using the information below.</p>
										<address>
											ArtMojo<br>
											iVision Web Studio<br>
											<a href="www.artmojo.in">www.artmojo.in</a><br>
											<a href="mailto:admin@artmojo.in">admin@artmojo.in</a>
										</address>
									</div>
								</div>
							</div>
						</div>
						@endsection