@extends('layouts.app')
@section('title', 'Features | Artmojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property = "og:description" content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals"/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@artmojoin" />
<meta name = "twitter:description"  content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals" />
<meta property = "og:type" content =  "ArtMojo:craft" />
<meta property  =  "og:title" content = "ArtMojo | Community of artists | Showcase your work and become popular" />
<meta property  =  "og:image" content = "https://artmojo.in/images/fb_banner.png" />
<meta property="fb:app_id" content="373046166427120" />
@endsection
@section('content')
<div class="hero landingscreen clearfix">
	{{ '', $randNum = mt_rand(1, 14) }}
	<figure style="background-image: url(/images/landing/{{$randNum}}.jpg)"></figure>
	<figcaption class="caption text-center">
		<h1>Creativity is important for us</h1>
	</figcaption>
</div>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image">
						<img class="img-responsive" src="../images/account_creation.png" width="60" style="margin:20px auto">
					</div>
						<h3 class="title">Easy Account Creation</h3>
						<p>
							User account creation and log in to website is done just by clicking the sign in button. 
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image">
						<img class="img-responsive" src="../images/upload.png">
					</div>
						<h3 class="title">Art Upload</h3>
						<p>
							Artmojo provided clean and easy user interface to upload the art.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image">
						<img class="img-responsive" src="../images/sources.png">
					</div>
						<h3 class="title">Upload from multiple sources</h3>
						<p>
							User can upload their arts from any social platform e.g. Facebook, Instagram, Google Photos, Flickr, Dropbox, Box etc. 
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image">
						<img class="img-responsive" src="../images/suggestion1.png">
					</div>
						<h3 class="title">Art Suggestions</h3>
						<p>
							Artmojo has provided an easy way for users to provide their creative suggestions to the artist.
							Users can click part of the art where he/she wants to give suggestions and write it down over there.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image"><img class="img-responsive" src="../images/rating.png"></div>
						<h3 class="title">Art Rating System</h3>
						<p>
							Users are able to rate the art for their creativity, perfection and uniqueness.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image"><img class="img-responsive" src="../images/update.png"></div>
						<h3 class="title">Art Update</h3>
						<p>
							Users will be able to modify or delete their art at any time.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-6 col-lg-4 hide">
			<div class="box">
				<div class="icon">
					<div class="info">
					<div class="image"><img class="img-responsive" src="../images/update.png"></div>
						<h3 class="title">Art Deletion</h3>
						<p>
							Users will be able to modify or delete their art at any time.
						</p>
					</div>
				</div>
				<div class="space"></div>
			</div>
		</div>
	</div>
</div>

<style>
	.white {
		color: white;
	}

	.btn-lg {
		font-size: 38px;
		line-height: 1.33;
		border-radius: 6px;
	}

	.box .icon {
		text-align: center;
		position: relative;
	}

	.box .icon .image {
		position: relative;
		z-index: 2;
		margin: auto;
		width: 88px;
		height: 88px;
		border: 7px solid white;
		line-height: 88px;
		border-radius: 50%;
		background: #eee;
		overflow: hidden;
		vertical-align: middle;
	}

	
	.box .icon .image i {
		font-size: 40px !important;
		color: #fff !important;
	}

	
	.box .icon .info {
		background: #FFF;
		border: 1px solid #e2e2e2;
		padding: 15px 0 10px 0;
		min-height: 290px;
	}

	.box .icon .info h3.title {
		color: #222;
		font-weight: 500;
	}

	.box .icon .info p {
		color: #666;
		line-height: 1.5em;
		margin: 20px;
	}


	.box .icon .info .more a {
		color: #222;
		line-height: 12px;
		text-transform: uppercase;
		text-decoration: none;
	}

	
	.box .space {
		height: 30px;
	}
</style>
@endsection