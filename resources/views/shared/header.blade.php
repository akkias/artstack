<nav class="navbar navbar-default navbar-static-top {{ Request::is('/','quest','login') ? 'navbar-trans' : '' }} role="navigation">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a onclick="ga('send', 'event', '/', 'Logo clicked');" class="navbar-brand" href="/">
				<img height="32" src="/images/artmojo_logow.png" alt="ArtMojo">
			</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav menu">
				<li class="{{ Request::is('explore') ? 'active' : '' }}"><a href="/explore" onClick="analytics.page('Opened explore page');">
					<svg height="20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 443.771 443.772" style="enable-background:new 0 0 443.771 443.772;" xml:space="preserve">
						<path d="M163.073,3.459C140.557,125.764,122.304,144.016,0,166.532c122.305,22.516,140.557,40.768,163.073,163.073     c22.516-122.305,40.768-140.557,163.073-163.073C203.841,144.016,185.588,125.764,163.073,3.459z" fill="#FFFFFF"/>
						<path d="M340.064,232.896c-14.318,77.781-25.928,89.39-103.708,103.708c77.78,14.319,89.39,25.928,103.708,103.708     c14.318-77.78,25.928-89.39,103.707-103.708C365.99,322.286,354.383,310.677,340.064,232.896z" fill="#FFFFFF"/>
					</svg>
					Explore</a></li>
					<li class="{{ Request::is( 'quest') ? 'active' : '' }}"><a href="/quest" onClick="analytics.page('Opened quest page');">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;" xml:space="preserve" height="20">
							<path d="M466.45,49.374c-7.065-8.308-17.368-13.071-28.267-13.071H402.41v-11.19C402.41,11.266,391.143,0,377.297,0H134.705    c-13.848,0-25.112,11.266-25.112,25.112v11.19H73.816c-10.899,0-21.203,4.764-28.267,13.071    c-6.992,8.221-10.014,19.019-8.289,29.624c9.4,57.8,45.775,108.863,97.4,136.872c4.717,11.341,10.059,22.083,16.008,32.091    c19.002,31.975,42.625,54.073,68.627,64.76c2.635,26.644-15.094,51.885-41.794,57.9c-0.057,0.013-0.097,0.033-0.153,0.046    c-5.211,1.245-9.09,5.921-9.09,11.513v54.363h-21.986c-19.602,0-35.549,15.947-35.549,35.549v28.058    c0,6.545,5.305,11.85,11.85,11.85H390.56c6.545,0,11.85-5.305,11.85-11.85v-28.058c0-19.602-15.947-35.549-35.549-35.549h-21.988    V382.18c0-5.603-3.893-10.286-9.118-11.52c-0.049-0.012-0.096-0.028-0.145-0.04c-26.902-6.055-44.664-31.55-41.752-58.394    c25.548-10.86,48.757-32.761,67.479-64.264c5.949-10.009,11.29-20.752,16.008-32.095c51.622-28.01,87.995-79.072,97.395-136.87    C476.465,68.392,473.443,57.595,466.45,49.374z M60.652,75.192c-0.616-3.787,0.431-7.504,2.949-10.466    c2.555-3.004,6.277-4.726,10.214-4.726h35.777v21.802c0,34.186,4.363,67.3,12.632,97.583    C89.728,153.706,67.354,116.403,60.652,75.192z M366.861,460.243c6.534,0,11.85,5.316,11.85,11.85v16.208H134.422v-16.208    c0-6.534,5.316-11.85,11.85-11.85H366.861z M321.173,394.03v42.513H191.96V394.03H321.173z M223.037,370.331    c2.929-3.224,5.607-6.719,8.002-10.46c7.897-12.339,12.042-26.357,12.228-40.674c4.209,0.573,8.457,0.88,12.741,0.88    c4.661,0,9.279-0.358,13.852-1.036c0.27,19.239,7.758,37.45,20.349,51.289H223.037z M378.709,81.803    c0,58.379-13.406,113.089-37.747,154.049c-23.192,39.03-53.364,60.525-84.956,60.525c-31.597,0-61.771-21.494-84.966-60.523    c-24.342-40.961-37.748-95.671-37.748-154.049V25.112c0-0.78,0.634-1.413,1.412-1.413h242.591c0.78,0,1.414,0.634,1.414,1.413    V81.803z M451.348,75.192c-6.702,41.208-29.074,78.51-61.569,104.191c8.268-30.283,12.631-63.395,12.631-97.58V60.001h35.773    c3.938,0,7.66,1.723,10.214,4.726C450.915,67.688,451.963,71.405,451.348,75.192z" fill="#FFFFFF"/>
							<path d="M327.941,121.658c-1.395-4.288-5.103-7.414-9.566-8.064l-35.758-5.196l-15.991-32.402    c-1.997-4.044-6.116-6.605-10.626-6.605c-4.511,0-8.63,2.561-10.626,6.605l-15.991,32.402l-35.758,5.196    c-4.464,0.648-8.172,3.775-9.566,8.065c-1.393,4.291-0.231,8.999,2.999,12.148l25.875,25.221l-6.109,35.613    c-0.763,4.446,1.064,8.938,4.714,11.59c3.648,2.651,8.487,3,12.479,0.902L256,190.32l31.982,16.813    c1.734,0.911,3.627,1.36,5.512,1.36c2.456,0,4.902-0.763,6.966-2.263c3.65-2.652,5.477-7.144,4.714-11.59l-6.109-35.613    l25.875-25.221C328.172,130.658,329.334,125.949,327.941,121.658z M278.064,146.405c-2.793,2.722-4.068,6.644-3.408,10.489    l3.102,18.09l-16.245-8.541c-1.725-0.908-3.62-1.36-5.514-1.36c-1.894,0-3.788,0.454-5.514,1.36l-16.245,8.541l3.102-18.09    c0.66-3.844-0.615-7.766-3.408-10.489l-13.141-12.81l18.162-2.64c3.859-0.56,7.196-2.985,8.922-6.482l8.123-16.458l8.122,16.458    c1.727,3.497,5.062,5.921,8.922,6.482l18.162,2.64L278.064,146.405z" fill="#FFFFFF"/>
						</svg>


						Quest</a></li>
						<li class="{{ Request::is( 'features') ? 'active' : '' }}"><a href="/#features" onClick="analytics.page('Opened how it works page');"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M432 80v352H80V80h352m16-16H64v384h384V64z"/><path d="M192 152h192v16H192zM192 248h192v16H192zM192 344h192v16H192z"/><circle cx="144" cy="160" r="16"/><circle cx="144" cy="256" r="16"/><circle cx="144" cy="352" r="16"/></svg> Features</a></li>
						<li class="dropdown">
							<a href="#" class="more"><svg xmlns="http://www.w3.org/2000/svg" width="32" viewBox="0 0 512 512"><path d="M256 224c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32zM128.4 224c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.4-32-32-32zM384 224c-17.7 0-32 14.3-32 32s14.3 32 32 32 32-14.3 32-32-14.3-32-32-32z"/></svg></a>
							<ul class="dropdown-menu">
								<li><a href="/privacy-policy">Privacy Policy</a></li>
								<li><a href="/terms-of-service">Terms of Service</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						@if(Request::is('suggest/*'))
						<li>
							<div class="social-actions pull-right">
								@if(Auth::user() && Auth::user()->id == $art->user_id)
								<a data-toggle="tooltip" data-placement="bottom" title="Edit Art" class="action edit" href="/edit/art/{{$art->id}}">
									<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path fill="#010101" d="M64 368v80h80l235.727-235.73-80-79.997L64 368zm377.602-217.602c8.53-8.53 8.53-21.334 0-29.865l-50.135-50.135c-8.53-8.53-21.334-8.53-29.865 0l-39.468 39.47 80 79.997 39.468-39.467z"/></svg>
								</a>
								@endif
								@if(Auth::user())
								@if(isArtLiked(Auth::user()['id'], $art->id))
								<a data-toggle="tooltip" data-placement="bottom" title="Love" href="#" class="action love ed" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}})">
									@else
									<a data-toggle="tooltip" data-placement="bottom" title="Love" href="#" class="action love" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}})">
										@endif	
										<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M359.385 80C319.965 80 277.17 97.6 256 132.8 234.83 97.6 192.034 80 152.615 80 83.647 80 32 123.238 32 195.78c0 31.287 12.562 71.923 40.923 105.656 28.36 33.735 45.23 51.7 100.153 88C228 425.738 256 432 256 432s28-6.262 82.924-42.564c54.923-36.3 71.794-54.265 100.153-88C467.437 267.703 480 227.066 480 195.78 480 123.237 428.353 80 359.385 80z"/></svg>
										@if(Auth::user()->id == $art->user_id)
										<span class="count">{{$art->likes_count}}</span>
										@endif
									</a>
									@else
									<span title="Love" class="action love" id="love{{$art->id}}" data-toggle="modal" data-target="#loginModal">
										<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
									</span>
									@endif
									<a class="action fb" data-toggle="tooltip" data-placement="bottom" title="Share on Facebook"  href="https://www.facebook.com/sharer/sharer.php?u=https://artmojo.in/suggest/{{$art->id}}" target="_blank">
										<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 512 512"><path d="M426.8 64H85.2C73.5 64 64 73.5 64 85.2v341.6c0 11.7 9.5 21.2 21.2 21.2H256V296h-45.9v-56H256v-41.4c0-49.6 34.4-76.6 78.7-76.6 21.2 0 44 1.6 49.3 2.3v51.8h-35.3c-24.1 0-28.7 11.4-28.7 28.2V240h57.4l-7.5 56H320v152h106.8c11.7 0 21.2-9.5 21.2-21.2V85.2c0-11.7-9.5-21.2-21.2-21.2z"/></svg>
									</a>
									<a class="action twit" data-toggle="tooltip" data-placement="bottom" title="Share on Twitter" href="https://twitter.com/intent/tweet?url=https://artmojo.in/suggest/{{$art->id}}&text={{$art->description}}&via=artmojoin&hashtags=art,creativity,artmojo" target="_blank">
										<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M492 109.5c-17.4 7.7-36 12.9-55.6 15.3 20-12 35.4-31 42.6-53.6-18.7 11.1-39.4 19.2-61.5 23.5C399.8 75.8 374.6 64 346.8 64c-53.5 0-96.8 43.4-96.8 96.9 0 7.6.8 15 2.5 22.1-80.5-4-151.9-42.6-199.6-101.3-8.3 14.3-13.1 31-13.1 48.7 0 33.6 17.2 63.3 43.2 80.7-16-.4-31-4.8-44-12.1v1.2c0 47 33.4 86.1 77.7 95-8.1 2.2-16.7 3.4-25.5 3.4-6.2 0-12.3-.6-18.2-1.8 12.3 38.5 48.1 66.5 90.5 67.3-33.1 26-74.9 41.5-120.3 41.5-7.8 0-15.5-.5-23.1-1.4C62.8 432 113.7 448 168.3 448 346.6 448 444 300.3 444 172.2c0-4.2-.1-8.4-.3-12.5C462.6 146 479 129 492 109.5z"/></svg>
									</a>
									<a class="action pint" data-toggle="tooltip" data-placement="bottom" title="Share on Pinterest" href="https://pinterest.com/pin/create/button/?url=https://artmojo.in/suggest/{{$art->id}}&media={{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,height:630/{{$uriPieces[3]}}&description={{$art->description}}"
										target="_blank">
										<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M256 32C132.3 32 32 132.3 32 256c0 91.7 55.2 170.5 134.1 205.2-.6-15.6-.1-34.4 3.9-51.4 4.3-18.2 28.8-122.1 28.8-122.1s-7.2-14.3-7.2-35.4c0-33.2 19.2-58 43.2-58 20.4 0 30.2 15.3 30.2 33.6 0 20.5-13.1 51.1-19.8 79.5-5.6 23.8 11.9 43.1 35.4 43.1 42.4 0 71-54.5 71-119.1 0-49.1-33.1-85.8-93.2-85.8-67.9 0-110.3 50.7-110.3 107.3 0 19.5 5.8 33.3 14.8 43.9 4.1 4.9 4.7 6.9 3.2 12.5-1.1 4.1-3.5 14-4.6 18-1.5 5.7-6.1 7.7-11.2 5.6-31.3-12.8-45.9-47-45.9-85.6 0-63.6 53.7-139.9 160.1-139.9 85.5 0 141.8 61.9 141.8 128.3 0 87.9-48.9 153.5-120.9 153.5-24.2 0-46.9-13.1-54.7-27.9 0 0-13 51.6-15.8 61.6-4.7 17.3-14 34.5-22.5 48 20.1 5.9 41.4 9.2 63.5 9.2 123.7 0 224-100.3 224-224C480 132.3 379.7 32 256 32z"/></svg>
									</a>
								</div>
								@if(Auth::user() && $art->enableSuggestions && Auth::user()->id != $art->user_id)
								<div class="rating pull-right">
									<span class="pull-right total-rating" id = "totalRatingSpan">
										{{$art->rating == 0 ? $art->rating :  number_format($art->rating, 1)}}
									</span>
									<div id="rateYo" class="pull-right" 
									onclick = "setArtRating({{$art->id}}, this)">
								</div>
								<span class="hide">
									{{$art->rating_count}}
								</span>
							</div>	
							<button class="action pull-right disable-sgst" onClick="disableSuggestions()">
								<svg height="16" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 24 24" style="enable-background:new 0 0 24 24;" xml:space="preserve">
								<circle style="fill:none;stroke:#fff;stroke-width:2;stroke-miterlimit:10;" cx="12" cy="12" r="11"/>
								<line style="fill:none;stroke:#fff;stroke-width:2;stroke-miterlimit:10;" x1="4.158" y1="4.158" x2="19.842" y2="19.842"/>
							</svg>
							Disable</button>
							<button class="action pull-right enable-sgst" onClick="enableSuggestions()">
								<svg height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M96 124.2c0-6.9 5.2-12.2 12.2-12.2H176V64h-66.8C75.7 64 48 90.7 48 124.2V192h48v-67.8zM403.6 64H336v48h67.2c6.9 0 12.8 5.2 12.8 12.2V192h48v-67.8c0-33.5-27-60.2-60.4-60.2zM416 386.8c0 6.9-5.2 12.2-12.2 12.2H336v49h67.8c33.5 0 60.2-27.7 60.2-61.2V320h-48v66.8zM108.2 399c-6.9 0-12.2-5.2-12.2-12.2V320H48v66.8c0 33.5 27.7 61.2 61.2 61.2H176v-49h-67.8z"/></svg>
								Add Suggestion
							</button>
							@else
							<button class="action pull-right enable-sgst" data-toggle="modal" data-target="#loginModal">
								<svg height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M96 124.2c0-6.9 5.2-12.2 12.2-12.2H176V64h-66.8C75.7 64 48 90.7 48 124.2V192h48v-67.8zM403.6 64H336v48h67.2c6.9 0 12.8 5.2 12.8 12.2V192h48v-67.8c0-33.5-27-60.2-60.4-60.2zM416 386.8c0 6.9-5.2 12.2-12.2 12.2H336v49h67.8c33.5 0 60.2-27.7 60.2-61.2V320h-48v66.8zM108.2 399c-6.9 0-12.2-5.2-12.2-12.2V320H48v66.8c0 33.5 27.7 61.2 61.2 61.2H176v-49h-67.8z"/></svg>
								Add Suggestion
							</button>
							@endif
						</li>

						@else
						<!-- Authentication Links -->
						@if (Auth::guest())
						<li><a onClick="analytics.page('Opened modal box to login');" class="login" href="#" data-toggle="modal" data-target="#loginModal">Login / Register</a></li>
						@else
						<li>
							<a data-turbolinks="false" class="post" href="/post/art?quest=ArtMojoCreativeQuest">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M416 277.333H277.333V416h-42.666V277.333H96v-42.666h138.667V96h42.666v138.667H416v42.666z"/></svg> POST YOUR ART
							</a>
						</li>
						<li>
							@if(Auth::user() && userNotificationPresent())
							@if(userNotificationPresent() > 0)
							<span class="noti-count">{{userNotificationPresent()}}</span>
							@endif
							<a class="noti has-notifications" onclick="toggleNotificationCenter()" href="#">
								<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 512 512"><path d="M256 464c22.78 0 41.41-18.72 41.41-41.6H214.59c0 22.88 18.633 41.6 41.412 41.6zm134.59-124.8V224.8c0-63.44-44.517-117.518-103.53-131.04V79.2C287.06 61.518 273.6 48 256 48s-31.06 13.518-31.06 31.2v14.56c-59.014 13.522-103.53 67.6-103.53 131.04v114.4L80 380.8v20.8h352v-20.8l-41.41-41.6z"/></svg>
							</a>
							@else
							<span class="noti-count" style="display: none;">0</span>
							<a class="noti" onclick="toggleNotificationCenter()" href="#" >
								<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 512 512"><path d="M256 464c22.78 0 41.41-18.72 41.41-41.6H214.59c0 22.88 18.633 41.6 41.412 41.6zm134.59-124.8V224.8c0-63.44-44.517-117.518-103.53-131.04V79.2C287.06 61.518 273.6 48 256 48s-31.06 13.518-31.06 31.2v14.56c-59.014 13.522-103.53 67.6-103.53 131.04v114.4L80 380.8v20.8h352v-20.8l-41.41-41.6z"/></svg>
							</a>
							@endif	
						</li>
						<li class="dropdown user-dd">
							<a href="/profile/{{Auth::user()->id}}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
								<img src="{{getProfilePic()}}" height="32" width="32" class="img-circle" alt="avatar">
								<i class="icon-menu"></i>
							</a>
							<ul class="dropdown-menu">
								<li><a href="/profile/{{Auth::user()->id}}">Profile</a></li>
								<li><a href="/profile/{{Auth::user()->id}}/following">Following</a></li>
								<li><a href="/profile/{{Auth::user()->id}}/followers">Followers</a></li>
								<li class="hidden"><a href="/settings">Settings</a></li>
								<li>
									<a href="{{ url('/logout') }}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
									Logout
								</a>

								<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
									{{ csrf_field() }}
								</form>
							</li>
						</ul>
					</li>
					@endif
					@endif

				</ul>
				<ul class="nav navbar-nav hidden">
					<li class="active"><a href="#">Discover</a></li>
					<li><a href="#">Popular</a></li>
					<li class="hidden"><a href="#">My Feed</a></li>
					<li class="hidden"><a href="#">Starred</a></li>
					<li class="hidden"><a href="#">Shop</a></li>
					<li class="hidden"><a class="post" href="#">
						<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512">
							<g>
								<path d="M64 368v80h80l235.727-235.73-80-79.997L64 368zm377.602-217.602c8.53-8.53 8.53-21.334 0-29.865l-50.135-50.135c-8.53-8.53-21.334-8.53-29.865 0l-39.468 39.47 80 79.997 39.468-39.467z"/>
							</g>
						</svg>
						Post</a></li>
					</ul>
					@if(!Request::is('suggest/*'))
					<form class="navbar-form" action="/search" role="search">
						<i class="artmojo-icon-search"></i>
						<input value="{{$query or ''}}" name="q" type="text" class="form-control {{Request::path() == 'search' ?'has-query' : ''}}" placeholder="Search...">
						@if(Request::path() == 'search')
						<div class="powered-by pull-right">Powered by <img height="16" src="/images/Algolia_logo_bg-white.svg"></div>
						@endif
					</form>
					@endif
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>