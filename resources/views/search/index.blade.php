@extends('layouts.app')
@section('title', $query.' | Artmojo - Be creative together')
@section('ogTags')
<meta content="Discover trending creative photographs, paintings, sketches, digital art, 3D art, graffiti, poetry, animations and other creative work from across the world on Touchtalent" name="description">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<meta content="1 Days" name="revisit-after">
<meta content="all,index,follow" name="ROBOTS">
<meta content="Discover trending creative photographs, paintings, sketches, digital art, 3D art, graffiti, poetry, animations and other creative work from across the world on Touchtalent" itemprop="description">
<meta content="http://www.touchtalent.com/search?q=akshay&type=&country=&sort=recent&category=" itemprop="url">
<meta content="316237385059852" property="fb:app_id">
<meta content="http://www.touchtalent.com/search?q=akshay&type=&country=&sort=recent&category=" property="og:url">
<meta content="Touchtalent.com" property="og:site_name">
<meta content="Discover trending creative photographs, paintings, sketches, digital art, 3D art, graffiti, poetry, animations and other creative work from across the world on Touchtalent" property="og:description">
<meta name="twitter:domain">
<meta content="@touchtalent" name="twitter:site">
<meta content="Discover trending creative photographs, paintings, sketches, digital art, 3D art, graffiti, poetry, animations and other creative work from across the world on Touchtalent" name="twitter:description">
<meta content="Touchtalent" name="twitter:app:name:googleplay">
<meta content="com.app.touchtalent" name="twitter:app:id:googleplay">
<meta content="com.app.touchtalent" property="al:android:package">
<meta content="Touchtalent" property="al:android:app_name">
<meta content="touchtalent://search" property="al:android:url">
<meta content="touchtalent://search" name="twitter:app:url:googleplay">
<meta content="Trending Creative Works | Touchtalent" property="og:title">
<meta content="http://static.touchtalent.com/fb_dp.png" property="og:image">
<meta content="http://static.touchtalent.com/fb_dp.png" itemprop="image">
<meta content="summary_large_image" name="twitter:card">
<meta content="Trending Creative Works | Touchtalent" name="twitter:title">
<meta content="http://static.touchtalent.com/fb_dp.png" name="twitter:image">
@endsection
@section('content')
<div id="streamline" class="streamline animated" data-columns>
	@if(count($arts) > 0)
		@foreach($arts as $art)
			@include('/artwork/stream')	
		@endforeach
	@else
	<div class="empty-art text-center">
		<div class="empty-content">
			<img src="/images/empty.png" height="250" class="hide">
			<span class="text-muted" style="font-size: 128px">(>_<)</span>
			<h2>No Arts</h2>
			<p>Be the first one to share a creative work on <strong>{{$query}}</strong></p>
			<br>
			<p><a data-turbolinks="false" class="post-art btn btn-primary btn-lg" href="/post/art">
			<svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 512 512"><path d="M256 48C141.125 48 48 141.125 48 256s93.125 208 208 208 208-93.125 208-208S370.875 48 256 48zm107 229h-86v86h-42v-86h-86v-42h86v-86h42v86h86v42z"/></svg>
			POST YOUR ART</a></p>
		</div>
	</div>
	@endif
</div>
<div class="text-center">
	{{ $arts->links() }}
</div>
@endsection