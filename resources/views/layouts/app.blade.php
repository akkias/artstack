<!DOCTYPE html>
<html lang="en">
<head>
  <?php
//set headers to NOT cache a page
header("Cache-Control: private, stale-while-revalidate=604800"); //HTTP 1.1

?>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<meta content="width=device-width,initial-scale = 1.0" name="viewport">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>
<meta content='ArtMojo, ArtMojoin, photographers, photography, photo, photos, inspiring photography, photo sharing, photography community, photo download, wall art, commercial photography, pulse, affection, flow, following, activity, fresh, upcoming, editors, photo portfolio Social Network, Ad-free, message service, data mining, free, instant message, message, messaging, private messaging, text messaging, text post, image post, sound file, video, words, friends, friends and noise, Youtube, Vimeo, Facebook, Tumblr, Twitter, Instagram, App.net, Pinterest, Path,Art, Digital Art, Photography, Artist Community, Traditional Art, Community Art, Contemporary Art, Modern Art, Skins, Themes, Poetry, Prose, Applications, Wallpapers, Online Art, graphic Design, web Design, gaming, animation, comic Books, digital Images' name="keywords">
<meta content='ArtMojo is the online social community for artists and art enthusiasts, allowing people to showcase their workand let them help newbies to achieve their goals' name='description'>
<meta name="classification" content="Art">
<meta name="copyright" content="Copyright 2017 ArtMojo">
<link rel="shortcut icon" href="/images/favmj.png">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="turbolinks-root" content="/">
<meta name="turbolinks-cache-control" content="no-cache">
<meta name="google-site-verification" content="qeZjYkQCSGHdKzEQGDTsY1moPfF560UAaklYLp4D8OQ" />
@yield('ogTags')
<!-- Styles -->
<link rel="stylesheet" href="{{ elixir('css/app.css') }}" media="all">
<!-- Scripts -->
<script src="https://js.pusher.com/4.0/pusher.min.js"></script>
<script>
  window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
    ]); ?>
  </script>
</head>
<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/turbolinks/5.0.0/turbolinks.min.js"></script>
<script data-cfasync="false" type="text/javascript" src="https://api.filestackapi.com/filestack.js"></script>
<script data-cfasync="false" src="{{ elixir('js/all.js') }}"></script>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '373046166427120',
      xfbml      : true,
      version    : 'v2.8'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));


  var pusher = new Pusher('f020037a0fd87c11fa86', {
    cluster: 'ap2',
    encrypted: true
  });

  var openChannel = pusher.subscribe('postNotificationGlobal');
  var channel = pusher.subscribe('notifications-{{Auth::user()['id']}}');

  channel.bind('like', function(data) {
    notification(data);
  });
  channel.bind('rating', function(data) {
    notification(data);
  });
  channel.bind('approvedRating-{{Auth::user()['id']}}', function(data) {
    notification(data);
  });
  channel.bind('comment', function(data) {
    notification(data);
  });
  channel.bind('feedback', function(data) {
    updateSuggestionsRealTime(data);
    notification(data);
  });
  openChannel.bind('postNotification', function(data) {
    postArtNotification(data);
  });
</script>

<script>
  function popupwindow(url, title, w, h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
  } 
</script>
<body>
  <input type="hidden" id="guid" name="" value="{{Auth::user()['id']}}">
  @include('shared.analyticstracking')
  @section('header')
  @include('shared.header')
  @show
  <div class="notification-overlay" onclick="toggleNotificationCenter()"></div>
  <div class="notification-center">
    <div class="heading text-center">
      <h4>
        <button class="pull-right notification-center-close" onclick="toggleNotificationCenter()">
          <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 512 512">
            <path d="M256 48C140.56 48 48 140.56 48 256c0 115.436 92.56 208 208 208 115.435 0 208-92.564 208-208 0-115.44-92.564-208-208-208zm104.002 282.88L330.882 360 256 285.118l-74.88 74.88-29.122-29.117L226.88 256 152 181.12l29.12-29.117L256 226.88l74.88-74.877L360 181.12 285.12 256 360 330.88z"></path></svg>
          </button>
          Notifications
        </h4>
      </div>
      <div id = "notificationItems" class="content">
      </div>
    </div>
    <main class="main">
      @yield('content')
    </main>
    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button onClick="analytics.track('Closed login modal box');" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">
              <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 512 512"><path d="M256 48C140.56 48 48 140.56 48 256c0 115.436 92.56 208 208 208 115.435 0 208-92.564 208-208 0-115.44-92.564-208-208-208zm104.002 282.88L330.882 360 256 285.118l-74.88 74.88-29.122-29.117L226.88 256 152 181.12l29.12-29.117L256 226.88l74.88-74.877L360 181.12 285.12 256 360 330.88z"/></svg>
            </span></button>
            <h4 class="modal-title">Login to ArtMojo</h4>
          </div>
          <div class="modal-body">
            <div class="social-login">
              <a onClick="analytics.track('Clicked on login with facebook from modal'); popupwindow('/redirect', 'windowOpenTab', 626,436)" href="#" class="btn btn-block share btn-facebook social-login-link"><i class="artmojo-icon-facebook hidden-xs"></i> Continue with Facebook</a>
              <a onClick="analytics.track('Clicked on login with google from modal'); popupwindow('/redirect/g', 'windowOpenTab', 626,436)" href="#" class="btn btn-block share btn-google social-login-link"><i class="artmojo-icon-google hidden-xs"></i> Continue with Google</a>
              <div>By signing up, you agree to our <a href="/terms-of-service">Terms of Service.</a></div>
            </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="toast">
      <a class="toast-close">&times;</a>
      <div class="avatar pull-left">
        <img src="/images/suggestion1.png" height="32" />
      </div>
      <div class="toast-meta">
        <p><span class="name">Akshay Sonawane</span> <span class="notification-text">has commented on your photo</span></p>
        <p>
          <small><a class="art-link">View Art</a></small>
          <small><a class="profile-link">View Profile</a></small>
        </p>
      </div>
    </div>
    <a href="/explore" onclick="$(this).removeClass('active')" class="view-new-art"><img src="/images/Party-Poppers-128.png" height="32">View new arts</a>
  </body>
  </html>
