@extends('layouts.app')
@section('title', 'ArtMojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property = "og:description" content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals"/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@artmojoin" />
<meta name = "twitter:description"  content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals" />
<meta property = "og:type" content =  "ArtMojo:craft" />
<meta property  =  "og:title" content = "ArtMojo | Community of artists | Showcase your work and become popular" />
<meta property  =  "og:image" content = "https://artmojo.in/images/fb_banner.png" />
<meta property="fb:app_id" content="373046166427120" />
@endsection
@section('content')
<div class="page-header hidden">
	<?php $cat = str_replace('_', ' ',Request::path()) ?>
	@if(Request::path() === "/")
	<?php
	$cat = "Featured"
	?>
	@endif
	<h1>{{str_replace('Category/', '', $cat)}}</h1>
</div>
<div id="streamline" class="streamline animated" data-columns>
	@foreach($arts as $art)
	@include('/artwork/stream')	
	@endforeach
</div>
<div class="text-center">
	{{ $arts->links() }}
</div>
@endsection