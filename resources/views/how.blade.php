@extends('layouts.app')
@section('title', 'Page Title')
@section('content')

<section class="how-containwer">
    <div class="container">
     <style type="text/css">
        section {
            padding-bottom: 40px;
        }
        .img-holder {
            box-shadow: rgba(0, 0, 0, 0.1) 0px 0px 20px 0px;
            padding: 10px;
            margin-top: 40px;
            background: #FFF;
            border-radius: 4px;
        }
        h2 {
            margin-top: 40px;
        }
    </style>
    <div class="row">
     <div class="col-md-8">
         <div class="img-holder">
             <img src="/images/how/discover.png" class="img-responsive">
         </div>
     </div>
     <div class="col-md-4">
         <h2>Why Discover?</h2>
         <p>
             Discover crafts from 196 countries.
         </p>
     </div>
 </div> 
 <div class="row">
     <div class="col-md-8">
         <div class="img-holder">
         <img src="/images/how/upload.png" class="img-responsive">
         </div>
     </div>
     <div class="col-md-4">
         <h2>Why Upload?</h2>
         <p>To get suggestions from the experts to improve your ability to craft</p>
         <p>Upload your craft to be popular in 196 countries</p>
         <p>Get featured in our trending page</p>
         <p>Chance to be 'Editors pick', which will allow your art to have special page for a day</p>
         <p>Gain advance access to professional features in future</p>
     </div>
 </div> 
 <div class="row">
     <div class="col-md-8">
         <div class="img-holder">
             <img src="/images/how/details.png" class="img-responsive">
         </div>
     </div>
     <div class="col-md-4">
         <h2>What Is Art Details Page?</h2>
         <p>Art details is the page where artist from across the world can come and view, like, comment, share and suggest ideas on your craft.</p>
     </div>
 </div> 
 <div class="row hidden">
     <div class="col-md-8">
         <div class="img-holder">
             <img src="/images/how/details.png" class="img-responsive">
         </div>
     </div>
     <div class="col-md-4">
         <h2>Pro features?</h2>
         <p>Art details is the page where artist from across the world can come and view, like, comment, share and suggest changes on your craft.</p>
     </div>
 </div> 
</div>
</section>


<div class="modal fade" id="whatIsModal" tabindex="-1" role="dialog" aria-labelledby="whatIsModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header hidden">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">What is ArtMojo</h4>
            </div>
            <div class="modal-body">
                <div class="what-modal-content">
                    <div class="white-card">
                        <h2>What?</h2>
                        <p><strong>ArtMojo</strong> is a community for discovering, sharing and monetizing inspiring art powered by creative people around the world.</p>
                    </div>
                    <div class="white-card">
                        <h2>Why?</h2>
                        <p>In todays world there is no way to earn money for artist apart from selling art but what if someone is very much talented and want to grow as artist as well as economically, currently there is no way to do that therefore we have came with the unique solution to the problem and that is <strong>we will be paying artist for uploading and sharing it as many as times</strong> this way we will help someone to grow as an artist. </p>
                    </div>
                    <div class="white-card">
                        <h2>How?</h2>
                        <p>
                            <strong>We will show ad on the art page which will help us pay to the artist.</strong>
                            <br>Artist only need to do is the upload his/her own copyrighted art to get it monetized if we found that the art which has been uploaded is stolen one we will suspend the account right away and we wont pay anything to the artist.
                        </p>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection