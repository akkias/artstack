@extends('layouts.app')
@section('title', $profileName.' | Artmojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property  =  "og:title" content = "{{$profileName}} | ArtMojo | Community of artists" />
<meta property = "og:description" content = "Discover {{$profileName}}'s creative work on ArtMojo."/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@ArtMojoin" />
<meta name = "twitter:description"  content = "Discover {{$profileName}}'s creative work on ArtMojo." />
<meta property = "og:type" content =  "ArtMojo:craft" />
<meta property  =  "og:image" content = "{{$profilePic}}" />
@endsection
@section('content')
<div class="hero profile-hero">
	<figure></figure>
	<div class="profile-share-buttons text-center">
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Pinterest" class="pint" 
		href="https://pinterest.com/pin/create/button/?http_build_query([ 'url' => 'https://artmojo.in/profile/$userId', 'media' => '$profilePic', 'description' => '$description' ]) " target="_blank">
		<i class="artmojo-icon-pinterest-p"></i>
	</a>
	<a data-toggle="tooltip" data-placement="bottom" title="Share on Twitter" class="tw" href="https://twitter.com/intent/tweet?url=https://artmojo.in/profile/{{$userId}}&text=Title&via=artmojoin&hashtags=creative,artmojoin" target="_blank">
		<i class="artmojo-icon-twitter"></i>
	</a>
	<a data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" class="fb" href="https://www.facebook.com/sharer/sharer.php?u=https://artmojo.in/profile/{{$userId}}" target="_blank">
		<i class="artmojo-icon-facebook"></i>
	</a>
</div>
<div class="profile-meta">
	<p class="avatar-holder pull-left"><img src="{{$profilePic}}" class="avatar-img img-responsive" alt="" height = "75" width = "50"></p>
	<span class="name">{{$profileName}}</span>
	@if(Auth::user())
	@if (Auth::user()->id != $userId)
	@if(isFollowingTo(Auth::user()->id , $userId))
	<button class="btn follow ed" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Following</button>
	@else
	<button class="btn follow btn-twitter" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Follow</button>
	@endif
	@endif
	@endif		
	<div class="meta">
		<ul class="list-inline list-unstyled">
			<li><a href="/profile/{{$userId}}"><strong>{{$posts}}</strong>Posts</a></li>
			<li><a href="/profile/{{$userId}}/following"><strong>{{$following}}</strong>Following</a></li>
			<li><a href="/profile/{{$userId}}/followers"><strong>{{$followers}}</strong>Followers</a> </li>
			<li><span><strong>{{$likes}}</strong>Affection</span></li>
			<li><span><strong>{{thousandsCurrencyFormat($views)}}</strong>Art Views</span></li>
			<li><span><strong>{{$apporvedSuggestionsCount}}</strong>Approved suggestions</span></li>
		</ul>
	</div>
</div>
</div>
@if(count($arts) > 0)
<ul class="streamline animated">
	@foreach($arts as $art)
	@include('/artwork/stream')	
	@endforeach
</ul>
@else
<div class="empty-art text-center">
	<div class="empty-content">
		<img src="/images/empty.png" height="250">
		<h2>0 Arts</h2>
		<p>This user has not yet published any art.</p>
	</div>
</div>
@endif
@endsection