@extends('layouts.app')
@section('title', $profileName.' | Artmojo - Be creative together')
@section('content')
<div class="hero profile-hero">
	<figure></figure>
	<div class="profile-share-buttons text-center">
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Pinterest" class="pint" 
		href="https://pinterest.com/pin/create/button/?http_build_query([ 'url' => 'https://artmojo.in/profile/$userId', 'media' => '$profilePic', 'description' => '$description' ]) " target="_blank">
			<i class="artmojo-icon-pinterest-p"></i>
		</a>
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Twitter" class="tw" href="https://twitter.com/intent/tweet?url=https://artmojo.in/profile/{{$userId}}&text=Title&via=artmojoin&hashtags=creative,artmojoin" target="_blank">
			<i class="artmojo-icon-twitter"></i>
		</a>
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" class="fb" href="https://www.facebook.com/sharer/sharer.php?u=https://artmojo.in/profile/{{$userId}}" target="_blank">
			<i class="artmojo-icon-facebook"></i>
		</a>
	</div>
	<div class="profile-meta">
		<p class="avatar-holder pull-left"><img src="{{$profilePic}}" class="avatar-img" alt="" height = "75" width = "50"></p>
		<span class="name">{{$profileName}}</span>
		@if(Auth::user())
			@if (Auth::user()->id != $userId)
			@if(isFollowingTo(Auth::user()->id , $userId))
			<button class="btn follow ed" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Following</button>
			@else
			<button class="btn follow btn-twitter" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Follow</button>
			@endif
			@endif
			@endif
		<div class="meta">
			<ul class="list-inline list-unstyled">
				<li><a href="/profile/{{$userId}}"><strong>{{$posts}}</strong>Posts</a></li>
				<li><a href="/profile/{{$userId}}/following"><strong>{{$following}}</strong>Following</a></li>
				<li><a href="/profile/{{$userId}}/followers"><strong>{{$followers}}</strong>Followers</a> </li>
				<li><span><strong>{{$likes}}</strong>Affection</span></li>
				<li><span><strong>{{thousandsCurrencyFormat($views)}}</strong>Art Views</span></li>
				<li><span><strong>{{$apporvedSuggestionsCount}}</strong>Approved suggestions</span></li>
			</ul>
		</div>
	</div>
</div>
<ul class="streamline stacker-streamline animated">
	@foreach($followData as $user)
	<li class="stream stacker">
		<div class="content">
				@if(Auth::user())
			<div class="follow-action">
			@if (Auth::user()->id != $user[8])
			@if(isFollowingTo(Auth::user()->id , $user[8]))
			<button class="follow ed"  id = "follow{{$user[8]}}" onClick="textUserFollowClick({{$user[8]}},'{{$user[0]}}')">Following</button>
			@else
			<button class="follow"  id = "follow{{$user[8]}}" onClick="textUserFollowClick({{$user[8]}},'{{$user[0]}}')">+ Follow</button>
			@endif
			@endif
			</div>
			@endif
			<div class="avatar-wrapper pull-left"><a href="#"><img src="{{$user[1]}}" height="75" width = "75" class="img-circle" alt=""></a></div>	
			<div class="meta">
				<a href = "/profile/{{$user[8]}}"><h3>{{$user[0]}}</h3></a>
				<ul class="list-inline list-unstyled">
					<li><a href="#"><strong>{{$user[7]}}</strong><small>Posts</small></a></li>
					<li><a href="#"><strong>{{$user[2]}}</strong><small>Following</small></a></li>
					<li><a href="#"><strong>{{$user[3]}}</strong><small>Followers</small></a> </li>
					<li><span><strong>{{$user[5]}}</strong><small>Affection</small></span></li>
					<li class="hidden"><span><strong>{{$user[4]}}</strong><small>Art Views</small></span></li>
				</ul>
			</div>
		</div>
		<a href="#" class="cover" style="background-image:url('/images/gallery/13.jpg')"></a>
	</li>
	@endforeach
	@if(count($followData) == 0)
		<div class="empty-art text-center">
		<div class="empty-content">
			<img src="/images/empty.png" height="250">
			<br/><br/><h2>No data</h2>
		</div>
		</div>

	@endif
	
</ul>
@endsection