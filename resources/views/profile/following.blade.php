@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
<div class="hero profile-hero">
	<figure></figure>
	<div class="profile-share-buttons">
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Pinterest" class="pint" 
		href="https://pinterest.com/pin/create/button/?http_build_query([ 'url' => 'https://artmojo.in/profile/$userId', 'media' => '$profilePic', 'description' => '$description' ]) " target="_blank">
			<i class="artmojo-icon-pinterest-p"></i>
		</a>
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Twitter" class="tw" href="https://twitter.com/intent/tweet?url=https://artmojo.in/profile/{{$userId}}&text=Title&via=artmojoin&hashtags=creative,artmojoin" target="_blank">
			<i class="artmojo-icon-twitter"></i>
		</a>
		<a data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" class="fb" href="https://www.facebook.com/sharer/sharer.php?u=https://artmojo.in/profile/{{$userId}}" target="_blank">
			<i class="artmojo-icon-facebook"></i>
		</a>
	</div>
	<div class="profile-meta">
			@if(Auth::user())
			@if (Auth::user()->id != $userId)
			@if(isFollowingTo(Auth::user()->id , $userId))
			<button class="btn follow ed" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Following</button>
			@else
			<button class="btn follow btn-twitter" type="" id = "follow{{$userId}}" onClick="textUserFollowClick({{$userId}},'{{$profileName}}')">Follow</button>
			@endif
			@endif
			@endif
		<div class="avatar-holder"><img src="{{$profilePic}}" height="175" class="img-circle avatar-img" alt=""></div>
		<div class="meta text-center">
			<h1>{{$profileName}}</h1>
			<ul class="list-inline list-unstyled">
				<li><a href="/profile/{{$userId}}"><strong>{{$posts}}</strong><br>Posts</a></li>
				<li><a href="/profile/{{$userId}}/following"><strong>{{$following}}</strong><br>Following</a></li>
				<li><a href="/profile/{{$userId}}/followers"><strong>{{$followers}}</strong><br>Followers</a> </li>
				<li><span><strong>{{$likes}}</strong><br>Affection</span></li>
				<li class="hidden"><span><strong>{{thousandsCurrencyFormat($views)}}</strong><br>Art Views</span></li>
				<li class="hidden"><span><strong>{{$apporvedSuggestionsCount}}</strong><br>Approved suggestions</span></li>
			</ul>
		</div>
	</div>
</div>
@endsection