@extends('layouts.app')
@section('title', $art->description.' | Artmojo - Be creative together')
<?php
$uri  = $art->images;
$uriPieces = explode("/", $uri);
?>
@section('ogTags')
<meta property="og:site_name" content="artmojo.in" />
<meta property="og:description" content="ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals" />

<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@artmojoin">
<meta name="twitter:creator" content="@artmojoin">
<meta name="twitter:title" content="{{$art->description}}">
<meta name="twitter:description" content="ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals">
<meta name="twitter:image" content="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,height:630/{{$uriPieces[3]}}">

<meta name="pinterest-rich-pin" content="false" />
<meta property="og:url" content="https://www.artmojo.in/suggest/{{$art->id}}" />
<meta property="article:author" content="ArtMojo" />
<meta property="fb:app_id" content="373046166427120" />
<meta property="og:type" content="blog" />
<meta property="og:title" content="{{$art->description}}" />
<meta property="og:image" content="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,height:630/{{$uriPieces[3]}}" />

@endsection
@section('content')
<input type="hidden" name="" value="{{$art->id}}" id="artId">
<div class="suggestion-help">Click anywhere on the image to add suggestion</div>
<div class="hidden">
	<input type = "radio" onclick = "enableRadioClick(this.value)" name = "suggestEnable" value = "enable" id= "suggestionCheck" />Enable for suggestion
	<input type = "radio" onclick = "enableRadioClick(this.value)" name = "suggestEnable" value = "disable" id= "suggestionCheck" checked/>Disable for suggestion
	<button id = "selectButton" onclick = "selectSuggestPos()" disabled>Select position for suggestion</button>		
	<button id = "provideButton" onclick = "provideSuggest()" >Provide suggestion</button>		
	<button id = "addButton" onclick = "addSuggest()" >Add suggestions</button>
	<button id = "clearButton" onclick = "clearSuggest()" >Clear suggestion</button>		
	<select id = "boxSize" onchange = "boxSizeChanged(this.value)">
		<option value = "5">5</option>
		<option value = "10">10</option>
		<option value = "20">20</option>
		<option value = "30">30</option>
		<option value = "40">40</option>
		<option value = "50">50</option>
		<option value = "75">75</option>
		<option value = "100">100</option>
	</select>
</div>
</div>
<div class="comment-tooltip">
	<label>Add suggestion</label>
	<textarea autofocus id = "suggestionForArt" placeholder="Write suggestion or provide reference URL" rows="3" class="form-control " name=""></textarea>
	<button id ="modalSubmit" type="" class="btn btn-primary pull-right" onclick = "addSuggest()">Submit</button>
	<button data-dismiss="modal" onclick="hideCommentTooltip()" type="" class="btn btn-link pull-right">Cancel</button>
</div>

<div class="feedback-actions hidden clearfix">
	<a onclick="ga('send', 'event', '/', 'Logo clicked');" class="suggest-brand" href="/">
		<img height="40" src="/images/artmojo_logo1.png" alt="ArtMojo">
	</a>
	<div class="art-desc">Art Description: {{$art->description}}</div>
	<div class="social-share-btns clearfix pull-right">
		<a href="#" class="action pull-right report hidden">Report</a>
	</div>
</div>
<div class="suggest-toolbar clearfix">
	<a onclick="ga('send', 'event', '/', 'Logo clicked');" class="suggest-logo pull-left" href="/">
		<img height="32" src="/images/artmojo_logow.png" alt="ArtMojo">
	</a>
	<ul class="nav navbar-nav menu">
		<li class="{{ Request::is( '/','explore') ? 'active' : '' }}"><a href="/explore" onClick="analytics.page('Opened explore page');">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="20" height="20">
				<path style="text-indent:0;text-align:start;line-height:normal;text-transform:none;block-progression:tb;-inkscape-font-specification:Bitstream Vera Sans" d="M 16 4 C 9.3844277 4 4 9.3844277 4 16 C 4 22.615572 9.3844277 28 16 28 C 22.615572 28 28 22.615572 28 16 C 28 9.3844277 22.615572 4 16 4 z M 14.96875 6.0625 C 14.978917 6.0614718 14.989824 6.0634979 15 6.0625 L 15 7 L 17 7 L 17 6.0625 C 21.738858 6.5272264 25.472774 10.261142 25.9375 15 L 25 15 L 25 17 L 25.9375 17 C 25.472774 21.738858 21.738858 25.472774 17 25.9375 L 17 25 L 15 25 L 15 25.9375 C 10.261142 25.472774 6.5272264 21.738858 6.0625 17 L 7 17 L 7 15 L 6.0625 15 C 6.5262285 10.271318 10.244299 6.5403141 14.96875 6.0625 z M 22.5 9.5 L 14.15625 14.15625 L 9.5 22.5 L 17.84375 17.84375 L 22.5 9.5 z M 16 14.5 C 16.828 14.5 17.5 15.172 17.5 16 C 17.5 16.828 16.828 17.5 16 17.5 C 15.172 17.5 14.5 16.828 14.5 16 C 14.5 15.172 15.172 14.5 16 14.5 z" color="#000" overflow="visible" font-family="Bitstream Vera Sans"/>
			</svg>
			Explore</a></li>
			<li class="{{ Request::is( 'quest') ? 'active' : '' }}"><a href="/quest" onClick="analytics.page('Opened quest page');">

				<?xml version="1.0" encoding="iso-8859-1"?>
				<!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="16px" height="16px">
					<g>
						<g>
							<path d="M226.932,399.948c-19.96,18.445-47.567,22.576-72.053,10.786c-8.852-4.263-16.322-10.149-22.17-17.199l-33.341,73.744    c-1.517,3.355,0.177,5.884,0.975,6.815c0.798,0.93,3.039,2.989,6.585,2.003l24.272-6.756c2.766-0.769,5.562-1.14,8.319-1.14    c11.631,0,22.578,6.583,27.849,17.492l10.962,22.685c1.601,3.315,4.604,3.646,5.854,3.621c1.226-0.016,4.242-0.414,5.758-3.769    l53.033-117.304C237.148,392.603,231.63,395.606,226.932,399.948z" fill="#FFFFFF"/>
						</g>
					</g>
					<g>
						<g>
							<path d="M412.631,467.279l-33.341-73.744c-5.848,7.051-13.318,12.937-22.17,17.199c-24.487,11.79-52.093,7.659-72.053-10.786    c-4.698-4.342-10.216-7.345-16.045-9.022l53.033,117.304c1.517,3.356,4.533,3.753,5.758,3.769c1.25,0.025,4.253-0.306,5.854-3.621    l10.962-22.685c5.27-10.909,16.218-17.492,27.849-17.492c2.757,0,5.554,0.371,8.319,1.14l24.272,6.756    c3.546,0.987,5.788-1.072,6.585-2.003C412.454,473.162,414.148,470.633,412.631,467.279z" fill="#FFFFFF"/>
						</g>
					</g>
					<g>
						<g>
							<path d="M438.821,207.791c-27.69-18.96-36.282-56.605-19.56-85.702c10.051-17.491,4.82-34.775-3.427-45.118    c-8.248-10.34-23.936-19.285-43.223-13.38c-32.084,9.827-66.877-6.925-79.201-38.141C286.002,6.686,269.227,0,256,0    c-13.227,0-30.002,6.686-37.41,25.451c-12.324,31.217-47.114,47.967-79.201,38.141c-19.289-5.904-34.974,3.039-43.223,13.38    c-8.247,10.343-13.478,27.625-3.427,45.118c16.722,29.096,8.13,66.742-19.56,85.702c-16.646,11.399-19.431,29.24-16.489,42.136    c2.942,12.896,13.194,27.761,33.137,30.808c33.174,5.068,57.248,35.256,54.809,68.727c-1.468,20.121,10.745,33.423,22.662,39.163    c11.918,5.739,29.932,6.995,44.748-6.698c12.322-11.387,28.141-17.083,43.953-17.083c15.818,0,31.628,5.693,43.952,17.083    c14.818,13.694,32.833,12.438,44.75,6.698c11.917-5.739,24.129-19.041,22.662-39.162c-2.439-33.471,21.635-63.659,54.809-68.728    c19.943-3.047,30.193-17.913,33.137-30.808C458.252,237.03,455.465,219.189,438.821,207.791z M256,335.923    c-72.575,0-131.619-59.044-131.619-131.619S183.424,72.684,256,72.684c72.576,0,131.619,59.044,131.619,131.619    C387.618,276.878,328.575,335.923,256,335.923z" fill="#FFFFFF"/>
						</g>
					</g>
					<g>
						<g>
							<path d="M255.999,97.225c-59.044,0-107.079,48.036-107.079,107.079c0,59.043,48.034,107.079,107.079,107.079    s107.079-48.036,107.079-107.079S315.043,97.225,255.999,97.225z M310.874,193.922l-66.642,48.675    c-2.115,1.545-4.653,2.362-7.237,2.362c-0.666,0-1.335-0.054-2.001-0.164c-3.249-0.537-6.147-2.358-8.041-5.054l-19.934-28.382    c-3.895-5.547-2.556-13.2,2.989-17.095c5.546-3.895,13.198-2.557,17.094,2.989l12.75,18.154l56.548-41.302    c5.473-3.995,13.15-2.803,17.146,2.671C317.543,182.248,316.346,189.924,310.874,193.922z" fill="#FFFFFF"/>
						</g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
				</svg>
				Quest</a></li>
				<li class="{{ Request::is( 'features') ? 'active' : '' }}"><a href="/features" onClick="analytics.page('Opened how it works page');"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M432 80v352H80V80h352m16-16H64v384h384V64z"/><path d="M192 152h192v16H192zM192 248h192v16H192zM192 344h192v16H192z"/><circle cx="144" cy="160" r="16"/><circle cx="144" cy="256" r="16"/><circle cx="144" cy="352" r="16"/></svg> Features</a></li>
			</ul>
			

			<button class="action pull-right stats hide"  onClick="$('#artStatisticModal').modal();">
				<svg height="20" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M403.1 108.9c-81.2-81.2-212.9-81.2-294.2 0s-81.2 212.9 0 294.2c81.2 81.2 212.9 81.2 294.2 0 81.2-81.2 81.2-213 0-294.2zm-281.9 12.3c74.3-74.3 195.3-74.3 269.6 0 38 38 56.5 88.1 55.7 138-2.9-4.6-23.1-35.2-52.5-35.2-27.9 0-42.3 26.1-51.8 43.3-1.4 2.6-2.8 5-4 7.1-11.6 19.5-27.7 30.4-43.1 29.3-13.6-1-25.2-11.3-32.6-29.2-9.3-22.4-29.6-46.5-53.7-49.9-11.4-1.6-28.6.9-45.3 21.7-3.3 4.1-7 9.5-11.2 15.9-10.6 15.7-26.5 39.4-38.7 41.4-21 3.4-36.6-12.2-39.3-14.6-2-1.7-4.4-4.3-7.3-7.6-7.5-56.9 10.5-116.6 54.2-160.2zm269.6 269.6c-74.3 74.3-195.3 74.3-269.6 0-24.2-24.2-40.5-53.3-48.9-84.1 7 5.7 19.3 13.3 35.5 13.3 2.7 0 5.6-.2 8.5-.7 19-3.1 35.8-28.1 49.4-48.2 3.9-5.8 7.5-11.2 10.4-14.8 9.7-12 20-17.4 30.6-15.9 12.9 1.8 31.1 16.2 41.1 40.2 9.9 23.7 26.3 37.6 46.3 39 21.6 1.5 43.3-12.4 58-37.1 1.4-2.3 2.8-4.8 4.2-7.5 8.6-15.6 19.3-35.1 37.7-35.1 11.1 0 20.9 7.3 27.2 13.4 7.4 7.2 9 9 12.9 14.6 3.4 4.8 6.5 8.7 10.3 16.7-5.8 38.9-23.7 76.3-53.6 106.2z"/></svg>Statistics
			</button>

<!-- <button class="action pull-right disable-sgst" style = "display:block !important;" onClick="getArtStat({{$art->id}})">Statistics</button>
-->

@if($rating)
<script>
	var rating = {{$rating}};
	if(rating > 0){
		setTimeout(function (){
			$("#rateYo").rateYo("option", "rating", rating);			
		}, 500);

	}
</script>
@endif
</div>
<div class="artwork-container clearfix">
	<section class="artwork" id="artwork">
		<div class="modal fade" id="deleteConfirmModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						Are you sure you want to delete this post?
					</div>
					<div class="modal-footer">
						<button data-dismiss="modal" id = "modalCancel" type="" class="btn">Cancel</button>
						<button id ="modalSubmit" type="" class="btn btn-twitter" onclick = "deleteArt({{$art->id}})">Ok</button>
					</div>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

		<div class="modal fade" id="artStatisticModal" tabindex="-1" role="dialog" aria-labelledby="statisticModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body modal-white-back" >
						<style type="text/css">
							.modal-white-back{
								background:white !important;
							}
							.stat-row {
								margin-bottom: 10px;
							}
							.stat-row img {
								margin-right: 5px;
							}
							.stat-row .country-wrapper {
								min-width: 250px;
								display: inline-block;
								padding-left: 25px;
							}
							.stat-row span {
								margin-left: 25px;
							}
						</style>
						<div class="hide">
							{{$art->view_count + $art->likes_count}}
						</div>


						@if(count(getArtStatistics($art->id)) == 0)
						No Data
						@endif
					</div>
					<div class="modal-footer hide">
						<button data-dismiss="modal" id = "modalCancel" type="" class="btn">Close</button>
					</div>

				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>
		<span id="suggestImgSrcContainer" data-img-src="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,width:1680,height:920/{{$uriPieces[3]}}"></span>
		<div class="tbl">
			<div class="tblr">
				@if($prevId = prevArtId($art->id))
				<a class="prev" href="/suggest/{{$prevId}}" >
					<span class="arrow-wrapper">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="30" height="46" viewBox="0 0 30 46">
							<path d="M25.767,39.039 C26.537,39.785 26.494,41.049 25.670,41.848 C24.846,42.646 23.545,42.689 22.775,41.943 L6.492,25.993 L6.492,25.993 L5.831,25.351 L3.500,23.089 L6.492,20.185 L6.492,20.185 L22.945,4.070 C23.728,3.311 25.040,3.343 25.864,4.141 C26.688,4.941 26.721,6.215 25.937,6.974 L9.542,23.064 L25.767,39.039 Z" class="arrow"/>
						</svg>
					</span>
				</a>
				@endif
				@if($nextId = nextArtId($art->id))
				<a class = "next" href="/suggest/{{$nextId}}">
					<span class="arrow-wrapper">
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid" width="30" height="46" viewBox="0 0 30 46">
							<path d="M4.335,7.115 C3.572,6.376 3.616,5.125 4.432,4.333 C5.248,3.543 6.536,3.500 7.299,4.239 L23.427,20.036 L23.427,20.036 L24.082,20.671 L26.391,22.912 L23.427,25.788 L23.427,25.788 L7.130,41.748 C6.355,42.500 5.055,42.468 4.240,41.678 C3.423,40.886 3.391,39.624 4.166,38.872 L20.406,22.937 L4.335,7.115 Z" class="arrow"/>
						</span>
					</a>
					@endif
					<div class="tbltd">
						<div class="loader"></div>
						<canvas id="myCanvas">
							<img class="art" data-id="{{$art->id}}" id = "artPic">
						</div>
					</div>
				</div>
				<div ></div>
			</section>
			<aside class="aside hidden-xs">
				@if($art->description)
				<div class="art-description clearfix">{{$art->description}}</div>
				@endif
				<div class="clearfix user">
					<div class="avatar-wrapper">
						<img class="img-circle" src="{{getProfilePicById($art->user_id)}}" alt="" height="36" width = "36">
					</div>
					<div class="details">
						@if (Auth::user() && Auth::user()->id == $art->user_id)
						<button class="btn btn-delete btn-xs pull-right" onClick = "$('#deleteConfirmModal').modal()">Delete art</button>
						@endif
						<a href="/profile/{{$art->user_id}}">{{$art->name}}</a><br>
						<small class="text-muted timestamp">{{$art->created_at}}</small>
						@if (Auth::user() && Auth::user()->id != $art->user_id)
						@if(!isFollowingTo(Auth::user()->id , $art->user_id))
						<button class="btn sub-btn" type="" id = "follow{{$art->user_id}}" 
							onClick="textUserFollowClick({{$art->user_id}}, '{{$art->name}}')">Follow</button>
							@endif
							@endif
						</div>
					</div>
					<div class="reactions hidden clearfix">
						<button data-toggle="tooltip" data-placement="top" title="Add to favourites" type="button" class="fav ed">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
						<button data-toggle="tooltip" data-placement="top" title="Love" type="button" class="loved">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
						<button data-toggle="tooltip" data-placement="top" title="Amazing" type="button" class="amazing">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
						<button data-toggle="tooltip" data-placement="top" title="Master" type="button" class="master">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
						<button data-toggle="tooltip" data-placement="top" title="Wow" type="button" class="wow">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
						<button data-toggle="tooltip" data-placement="top" title="Rock" type="button" class="rock">
							<span></span>
							<i class="img-circle ticked"></i>
						</button>
					</div>
					<div class="comments">
						<div role="tabpanel">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs nav-justified " role="tablist" >
								<li role="presentation" class="active">
									<a href="#suggestionsTab" aria-controls="suggestionsTab" role="tab" data-toggle="tab">Suggestions <span class="badge suggestBadge">{{count($suggests)}}</span></a>
								</li>
								<li role="presentation" >
									<a href="#commentsTab" onclick = "clearSuggest();" aria-controls="commentsTab" role="tab" data-toggle="tab">Comments <span class="badge commentBadge">{{count($comments)}}</span></a>
								</li>
							</ul>
							<!-- Tab panes -->
							<div class="tab-content comments-and-suggestions">
								<div role="tabpanel" class="tab-pane " id="commentsTab">
									@if(Auth::user())
									<div class="comment-form clearfix">
										<div class="avatar">
											<img class="img-circle" src="{{$profilePic}}" alt="" height="24" width = "24">
										</div>
										<form id = "commentForm" accept-charset="utf-8">
											<input type = "hidden" name = "artId" value = "{{$art->id}}"/>
											<textarea autofocus rows="1" type="text" id = "commentArea" class="comment-form-textarea autoheight" name="commentArea" placeholder="Add a comment or reference url"></textarea>
											<input id="commentSubmit" class="btn btn-twitter btn-sm pull-right" type = "submit" value = "Submit" disabled  />
										</form>
									</div>
									@endif
									<div class="comments-list art-comments-list" id="commentsList">
										@foreach($comments as $comment)
										<div class="comment" id = "commentDiv{{$comment->id}}">
											<div class="avatar">
												@if($loginBy == "g")
												<img class="img-circle" src="{{$comment->g_pic}}" alt="" height="24" width="24">
												@else
												<img class="img-circle" src="{{$comment->fb_pic}}" alt="" height="24" width="24">
												@endif
											</div>
											<div class="comment-data">
												<p>
													<a href="/profile/{{$comment->user_id}}">{{$comment->name}}</a> 
													<span class="text-muted comment-timestamp">{{$comment->created_at}}</span>
												</p>
												{!!nl2br(e($comment->text))!!}
											</div>
											@if(isValidUserToDeleteComment($art->id, $comment->id))
											<button class="delete" onClick ="commentDeleteClick({{$comment->id}})">
												<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg>
											</button>
											@endif
										</div>
										@endforeach
									</div>
								</div>
								<div id="checkSuggestionsTab{{Auth::user()['id']}}{{$art->id}}"></div>
								<div role="tabpanel" class="tab-pane active" id="suggestionsTab">
									<div class="suggestion-sorter hidden">
										Show: <a class="active" href="#">All</a>
										<a href="#">Bookmarked</a>
									</div>
									<div class="comments-list suggest-list" id= "suggests-list">
										<a href="/suggest/{{$art->id}}" class="refresh-suggestions text-center">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 512 512"><path d="M256 388c-72.597 0-132-59.405-132-132 0-72.6 59.403-132 132-132 36.3 0 69.3 15.4 92.406 39.6L278 234h154V80l-51.698 51.702C348.406 99.798 304.406 80 256 80c-96.797 0-176 79.203-176 176s78.094 176 176 176c81.045 0 148.287-54.134 169.4-128h-46.55c-18.745 49.56-67.138 84-122.85 84z"/></svg>
											See New Suggestions</a>
											@foreach($suggests as $suggest)
											<div class="comment" onmouseout="clearSuggest()" onmouseover ="artSuggestClick({{$suggest->x}},{{$suggest->y}})" id = "suggestDiv{{$suggest->id}}">
												<div class="avatar">
													@if($loginBy == "g")
													<img class="img-circle" src="{{$suggest->g_pic}}" alt="" height="24" width = "24">		
													@else
													<img class="img-circle" src="{{$suggest->fb_pic}}" alt="" height="24" width = "24">		
													@endif
													@if (Auth::user() && $suggest->approved)
													<svg data-toggle="tooltip" data-placement="right" title="Approved by uploader" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M387.58 139.712L356.756 109 216.913 248.32l30.83 30.718 139.838-139.326zM481.173 109L247.744 340.47l-91.39-91.052-30.827 30.715L247.744 403 512 139.712 481.172 109zM0 280.133L123.32 403l30.83-30.713-122.216-122.87L0 280.134z"/></svg>
													@endif
												</div>
												<div class="comment-data">
													<p>
														<a href="/profile/{{$suggest->user_id}}">{{$suggest->name}}</a>
														<span>&middot;</span>
														<span><small class="text-muted comment-timestamp">{{$suggest->created_at}}</small></span>
													</p>
													<p>{!!nl2br(e($suggest->text))!!}</p>
												</div>
												@if(isValidUserToDeleteSuggestion($art->id, $suggest->id))
												<button class="delete" onClick ="suggestDeleteClick({{$suggest->id}})">
													<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M405 136.798L375.202 107 256 226.202 136.798 107 107 136.798 226.202 256 107 375.202 136.798 405 256 285.798 375.202 405 405 375.202 285.798 256z"/></svg>
												</button>
												@endif
												<div class="comment-actions suggest-approve-section">
													
														@if(Auth::user())
														@if(checkIfAlreadyUpvoted(Auth::user()['id'], $suggest->upvotes))
														<span id = "upvoteSpan{{$suggest->id}}" class="is-upvoted">
															<svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
															<path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
															C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
															c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
															c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
															C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
															<path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
															s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
															<small class="is-upvoted">
																<span id = "upvoteSpanCount{{$suggest->id}}">{{$suggest->upvoteCount}}</span>
															</small>
														</span>
														@else
														<span id = "upvoteSpan{{$suggest->id}}" hidden>
															<svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
															<path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
															C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
															c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
															c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
															C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
															<path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
															s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
															<small class="is-upvoted">
																<span id = "upvoteSpanCount{{$suggest->id}}">{{$suggest->upvoteCount}}</span>
															</small>
													</span>

													<a href="#" id = "upvote{{$suggest->id}}" onclick = "upvoteSuggestionClick({{$suggest->id}})"> 
														<svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
															<path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
															C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
															c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
															c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
															C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
															<path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
															s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
														<small class="">
																<span id = "upvoteSpanCount{{$suggest->id}}">{{$suggest->upvoteCount}}</span>
															</small>&nbsp;		
													<small class="middle">Upvote</small>
												</a>

												@endif
												@else
													<svg height="12" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
															<path d="M495.969,214.768l-24.695-14.225c-9.711-5.598-20.727-8.543-31.945-8.543H368c-8.836,0-16-7.164-16-16V43.84
															C352,28.465,339.531,16,324.156,16h-0.094c-11.977,0-22.617,7.668-26.406,19.035l-30.719,80.355
															c-7.117,18.611-19.844,34.547-36.422,45.598L160,208v240h14.273c11.602,0,22.977,3.152,32.93,9.121l49.594,29.758
															c9.953,5.969,21.328,9.121,32.93,9.121h136.938c13.852,0,27.32-4.492,38.398-12.801l34.141-25.6
															C507.258,451.557,512,442.072,512,432V242.496C512,231.053,505.891,220.48,495.969,214.768z"/>
															<path d="M96,176H0v304h96c17.672,0,32-14.328,32-32V208C128,190.326,113.672,176,96,176z M64,448c-17.672,0-32-14.328-32-32
															s14.328-32,32-32s32,14.328,32,32S81.672,448,64,448z"/></svg>
															<small class="">
														<span id = "upvoteSpanCount{{$suggest->id}}">{{$suggest->upvoteCount}}</span>
													</small>
													
												@endif

												<br>
												@if (Auth::user() && Auth::user()->id == $art->user_id && !$suggest->approved)
													<a href="#"  id = "approveSuggest{{$suggest->id}}" onclick = "suggestionApproveClick({{$suggest->id}})">
														Approve suggestion</a>
														@endif												
											</div>
										</div> 
										@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
				</aside>
			</div>
			<!-- Latest compiled and minified CSS -->
			<link data-cfasync="false" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
			<!-- Latest compiled and minified JavaScript -->
			<script data-cfasync="false" src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
			<script data-cfasync="false" src= "/js/suggest.js"></script>
			@endsection