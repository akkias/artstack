@extends('layouts.app')
@section('title', 'Edit your art | Artmojo - Be creative together')
@section('content')
<div class="compose-wrapper">
	<form id = "artForm">
		<div class="compose-area">
			
			<div class="attached-image" style = "display:block;">
				<button onclick="removeAttachedPhoto(1)" class="delete hidden" type=""><i class="ion-md-close"></i></button>
				<img src="{{$art->images}}"  alt="" class="img-responsive" id="uploadedArt">
			</div>
			<div id="localDropResult"></div>
			<div class="textarea-wrapper">
				<textarea class="form-control" id="artDescriptionTextArea" rows="3" name="artDesc" placeholder="Tell us more about your beautiful art">{{$art->description}}</textarea>
			</div>
			<input type = "hidden"  name = "artId" value = "{{$art->id}}"/>
			<input type = "hidden" id="mode" name = "postMode" value = "edit"/>
			<input type = "hidden"  name = "imageUrl" id="fileName" value = "{{$art->images}}"/>
			<input type = "hidden" id = "categoryVal" value = "{{$art->category}}"/>
			<div class="categories-wrapper hidden">
				<select name="artType" id="artCategory" >
					<option value="-1">Choose one</option>
					<option value="1" >Art</option>
					<option value="2" >Digital Art</option>
					<option value="3" >Drawings</option>
					<option value="4" >Paintings</option>
					<option value="5" >Traditional Art</option>
					<option value="6" >Illustration</option>
					<option value="7" >Design</option>
					<option value="8" >Architecture</option>
					<option value="9" >Photography</option>
					<option value="10" >Sculpting</option>
					<option value="11" >Ceramics & Pottery</option>
					<option value="12" >Cartoons & Comics</option>
				</select>
			</div>
			<script>
				document.getElementById("artCategory").value = document.getElementById("categoryVal").value;
			</script>
			<div class="compose-actions-wrapper clearfix">
				<input id="postArtBtn" type = "submit" class="post pull-right btn btn-twitter" value = "Publish Art" />
				<button onclick="openPostCreator()" class="cancel pull-right hidden" type="button">
					<span>Cancel</span><i class="ion-md-close"></i>
				</button>
				<div class="pull-left">
					<div class="checkbox">
						<label>
						@if($art->nsfw)
						<input type="checkbox" name="artNSFW" checked/>
						@else
						<input type="checkbox" name="artNSFW" />
						@endif
						This photo contains nudity, sexually explicit or suggestive content.</label>
					</div>
					<div class="checkbox">
						<label>
						@if($art->enableSuggestions)
						<input type="checkbox" name="enableSuggestions" checked />
						@else
						<input type="checkbox" name="enableSuggestions"/>
						@endif
						Enable suggestions.
						</label>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection