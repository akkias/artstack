<div class="stream">
	<div class="stream-body clearfix">
		<a href="/suggest/{{$art->id}}">
			<?php
			$uri  = $art->images;
			$uriPieces = explode("/", $uri);
			?>
			@if($art->nsfw)
			<img alt="Adult art" class="img-responsive" src="/images/nsfw.png">
			@else
			<img alt="{{$art->description}}" src="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=width:800/{{$uriPieces[3]}}" class="img-responsive">
			@endif
		</a>
		@if($art->description)
		<p class="desc hidden">{{$art->description}}</p>
		@endif
	</div>
	<div class="stream-footer hidden clearfix">
		<span class="views ">{{thousandsCurrencyFormat($art->view_count)}} Views</span>
		<span class="views">{{$art->suggest_count}} Suggestions</span>
	</div>
	<div class="overlay">
		<a onClick="analytics.track('Opened art #{{$art->id}}');" href="/suggest/{{$art->id}}">&nbsp;</a>
		<span class="pull-left views hidden">
			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M14 12c0 1.103-.897 2-2 2s-2-.897-2-2 .897-2 2-2 2 .897 2 2zm10-.449s-4.252 7.449-11.985 7.449c-7.18 0-12.015-7.449-12.015-7.449s4.446-6.551 12.015-6.551c7.694 0 11.985 6.551 11.985 6.551zm-8 .449c0-2.208-1.791-4-4-4-2.208 0-4 1.792-4 4 0 2.209 1.792 4 4 4 2.209 0 4-1.791 4-4z"/></svg>
			{{thousandsCurrencyFormat($art->view_count)}}
		</span>
			@if($art->rating)
			<span class="rated pull-right">
				{{number_format($art->rating, 1)}}
			</span>
			@endif

			<span class="rated ">
			{{$art->suggest_count}} Suggestions
			</span>

		<div class="btm opaque">
			<div class="btm-actions pull-right">
				@if(Auth::user())
				@if(isArtLiked(Auth::user()->id, $art->id))
				<span data-toggle="tooltip" data-placement="top" title="Love" class="pull-right btn-action-btn love ed" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}})">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
					<span class="count">{{$art->likes_count}}</span>
				</span>
				@else
				<span data-toggle="tooltip" data-placement="top" title="Love" class="pull-right btn-action-btn love" id="love{{$art->id}}" onclick="artLikeClick({{$art->id}})">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
					<span class="count">{{$art->likes_count}}</span>
				</span>
				@endif
				@else 
				<span title="Love" class="pull-right btn-action-btn love" id="love{{$art->id}}" data-toggle="modal" data-target="#loginModal">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z"/></svg>
					<span class="count">{{$art->likes_count}}</span>
				</span>
				@endif
			</div>
			<div class="author">
				<span class="avatar">
					<a href="">
				      <img alt="{{$art->name}}" class="img-circle" src="{{getProfilePicById($art->user_id)}}" width="40" height="40" />
						<a href = "/profile/{{$art->user_id}}"><span title = "{{$art->name}}">{{$art->name}}</span></a>
						@if (Auth::user() && Auth::user()->id != $art->user_id)
						@if(!isFollowingTo(Auth::user()->id , $art->user_id))
						<button id = "follow{{$art->user_id}}" onClick="userFollowClickIcon({{$art->user_id}},'{{$art->name}}')" data-toggle="tooltip" data-placement="top" title="Follow" class="btn-follow follow{{$art->user_id}}">
							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 512 512"><path d="M416 277.333H277.333V416h-42.666V277.333H96v-42.666h138.667V96h42.666v138.667H416v42.666z"/></svg>
						</button>
						@endif	
						@endif
					</a>
				</span>
			</div>
		</div>
	</div>
</div>