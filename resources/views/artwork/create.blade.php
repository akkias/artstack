@extends('layouts.app')
@section('title', 'Upload your creative work | Artmojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property = "og:description" content = "ArtMojo | Upload your creative works and get discovered on ArtMojo"/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@artmojoin" />
<meta name = "twitter:description"  content = "ArtMojo | Upload your creative works and get discovered on ArtMojo" />
<meta property = "og:type" content =  "artstack:craft" />
<meta property =  "og:title" content = "Upload your creative works and get discovered on ArtMojo" />
<meta property  =  "og:image" content = "https://artmojo.in/images/fb_banner.png" />
<meta property="fb:app_id" content="373046166427120" />

<script src="https://static.filestackapi.com/v3/filestack-0.2.0.js"></script>

@endsection
@section('content')
<div class="compose-wrapper">

	<form id = "artForm">
		<div class="compose-area">
			<div class="upload-image-form-wrapper text-center" id="artStackDropZone">
				<i class="ion-ios-images-outline"></i>
				<h2>Drag and Drop your craft here</h2>
				<h4>OR</h4>
				<input type = "hidden" id="fileName" name = "imageUrl" value = ""/>
				<button class = "fp__btn" onclick ="filePick()">Pick File</button>
				 
				 <input type="filepicker" class = "hide" data-fp-multiple="false" data-fp-apikey="Aymg5zYafQHmriVxQiyeKz" />
				<input type="hidden" name="imageLGUrl" class="image-lg-url">
				<input type="hidden" name="imageMDUrl" class="image-md-url">
				<h4 class="pick-file-info">You can also import photos from other sources by clicking on "Pick File":</h4>
				<div class="source">
					<i class="icon-facebook"></i>
					<i class="icon-instagram"></i>
					<i class="icon-flickr"></i>
					<i class="icon-dropbox"></i>
				</div>
			</div>
			<input type = "hidden" id="mode" name = "postMode" value = "create"/>
			<div class="progress hidden">
				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
					60%
				</div>
			</div>
			<div class="attached-image">
				<button onclick="removeAttachedPhoto(1)" class="delete hidden" type=""><i class="ion-md-close"></i></button>
				<img src="" alt="" class="img-responsive" id="uploadedArt">
			</div>
			<div id="localDropResult"></div>
			<div class="textarea-wrapper">
				<textarea class="form-control" id="artDescriptionTextArea" rows="3" name="artDesc" placeholder="Tell us more about your beautiful art"></textarea>
			</div>

			<div class="categories-wrapper hidden">
				<select name="artType" id="artCategory">
					<option value="-1">Choose one</option>
					<option value="1">Art</option>
					<option value="2">Digital Art</option>
					<option value="3">Drawings</option>
					<option value="4">Paintings</option>
					<option value="5">Traditional Art</option>
					<option value="6">Illustration</option>
					<option value="7">Design</option>
					<option value="8">Architecture</option>
					<option value="9">Photography</option>
					<option value="10">Sculpting</option>
					<option value="11">Ceramics & Pottery</option>
					<option value="12">Cartoons & Comics</option>
				</select>
			</div>
			<div class="pull-left">
				<div class="checkbox">
					<label><input type="checkbox" name="artNSFW"/> This photo contains nudity, sexually explicit or suggestive content.</label>
				</div>
				<div class="checkbox">
					<label><input type="checkbox" checked name="enableSuggestions"/> Enable suggestions.</label>
				</div>
			</div>
			<div class="compose-actions-wrapper clearfix">
				<input onClick="analytics.track('Art published by {{Auth::user()->name}}');" id="postArtBtn" type = "submit" class="post pull-right btn btn-primary" value = "Publish Art" />
			<!-- <button class="post pull-right btn btn-primary" type="button">
				<span>Publish Art</span><i class="ion-ios-arrow-forward"></i>
			</button> -->
			<button onclick="openPostCreator()" class="cancel pull-right hidden" type="button">
				<span>Cancel</span><i class="ion-md-close"></i>
			</button>
		</div>
	</div>
</form>
</div>
<style type="text/css">.fp__btn{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:inline-block;height:34px;padding:4px 30px 5px 40px;position:relative;margin-bottom:0;vertical-align:middle;-ms-touch-action:manipulation;touch-action:manipulation;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;font-family:"Open Sans", sans-serif;font-size:12px;font-weight:600;line-height:1.42857143;color:#fff;text-align:center;white-space:nowrap;background:#ef4925;background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAVCAYAAABLy77vAAAABGdBTUEAALGPC/xhBQAAAJRJREFUOBHNUcEWgCAIy14fbl9egK5MRarHQS7ocANmOCgWh1gdNERig1CgwPlLxkZuE80ndHlU+4Lda1zz0m01dSKtcz0h7qpQb7WR+HyrqRPxahzwwMqqkEVs6qnv+86NQAbcJlK/X+vMeMe7XcBOYaRzcbItUR7/8QgcykmElQrQPErnmxNxl2yyiwcgEvQUocIJaE6yERwqXDIAAAAASUVORK5CYII=");background-repeat:no-repeat;background-position:15px 6px;border:1px solid transparent;border-radius:17px}.fp__btn:hover{background-color:#d64533}.fp__btn::after{position:absolute;content:"";top:15px;right:14px;width:7px;height:4px;background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAICAYAAAA1BOUGAAAABGdBTUEAALGPC/xhBQAAAGlJREFUCB1j/P//vw4DA4MiEKOD+0xAkatA/AJNBsS/ysTIyPgfyDgHxO+hCkD0Oag4RAhoPDsQm4NoqCIGBiBnAhBjAxNAkkxAvBZNFsQHuQesmxPIOQZVAKI54UZDFYgABbcBsQhMAgDIVGYSqZsn6wAAAABJRU5ErkJggg==");}.fp__btn:hover::after{background-position:0 -4px;}.fp__btn:active,.fp__btn:focus{outline:none}@media only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2){.fp__btn{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAqCAYAAADbCvnoAAAABGdBTUEAALGPC/xhBQAAAQFJREFUWAntWEESwjAIbBwfHl+upNoRNjKUJhk5kIvZQGG7bHOwPGltgdYtEJedShKyJnLHhEILz1Zi9HCOzFI7FUqFLAWseDgPdfeQ9QZ4b1j53nstnEJJyBqx20NeT1gEMB5uZG6Fzn5lV5UMp1ASQhMjdnvoqjewsYbDjcytEH5lsxULp1AS0sx8nJfVnjganf3NkVlKhVPIfQ9Zb6jF0atK3mNriXwpicPHvIeyr3sTDA53VgpgH8BvMu1ZCCz7ew/7MPwlE4CQJPNnQj2ZX4SYlEPbVpsvKFZ5TOwhcRoUTQiwwhVjArPEqVvRhMCneMXzDk9lwYphIwrZZOihF32oehMAa1qSAAAAAElFTkSuQmCC");background-size:18px 21px}.fp__btn::after{background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAQCAYAAAAmlE46AAAABGdBTUEAALGPC/xhBQAAANpJREFUKBWVkU8KglAYxJ/u3HuBwmUX8BqepKN4ka4RguDOVYu2QVCrhIJ6/caekqLiGxi+PzPD58PAWrszxmygD84h7hpePFLy1mEQBJamgvcVYXkqZXTR0LwpJWw0z0Ba6bymDcrI4kkp4EvzCNoVztNKfVATwoOiyx/NDup1SVqPQVBbDDeK3txBb9JuHfhNW3HWjZhDX+SGRAgPHkl5f0+kieBxRVieaPD5LGJ4WghLiwehbkBI4HUirF3S+SYrhhQ2f2H16aR5vMSYwbdjNtYXZ0J7cc70BXnFMHIGznzEAAAAAElFTkSuQmCC");background-size:7px 8px;}}</style>
@endsection