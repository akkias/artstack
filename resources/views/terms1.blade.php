@extends('layouts.app')
@section('title', 'Page Title')
@section('content')
<style type="text/css">
	.hero {
		background: #FFF;
	}
	.hero * {
		color: #5d646f;
	}
	.hero p {
		font-size: 16px;
	}
	.how-containwer {
		color: #5d646f;
	}
	h4 {
		line-height: 1.5;
	}
</style>
<div class="hero clearfix">
    <figcaption class="caption text-center">
        <h1>Terms of Service</h1>
        <p>The following document outlines the terms of use of the ArtStack website. 
        <br>Before using any of the ArtStack services, you are required to read, understand, and agree<br>to these terms. 
        <span class="hidden">The column on the right provides a short explanation of the terms of use and is <br>not legally binding.</span></p>
    </figcaption>
</div>
<section class="how-containwer">
    <div class="container">
     <style type="text/css">
         section {
            padding-bottom: 40px;
        }
        h5 {
            margin-bottom: 20px;
        }
    </style>
    <div class="row">
		<div class="col-md-7">
			<h5>DESCRIPTION OF SERVICE</h5>
			<p>ArtStack, Inc. (ArtStack or “We”) provides a platform via its website and apps (the “Site”) to a community of registered users (“Users” or “you”) to upload and display photographs and video (“Visual Content”) share comments, opinions and ideas, promote Visual Content collections, participate in contests and promotions, and license Visual Content as part of our Marketplace to other users (the “Services”). Additional Services may be offered by us from time to time. The following are the terms of service (“Terms”) for using the Site and the Services.</p>
		</div>
		<div class="col-md-5">
			<h5>BASICALLY,</h5>
			<h4>We develop a photo community and provide services to create online portfolios and license photos. We will develop more features and services in the future.</h4>
		</div>
    </div>
    <hr>
    <div class="row">
		<div class="col-md-7">
			<h5>ACCEPTANCE OF TERMS</h5>
			<p>BY USING THE SERVICES, YOU ARE AGREEING, ON BEHALF OF YOURSELF AND THOSE YOU REPRESENT, TO COMPLY WITH AND BE LEGALLY BOUND BY THESE TERMS AS WELL AS OUR PRIVACY POLICY AND ALL APPLICABLE LAWS. IF YOU, FOR YOURSELF OR ON BEHALF OF THOSE YOU REPRESENT, DO NOT AGREE TO ANY PROVISION OF THESE TERMS, YOU MUST, FOR YOURSELF AND ON BEHALF ANY SUCH PERSON(S), DISCONTINUE THE REGISTRATION PROCESS, DISCONTINUE YOUR USE OF THE SERVICES, AND, IF YOU ARE ALREADY REGISTERED, CANCEL YOUR ACCOUNT.</p>
		</div>
		<div class="col-md-5">
			<h5>BASICALLY,</h5>
			<h4>By using ArtStack, you agree to all the terms below.</h4>
		</div>
    </div>
    <hr>
    <div class="row">
		<div class="col-md-7">
			<h5>MODIFICATION OF TERMS</h5>
			<p>ArtStack reserves the right, at its sole discretion, to modify or replace the terms at any time. If the alterations constitute a material change to the terms, ArtStack will notify you by posting an announcement on the site. What constitutes a material change will be determined at ArtStack’s sole discretion. You shall be responsible for reviewing and becoming familiar with any such modifications. Using any Service or viewing any Visual Content shall constitute your acceptance of the Terms as modified.</p>
			<p>In addition, when using particular features of the Services, you shall be subject to any posted guidelines or rules applicable to such Services.</p>
			<p>Your access to and use of the Site and our Services may be interrupted from time to time as a result of equipment malfunction, updating, maintenance or repair of the Site or any other reason within or outside the control of ArtStack. ArtStack reserves the right to suspend or discontinue the availability of the Site and/or any Service and/or remove any Visual Content at any time at its sole discretion and without prior notice. ArtStack may also impose limits on certain features and Services or restrict your access to parts of or all of the Site and the Services without notice or liability. The Site should not be used or relied upon for storage of your Visual Content and you are directed to retain your own copies of all Visual Content posted on the Site.</p>
		</div>
		<div class="col-md-5">
			<h5>BASICALLY,</h5>
			<h4>If these terms change, we will notify you. As well, at times things can go wrong and the service may be interrupted. It’s unlikely, but sometimes things can go really wrong.</h4>
		</div>
    </div>
    <hr>
    <div class="row">
		<div class="col-md-7">
			<h5>REGISTRATION</h5>
			<p>As a condition to using Services, you are required to open an account with ArtStack and select a password and username, and to provide registration information. The registration information you provide must be accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your access to the Services, by either terminating your email access or your account.</p>
			<p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>
			<p>You are responsible for maintaining the confidentiality of your password and are solely responsible for all activities resulting from the use of your password and conducted through your ArtStack account.</p>
			<p>Services are available authorized representatives of legal entities and to individuals who are either (i) at least 18 years old to access the Marketplace, or (ii) at least 14 years old, and who are authorized to access the Site by a parent or legal guardian. If you have authorized a minor to use the Site, you are responsible for the online conduct of such minor, and the consequences of any misuse of the Site by the minor. Parents and legal guardians are warned that the Site does display Visual Content containing nudity and violence that may be offensive to some.</p>
			<p>The Services are for use by a) individuals who own Visual Content; b) entities that represent owners of Visual Content including but not limited to galleries, agents, representatives, distributors other market intermediaries; and c) individuals and entities seeking to license Visual Content. We are currently not accepting illustration and graphic design content to upload on the Site. If you are the owner of the Visual Content, but not the creator, you are not allowed to upload content for the purposes of self advertising.</p>
		</div>
		<div class="col-md-5">
			<h5>BASICALLY,</h5>
			<h4>To fully use the services, you need to create your own account, without violating other people’s rights.</h4>
		</div>
    </div>
    <hr>
    <div class="row">
		<div class="col-md-7">
			<h5>USER CONDUCT</h5>
			<p>All Content posted or otherwise submitted to the Site is the sole responsibility of the account holder from which such Content originates and you acknowledge and agree that you, and not ArtStack are entirely responsible for all Content that you post, or otherwise submit to the Site. ArtStack does not control user submitted Content and, as such, does not guarantee the accuracy, integrity or quality of such Content. You understand that by using the Site you may be exposed to Content that is offensive, indecent or objectionable.</p>
			<p>As a condition of use, you promise not to use the Services for any purpose that is unlawful or prohibited by these Terms, or any other purpose not reasonably intended by ArtStack. By way of example, and not as a limitation, you agree not to use the Services:</p>
			<ul>
				<li>To abuse, harass, threaten, impersonate or intimidate any person;</li>
				<li>To post or transmit, or cause to be posted or transmitted, any Visual Content or Communications that are libelous, defamatory, obscene, pornographic, abusive, offensive, profane, or that infringes any copyright or other right of any person;</li>
				<li>For any purpose (including posting or viewing Visual Content) that is not permitted under the laws of the jurisdiction where you use the Services;</li>
				<li>To post or transmit, or cause to be posted or transmitted, any Communication designed or intended to obtain password, account, or private information from any ArtStack user;</li>
				<li>To create or transmit unwanted ‘spam’ to any person or any URL</li>
				<li>To create multiple accounts for the purpose of voting for or against users’ Visual Content;</li>
				<li>To post copyrighted Visual Content or other Communications that do not belong to you or, with exception of commenting on Visual Content in Blogs, where you may post such Content with explicit mention of the author’s name and a link to the source of the Content;</li>
				<li>With the exception of accessing RSS feeds, you agree not to use any robot, spider, scraper or other automated means to access the Site for any purpose without our express written permission. Additionally, you agree that you will not: (i) take any action that imposes, or may impose in our sole discretion an unreasonable or disproportionately large load on our infrastructure; (ii) interfere or attempt to interfere with the proper working of the Site or any activities conducted on the Site; or (iii) bypass any measures we may use to prevent or restrict access to the Site;</li>
				<li>To artificially inﬂate or alter vote counts, blog counts, comments, or any other Service or for the purpose of giving or receiving money or other compensation in exchange for votes, or for participating in any other organized effort that in any way artificially alters the results of Services;</li>
				<li>To advertise to, or solicit, any user to buy or sell any third party products or services, or to use any information obtained from the Services in order to contact, advertise to, solicit, or sell to any user without their prior explicit consent;</li>
			</ul>
			<p>To report a suspected abuse of the Site or a breach of the Terms (other than relating to copyright infringement which is addressed under “COPYRIGHT COMPLAINTS” below) please send written notice to ArtStack at email: <a href="mailto:help@artstack.com">help@artstack.com</a>.</p>
			<p>You are solely responsible for your interactions with other users of the Site. ArtStack reserves the right, but has no obligation, to monitor disputes between you and other users.</p>
		</div>
		<div class="col-md-5">
			<h5>BASICALLY,</h5>
			<h4>You cannot use our site to post pornographic material, harass people, send spam, and do other crazy stuff. Be reasonable and responsible, don't do anything stupid, and you'll be fine.</h4>
		</div>
    </div>
 	</div>   
</section>



@endsection