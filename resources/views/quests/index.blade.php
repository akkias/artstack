@extends('layouts.app')
@section('title', 'Art quest | Artmojo - Be creative together')
@section('ogTags')
<meta property="og:description" content="Join the Being Creative Art Quest for a chance to win Amazon Gift Cards and get more exposure for your arts." />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@artmojoin">
<meta name="twitter:creator" content="@akkias">
<meta name="twitter:title" content="Being Creative | Art Quest by ArtMojo">
<meta name="twitter:description" content="Join the Being Creative Art Quest for a chance to win Amazon Gift Cards and get more exposure for your arts.">
<meta name="twitter:image" content="https://artmojo.in/images/banner.jpg">

<meta name="pinterest-rich-pin" content="false" />
<meta property="og:url" content="https://www.artmojo.in/quest" />
<meta property="og:site_name" content="artmojo.in" />
<meta property="article:author" content="ArtMojo" />
<meta property="fb:app_id" content="373046166427120" />
<meta property="og:type" content="blog" />
<meta property="og:title" content="Being Creative | Art Quest by ArtMojo" />
<meta property="og:image" content="https://artmojo.in/images/fb_banner_quest_bc.png" />
@endsection
@section('content')
<div class="top-wrapper quest">
	<div id="particleJS"></div>
	<div class="content text-center">
		<div>
		<svg height="72" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   viewBox="0 0 503.467 503.467" style="enable-background:new 0 0 503.467 503.467;" xml:space="preserve">
                   <g transform="translate(13 1)">
                    <path style="fill:#ECF4F7;" d="M383.8,2.84v34.133c0,102.4-17.067,136.533-17.067,136.533
                    c-25.6,59.733-85.333,68.267-85.333,170.667c0,0,0,25.6,17.067,51.2l25.6,34.133H153.4l25.6-34.133
                    c17.067-25.6,17.067-51.2,17.067-51.2c0-102.4-59.733-110.933-85.333-170.667c0,0-17.067-34.133-17.067-136.533V2.84H383.8z"/>
                    <polygon style="fill:#FFE079;" points="289.933,105.24 260.92,129.987 272.867,164.974 239.587,147.054 204.6,164.974 
                    218.253,129.987 187.533,105.24 223.373,105.24 238.733,71.107 254.093,105.24     "/>
                    <polygon style="fill:#AAB1BA;" points="153.4,429.507 324.067,429.507 332.6,429.507 349.667,497.774 126.947,497.774 
                    144.013,429.507     "/>
                </g>
                <path style="fill:#51565F;" d="M362.667,503.04h-222.72c-1.707,0-2.56-0.853-3.413-1.707s-0.853-2.56-0.853-3.413l17.067-68.267
                c0.853-1.707,2.56-3.413,4.267-3.413H345.6c1.707,0,3.413,1.707,4.267,3.413l17.067,68.267c0,0.853,0,2.56-0.853,3.413
                C365.227,502.187,364.373,503.04,362.667,503.04z M145.067,494.507h211.627l-15.36-59.733h-181.76L145.067,494.507z
                M293.547,468.907h-84.48c-2.56,0-4.267-1.707-4.267-4.267s1.707-4.267,4.267-4.267h84.48c2.56,0,4.267,1.707,4.267,4.267
                S296.107,468.907,293.547,468.907z M430.933,417.707c-2.56,0-4.267-1.707-4.267-4.267v-12.8h-12.8c-2.56,0-4.267-1.707-4.267-4.267
                c0-2.56,1.707-4.267,4.267-4.267h12.8v-12.8c0-2.56,1.707-4.267,4.267-4.267s4.267,1.707,4.267,4.267v12.8H448
                c2.56,0,4.267,1.707,4.267,4.267c0,2.56-1.707,4.267-4.267,4.267h-12.8v12.8C435.2,416,433.493,417.707,430.933,417.707z
                M311.467,400.64c-1.707,0-2.56-0.853-3.413-1.707c-17.92-26.453-17.92-52.053-17.92-53.76c0-67.413,25.6-95.573,50.347-122.88
                c13.653-15.36,26.453-29.013,35.84-49.493c0-0.853,17.067-34.987,17.067-134.827V8.107h-281.6v29.867
                c0,99.84,16.213,133.973,16.213,134.827c9.387,20.48,22.187,34.987,35.84,50.347c24.747,27.307,50.347,54.613,50.347,122.88
                c0,0.853,0,27.307-17.92,53.76c-1.707,1.707-4.267,2.56-5.973,0.853c-1.707-1.707-2.56-4.267-0.853-5.973
                c16.213-23.893,16.213-48.64,16.213-48.64c0-64-23.04-89.6-47.787-116.907c-13.653-14.507-27.307-29.867-36.693-52.053
                c-0.853-0.853-17.067-35.84-17.067-138.24V4.693c0-2.56,1.707-4.267,4.267-4.267h290.133c2.56,0,4.267,1.707,4.267,4.267v34.133
                c0,102.4-17.067,137.387-17.92,138.24c-9.387,22.187-23.04,37.547-36.693,52.053c-24.747,27.307-47.787,52.907-47.787,116.907
                c0,0,0,24.747,16.213,48.64c1.707,1.707,0.853,4.267-0.853,5.973C313.173,400.64,312.32,400.64,311.467,400.64z M106.667,357.974
                c-2.56,0-4.267-1.707-4.267-4.267v-4.267h-4.267c-2.56,0-4.267-1.707-4.267-4.267s1.707-4.267,4.267-4.267h4.267v-4.267
                c0-2.56,1.707-4.267,4.267-4.267s4.267,1.707,4.267,4.267v4.267h4.267c2.56,0,4.267,1.707,4.267,4.267s-1.707,4.267-4.267,4.267
                h-4.267v4.267C110.933,356.267,109.227,357.974,106.667,357.974z M21.333,332.374c-2.56,0-4.267-1.707-4.267-4.267v-12.8h-12.8
                C1.707,315.307,0,313.6,0,311.04s1.707-4.267,4.267-4.267h12.8v-12.8c0-2.56,1.707-4.267,4.267-4.267s4.267,1.707,4.267,4.267v12.8
                h12.8c2.56,0,4.267,1.707,4.267,4.267s-1.707,4.267-4.267,4.267H25.6v12.8C25.6,330.667,23.893,332.374,21.333,332.374z
                M473.6,255.574c-2.56,0-4.267-1.707-4.267-4.267v-21.333H448c-2.56,0-4.267-1.707-4.267-4.267c0-2.56,1.707-4.267,4.267-4.267
                h21.333v-21.333c0-2.56,1.707-4.267,4.267-4.267s4.267,1.707,4.267,4.267v21.333H499.2c2.56,0,4.267,1.707,4.267,4.267
                c0,2.56-1.707,4.267-4.267,4.267h-21.333v21.333C477.867,253.867,476.16,255.574,473.6,255.574z M413.867,178.774
                c-1.707,0-3.413-1.707-4.267-3.413c-0.853-2.56,0.853-4.267,3.413-5.12c31.573-7.68,47.787-26.453,47.787-55.467V55.04
                c0-6.827-5.973-12.8-12.8-12.8h-17.067c-2.56,0-4.267-1.707-4.267-4.267c0-2.56,1.707-4.267,4.267-4.267H448
                c11.947,0,21.333,9.387,21.333,21.333v59.733c0,33.28-18.773,54.613-54.613,64H413.867z M89.6,178.774h-0.853
                c-35.84-8.533-54.613-30.72-54.613-64V55.04c0-11.947,9.387-21.333,21.333-21.333h17.067c2.56,0,4.267,1.707,4.267,4.267
                c0,2.56-1.707,4.267-4.267,4.267H55.467c-6.827,0-12.8,5.973-12.8,12.8v59.733c0,29.013,16.213,47.787,47.787,55.467
                c2.56,0.853,3.413,2.56,3.413,5.12C93.013,177.067,91.307,178.774,89.6,178.774z M217.6,170.24c-0.853,0-1.707,0-2.56-0.853
                c-1.707-0.853-1.707-3.413-0.853-5.12l12.8-32.427l-28.16-23.04c-1.707-0.853-1.707-3.413-1.707-5.12
                c0.853-1.707,2.56-2.56,4.267-2.56h33.28l14.507-31.573c0.853-1.707,2.56-2.56,4.267-2.56l0,0c1.707,0,3.413,0.853,4.267,2.56
                l14.507,31.573h33.28c1.707,0,3.413,0.853,4.267,2.56c0.853,1.707,0,3.413-0.853,5.12L281.6,131.84l11.093,32.427
                c0.853,1.707,0,3.413-0.853,4.267c-1.707,0.853-3.413,1.707-5.12,0.853l-31.573-17.067l-32.427,17.067
                C219.307,170.24,218.453,170.24,217.6,170.24z M252.587,143.787c0.853,0,1.707,0,1.707,0.853l23.893,12.8l-8.533-24.747
                c-0.853-1.707,0-3.413,0.853-4.267l20.48-17.067h-23.893c-1.707,0-3.413-0.853-4.267-2.56L250.88,83.2l-11.093,25.6
                c-0.853,1.707-2.56,2.56-4.267,2.56h-23.893l21.333,17.067c1.707,0.853,1.707,3.413,1.707,5.12l-9.387,24.747l24.747-12.8
                C250.88,143.787,251.733,143.787,252.587,143.787z"/>
            </svg>
			<h1>Being Creative</h1>
			<small>By ArtMojo</small>
		</div>
		<div class="quest-share-buttons">
			<a data-toggle="tooltip" data-placement="bottom" title="Share on Facebook" class="fb" href="https://www.facebook.com/sharer/sharer.php?u=https://goo.gl/PoYld3" target="_blank">
				<i class="artmojo-icon-facebook"></i>
			</a><a data-toggle="tooltip" data-placement="bottom" title="Share on Twitter" class="tw" href="https://twitter.com/intent/tweet?url=https://goo.gl/KttN4b&text=Join the Being Creative Quest for a chance to win Amazon Gift Cards and get more exposure for your arts https://goo.gl/KttN4b&hashtags=artmojo" target="_blank">
				<i class="artmojo-icon-twitter"></i>
			</a><a data-toggle="tooltip" data-placement="bottom" title="Share on Pinterest" class="pint" href="https://pinterest.com/pin/create/button/?url=https://goo.gl/2hkWbO&media=https://artmojo.in/images/banner.jpg&description=Join the Being Creative Art Quest for a chance to win Amazon Gift Cards and get more exposure for your arts." target="_blank">
				<i class="artmojo-icon-pinterest-p"></i>
			</a>
		</div>
	</div>
</div>
<div class="header_fader_wrapper">
	<div class="fade one"></div>
	<div class="fade two"></div>
	<div class="fade three"></div>
	<div class="fade four"></div>
	<div class="fade five"></div>
	<div class="fade six"></div>
</div>


<div class="quest-content container-fluid">
	<div class="row">
		<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
			<div class="brief">
				<h4>The Brief</h4>
				<p>For this Quest, we are trying to get your best arts, designs, paintings, photographs etc. that showcase your <b>creativity</b> and <b>uniqueness</b>.</p>
				<p>All artmojo users will be able to rate the arts for their <b>creativity</b>, <b>precision</b> and <b>perfection</b>. And this is how it will help you to understand your creativity level.</p>
			</div>
		</div>
		<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
			<div class="quest-box">
				<h3>
					<img src="./images/clock.png" height="26" alt="Timing" />
					Timing
				</h3>
				<p>April 21, 2017 9:30 - May 20, 2017 06:30. <br>Your photos must be submitted between these times!</p>
			</div>
			<div class="quest-box">
				<h3>
					<img src="./images/prize.png" height="26" alt="About the Prize" />
					Prizes
				</h3>
				<h4>Judges Choice Award</h4>
				<p>₹ 2000 Amazon Gift Card</p>
				<hr>
				<h4>Most appreciated award</h4>
				<p>₹ 2000 Amazon Gift Card</p>
			</div>
			<div class="quest-box">
				<h3>
					<img src="./images/love1.png" height="26" alt="Rules" />
					How to participate
				</h3>
				<p>Login to ArtMojo, if you haven't already.</p>
				<p>Share your best arts, designs, paintings, photographs etc. on ArtMojo. The content of the art can be anything but creative. 
					Just add the hashtag <strong>#ArtMojoCreativeQuest</strong> in the description of the art.<p>
					<p>Share on social media to get maximum likes and rating to increase chance to win. Add hashtag <strong>#ArtMojoCreativeQuest</strong> <strong>#ArtMojo</strong> while sharing on social media.</p>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	/* -----------------------------------------------
/* Author : Vincent Garreau  - vincentgarreau.com
/* MIT license: http://opensource.org/licenses/MIT
/* GitHub : https://github.com/VincentGarreau/particles.js
/* How to use? : Check the GitHub README
/* v1.0.3
/* ----------------------------------------------- */
function launchParticlesJS(a,e){var i=document.querySelector("#"+a+" > canvas");pJS={canvas:{el:i,w:i.offsetWidth,h:i.offsetHeight},particles:{color:"#fff",shape:"circle",opacity:1,size:2.5,size_random:true,nb:200,line_linked:{enable_auto:true,distance:100,color:"#fff",opacity:1,width:1,condensed_mode:{enable:true,rotateX:65000,rotateY:65000}},anim:{enable:true,speed:1},array:[]},interactivity:{enable:true,mouse:{distance:100},detect_on:"canvas",mode:"grab",line_linked:{opacity:1},events:{onclick:{enable:true,mode:"push",nb:4}}},retina_detect:false,fn:{vendors:{interactivity:{}}}};if(e){if(e.particles){var b=e.particles;if(b.color){pJS.particles.color=b.color}if(b.shape){pJS.particles.shape=b.shape}if(b.opacity){pJS.particles.opacity=b.opacity}if(b.size){pJS.particles.size=b.size}if(b.size_random==false){pJS.particles.size_random=b.size_random}if(b.nb){pJS.particles.nb=b.nb}if(b.line_linked){var j=b.line_linked;if(j.enable_auto==false){pJS.particles.line_linked.enable_auto=j.enable_auto}if(j.distance){pJS.particles.line_linked.distance=j.distance}if(j.color){pJS.particles.line_linked.color=j.color}if(j.opacity){pJS.particles.line_linked.opacity=j.opacity}if(j.width){pJS.particles.line_linked.width=j.width}if(j.condensed_mode){var g=j.condensed_mode;if(g.enable==false){pJS.particles.line_linked.condensed_mode.enable=g.enable}if(g.rotateX){pJS.particles.line_linked.condensed_mode.rotateX=g.rotateX}if(g.rotateY){pJS.particles.line_linked.condensed_mode.rotateY=g.rotateY}}}if(b.anim){var k=b.anim;if(k.enable==false){pJS.particles.anim.enable=k.enable}if(k.speed){pJS.particles.anim.speed=k.speed}}}if(e.interactivity){var c=e.interactivity;if(c.enable==false){pJS.interactivity.enable=c.enable}if(c.mouse){if(c.mouse.distance){pJS.interactivity.mouse.distance=c.mouse.distance}}if(c.detect_on){pJS.interactivity.detect_on=c.detect_on}if(c.mode){pJS.interactivity.mode=c.mode}if(c.line_linked){if(c.line_linked.opacity){pJS.interactivity.line_linked.opacity=c.line_linked.opacity}}if(c.events){var d=c.events;if(d.onclick){var h=d.onclick;if(h.enable==false){pJS.interactivity.events.onclick.enable=false}if(h.mode!="push"){pJS.interactivity.events.onclick.mode=h.mode}if(h.nb){pJS.interactivity.events.onclick.nb=h.nb}}}}pJS.retina_detect=e.retina_detect}pJS.particles.color_rgb=hexToRgb(pJS.particles.color);pJS.particles.line_linked.color_rgb_line=hexToRgb(pJS.particles.line_linked.color);if(pJS.retina_detect&&window.devicePixelRatio>1){pJS.retina=true;pJS.canvas.pxratio=window.devicePixelRatio;pJS.canvas.w=pJS.canvas.el.offsetWidth*pJS.canvas.pxratio;pJS.canvas.h=pJS.canvas.el.offsetHeight*pJS.canvas.pxratio;pJS.particles.anim.speed=pJS.particles.anim.speed*pJS.canvas.pxratio;pJS.particles.line_linked.distance=pJS.particles.line_linked.distance*pJS.canvas.pxratio;pJS.particles.line_linked.width=pJS.particles.line_linked.width*pJS.canvas.pxratio;pJS.interactivity.mouse.distance=pJS.interactivity.mouse.distance*pJS.canvas.pxratio}pJS.fn.canvasInit=function(){pJS.canvas.ctx=pJS.canvas.el.getContext("2d")};pJS.fn.canvasSize=function(){pJS.canvas.el.width=pJS.canvas.w;pJS.canvas.el.height=pJS.canvas.h;window.onresize=function(){if(pJS){pJS.canvas.w=pJS.canvas.el.offsetWidth;pJS.canvas.h=pJS.canvas.el.offsetHeight;if(pJS.retina){pJS.canvas.w*=pJS.canvas.pxratio;pJS.canvas.h*=pJS.canvas.pxratio}pJS.canvas.el.width=pJS.canvas.w;pJS.canvas.el.height=pJS.canvas.h;pJS.fn.canvasPaint();if(!pJS.particles.anim.enable){pJS.fn.particlesRemove();pJS.fn.canvasRemove();f()}}}};pJS.fn.canvasPaint=function(){pJS.canvas.ctx.fillRect(0,0,pJS.canvas.w,pJS.canvas.h)};pJS.fn.canvasRemove=function(){pJS.canvas.ctx.clearRect(0,0,pJS.canvas.w,pJS.canvas.h)};pJS.fn.particle=function(n,o,m){this.x=m?m.x:Math.random()*pJS.canvas.w;this.y=m?m.y:Math.random()*pJS.canvas.h;this.radius=(pJS.particles.size_random?Math.random():1)*pJS.particles.size;if(pJS.retina){this.radius*=pJS.canvas.pxratio}this.color=n;this.opacity=o;this.vx=-0.5+Math.random();this.vy=-0.5+Math.random();this.draw=function(){pJS.canvas.ctx.fillStyle="rgba("+this.color.r+","+this.color.g+","+this.color.b+","+this.opacity+")";pJS.canvas.ctx.beginPath();switch(pJS.particles.shape){case"circle":pJS.canvas.ctx.arc(this.x,this.y,this.radius,0,Math.PI*2,false);break;case"edge":pJS.canvas.ctx.rect(this.x,this.y,this.radius*2,this.radius*2);break;case"triangle":pJS.canvas.ctx.moveTo(this.x,this.y-this.radius);pJS.canvas.ctx.lineTo(this.x+this.radius,this.y+this.radius);pJS.canvas.ctx.lineTo(this.x-this.radius,this.y+this.radius);pJS.canvas.ctx.closePath();break}pJS.canvas.ctx.fill()}};pJS.fn.particlesCreate=function(){for(var m=0;m<pJS.particles.nb;m++){pJS.particles.array.push(new pJS.fn.particle(pJS.particles.color_rgb,pJS.particles.opacity))}};pJS.fn.particlesAnimate=function(){for(var n=0;n<pJS.particles.array.length;n++){var q=pJS.particles.array[n];q.x+=q.vx*(pJS.particles.anim.speed/2);q.y+=q.vy*(pJS.particles.anim.speed/2);if(q.x-q.radius>pJS.canvas.w){q.x=q.radius}else{if(q.x+q.radius<0){q.x=pJS.canvas.w+q.radius}}if(q.y-q.radius>pJS.canvas.h){q.y=q.radius}else{if(q.y+q.radius<0){q.y=pJS.canvas.h+q.radius}}for(var m=n+1;m<pJS.particles.array.length;m++){var o=pJS.particles.array[m];if(pJS.particles.line_linked.enable_auto){pJS.fn.vendors.distanceParticles(q,o)}if(pJS.interactivity.enable){switch(pJS.interactivity.mode){case"grab":pJS.fn.vendors.interactivity.grabParticles(q,o);break}}}}};pJS.fn.particlesDraw=function(){pJS.canvas.ctx.clearRect(0,0,pJS.canvas.w,pJS.canvas.h);pJS.fn.particlesAnimate();for(var m=0;m<pJS.particles.array.length;m++){var n=pJS.particles.array[m];n.draw("rgba("+n.color.r+","+n.color.g+","+n.color.b+","+n.opacity+")")}};pJS.fn.particlesRemove=function(){pJS.particles.array=[]};pJS.fn.vendors.distanceParticles=function(t,r){var o=t.x-r.x,n=t.y-r.y,s=Math.sqrt(o*o+n*n);if(s<=pJS.particles.line_linked.distance){var m=pJS.particles.line_linked.color_rgb_line;pJS.canvas.ctx.beginPath();pJS.canvas.ctx.strokeStyle="rgba("+m.r+","+m.g+","+m.b+","+(pJS.particles.line_linked.opacity-s/pJS.particles.line_linked.distance)+")";pJS.canvas.ctx.moveTo(t.x,t.y);pJS.canvas.ctx.lineTo(r.x,r.y);pJS.canvas.ctx.lineWidth=pJS.particles.line_linked.width;pJS.canvas.ctx.stroke();pJS.canvas.ctx.closePath();if(pJS.particles.line_linked.condensed_mode.enable){var o=t.x-r.x,n=t.y-r.y,q=o/(pJS.particles.line_linked.condensed_mode.rotateX*1000),p=n/(pJS.particles.line_linked.condensed_mode.rotateY*1000);r.vx+=q;r.vy+=p}}};pJS.fn.vendors.interactivity.listeners=function(){if(pJS.interactivity.detect_on=="window"){var m=window}else{var m=pJS.canvas.el}m.onmousemove=function(p){if(m==window){var o=p.clientX,n=p.clientY}else{var o=p.offsetX||p.clientX,n=p.offsetY||p.clientY}if(pJS){pJS.interactivity.mouse.pos_x=o;pJS.interactivity.mouse.pos_y=n;if(pJS.retina){pJS.interactivity.mouse.pos_x*=pJS.canvas.pxratio;pJS.interactivity.mouse.pos_y*=pJS.canvas.pxratio}pJS.interactivity.status="mousemove"}};m.onmouseleave=function(n){if(pJS){pJS.interactivity.mouse.pos_x=0;pJS.interactivity.mouse.pos_y=0;pJS.interactivity.status="mouseleave"}};if(pJS.interactivity.events.onclick.enable){switch(pJS.interactivity.events.onclick.mode){case"push":m.onclick=function(o){if(pJS){for(var n=0;n<pJS.interactivity.events.onclick.nb;n++){pJS.particles.array.push(new pJS.fn.particle(pJS.particles.color_rgb,pJS.particles.opacity,{x:pJS.interactivity.mouse.pos_x,y:pJS.interactivity.mouse.pos_y}))}}};break;case"remove":m.onclick=function(n){pJS.particles.array.splice(0,pJS.interactivity.events.onclick.nb)};break}}};pJS.fn.vendors.interactivity.grabParticles=function(r,q){var u=r.x-q.x,s=r.y-q.y,p=Math.sqrt(u*u+s*s);var t=r.x-pJS.interactivity.mouse.pos_x,n=r.y-pJS.interactivity.mouse.pos_y,o=Math.sqrt(t*t+n*n);if(p<=pJS.particles.line_linked.distance&&o<=pJS.interactivity.mouse.distance&&pJS.interactivity.status=="mousemove"){var m=pJS.particles.line_linked.color_rgb_line;pJS.canvas.ctx.beginPath();pJS.canvas.ctx.strokeStyle="rgba("+m.r+","+m.g+","+m.b+","+(pJS.interactivity.line_linked.opacity-o/pJS.interactivity.mouse.distance)+")";pJS.canvas.ctx.moveTo(r.x,r.y);pJS.canvas.ctx.lineTo(pJS.interactivity.mouse.pos_x,pJS.interactivity.mouse.pos_y);pJS.canvas.ctx.lineWidth=pJS.particles.line_linked.width;pJS.canvas.ctx.stroke();pJS.canvas.ctx.closePath()}};pJS.fn.vendors.destroy=function(){cancelAnimationFrame(pJS.fn.requestAnimFrame);i.remove();delete pJS};function f(){pJS.fn.canvasInit();pJS.fn.canvasSize();pJS.fn.canvasPaint();pJS.fn.particlesCreate();pJS.fn.particlesDraw()}function l(){pJS.fn.particlesDraw();pJS.fn.requestAnimFrame=requestAnimFrame(l)}f();if(pJS.particles.anim.enable){l()}if(pJS.interactivity.enable){pJS.fn.vendors.interactivity.listeners()}}window.requestAnimFrame=(function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(a){window.setTimeout(a,1000/60)}})();window.cancelRequestAnimFrame=(function(){return window.cancelAnimationFrame||window.webkitCancelRequestAnimationFrame||window.mozCancelRequestAnimationFrame||window.oCancelRequestAnimationFrame||window.msCancelRequestAnimationFrame||clearTimeout})();function hexToRgb(c){var b=/^#?([a-f\d])([a-f\d])([a-f\d])$/i;c=c.replace(b,function(e,h,f,d){return h+h+f+f+d+d});var a=/^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(c);return a?{r:parseInt(a[1],16),g:parseInt(a[2],16),b:parseInt(a[3],16)}:null}window.particlesJS=function(d,c){if(typeof(d)!="string"){c=d;d="particles-js"}if(!d){d="particles-js"}var b=document.createElement("canvas");b.style.width="100%";b.style.height="100%";var a=document.getElementById(d).appendChild(b);if(a!=null){launchParticlesJS(d,c)}};

/* particlesJS('dom-id', params);
/* @dom-id : set the html tag id [string, optional, default value : particles-js]
/* @params: set the params [object, optional, default values : check particles.js] */

/* config dom id (optional) + config particles params */
particlesJS('particleJS', {
	particles: {
	color: '#fff',
  	value: 150,
    shape: 'circle',
    opacity: 1,
    size: 6,
    size_random: true,
    nb: 250,
    line_linked: {
    	enable_auto: true,
    	distance: 120,
    	color: '#fff',
    	opacity: 1,
    	width: 1,
    	condensed_mode: {
    		enable: false,
    		rotateX: 600,
    		rotateY: 600
    	}
    },
    anim: {
    	enable: true,
    	speed: 1
    }
},
interactivity: {
	enable: true,
	mouse: {
		distance: 250
	},
    detect_on: 'canvas', // "canvas" or "window"
    mode: 'grab',
    line_linked: {
    	opacity: .2
    },
    events: {
    	onclick: {
    		enable: true,
        mode: 'push', // "push" or "remove" (particles)
        nb: 4
    }
} 
},
/* Retina Display Support */
retina_detect: true
});
</script>
@endsection