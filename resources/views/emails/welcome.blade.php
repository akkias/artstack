<html>
<head></head>
<body style="background: #f7f8fa; color: #000;padding: 50px 0;margin: 0;text-align:center;">
	<table style="border: 1px solid #eeeff2; background: #FFF; max-width:600px;width: 100%;margin:0 auto;" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th style="border-bottom: 1px solid #d9ecf2;padding: 20px;" align="left">
					<a href="https://goo.gl/bSjvJZ">
						<img style="width: 140px;display: block;" src="https://artmojo.in/images/artmojo_logo.png" alt="">
					</a>
				</th>
				<th style="border-bottom: 1px solid #d9ecf2;padding: 20px;" align="right">
					<a style="margin-left: 5px;float: right;" href="https://www.instagram.com/artmojoin/" >
						<img style="width: 32px; display: block;" src="https://artmojo.in/images/insta.png" alt="">
					</a>
					<a style="float: right;" href="https://www.facebook.com/ArtMojoin" >
						<img style="width: 32px; display: block;" src="https://artmojo.in/images/fb.png" alt="">
					</a>
				</th>
			</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2" style="padding: 20px 0 40px;">
				<h2 style="color: #000; text-decoration: none; font-size: 24px; font-weight: normal;line-height: 1.5;margin: 20px;">
					Hello Sumit, You have new follower on TripTroop. Check out their stories and follow them back.
				</h2>
				<p>
					<img src="https://www.filestackapi.com/api/file/hebsIboXTrG5nIOKpgjl" height="150">
				</p>
				<a href="https://artmojo.in/profile/1" style="display: inline-block;color: #fff;margin-top: 8px;background: #1d6fdc;border-radius: 100px;width: auto;text-transform: uppercase;font-size: 14px;padding: 10px 20px;line-height:14px;letter-spacing: .03em;text-decoration: none;background: linear-gradient(40deg, #b73183 5%,#c13678 30%,#db5453 85%);background: -moz-linear-gradient(40deg, #b73183 5%,#c13678 30%,#db5453 85%);">View Profile</a>
			</td>
		</tr>
	</tbody>
</table>
<table style="border: 1px solid #eeeff2; background: #FFF; max-width:600px;width: 100%;text-align: left;margin:10px auto;" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td style="padding: 20px;">
				<div style="font-size:15px;line-height:24px;color:#191919;font-weight:bold">
					Help
				</div>
				<div style="font-size:13px;line-height:20px;color:rgba(0,0,0,0.54);font-weight:normal;margin-bottom:14px">
					Reach out to us for even a minutest query at <a href="mailto:admin@artmojo.in" style="color:rgb(87,87,87)">contact@<span class="il">triptroop</span>.me </a> and we will revert soon.
				</div>
			</td>
		</tr>
	</tbody>
</table>

<table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding:10px 0 0;max-width: 600px;margin: auto;text-align: left;">
	<tbody><tr>
		<td>
			<div style="font-size:12px;font-weight:normal;color:rgba(0,0,0,0.54);margin-bottom:5px">
				Cheers - Team 
				<a href="www.triptroop.me" style="color:rgb(87,87,87)" target="_blank">ArtStack </a> 
			</div>
		</td>
	</tr>
</tbody>
</table>
</body>
</html>