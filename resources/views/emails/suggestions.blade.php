<html>
<head></head>
<body style="background: #eaeced; color: #1b283b;padding: 50px 0;margin: 0;text-align:center;">
	<table style="border: 1px solid #d9ecf2; background: #FFF; max-width:600px;width: 100%;margin:0 auto;" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th style="border-bottom: 1px solid #d9ecf2;padding: 20px;" align="left">
					<a href="https://goo.gl/bSjvJZ">
						<img style="width: 140px;display: block;" src="https://artmojo.in/images/artmojo_logo.png" alt="">
					</a>
				</th>
				<th style="border-bottom: 1px solid #d9ecf2;padding: 20px;" align="right">
					<a style="margin-left: 5px;float: right;" href="https://www.instagram.com/artmojoin/" >
						<img style="width: 32px; display: block;" src="https://artmojo.in/images/insta.png" alt="">
					</a>
					<a style="float: right;" href="https://www.facebook.com/ArtMojoin" >
						<img style="width: 32px; display: block;" src="https://artmojo.in/images/fb.png" alt="">
					</a>
				</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td colspan="2" style="padding: 20px 0 40px;">
					<h2 style="color: #1b283b; text-decoration: none; font-size: 16px; font-weight: normal;line-height: 1.5;margin: 0px 20px 20px;text-align: left;">
						Hello {{$user->name}},<br>You have received new suggestion on your following art 
					</h2>
					<p>
						<?php
						$uri  = $art->images;
						$uriPieces = explode("/", $uri);
						?>
						<img src="{{$uriPieces[0]}}/{{$uriPieces[1]}}/{{$uriPieces[2]}}/resize=fit:max,width:900/{{$uriPieces[3]}}" width="100%">
					</p>
					<a href="https://artmojo.in/suggest/{{$art->id}}" style="display: inline-block;color: #fff;margin-top: 8px;background: #1d6fdc;border-radius: 100px;width: auto;text-transform: uppercase;font-size: 14px;padding: 15px 25px;line-height:14px;letter-spacing: .03em;text-decoration: none;background: linear-gradient(40deg, #b73183 5%,#c13678 30%,#db5453 85%);background: -moz-linear-gradient(40deg, #b73183 5%,#c13678 30%,#db5453 85%);">View Suggestion</a>
				</td>
			</tr>
		</tbody>
	</table>
	<table style="border: 1px solid #d9ecf2; background: #FFF; max-width:600px;width: 100%;text-align: left;margin:10px auto;" cellpadding="0" cellspacing="0" border="0">
		<tbody>
			<tr>
				<td style="padding: 20px;">
					<div style="font-size:15px;line-height:24px;color:#191919;font-weight:bold">
						Help
					</div>
					<div style="font-size:13px;line-height:20px;color:rgba(0,0,0,0.54);font-weight:normal;margin-bottom:14px">
						Reach out to us for even a minutest query at <a href="mailto:admin@artmojo.in" style="color:rgb(87,87,87)">admin@<span class="il">artmojo</span>.in </a> and we will revert soon.
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding:10px 0 0;max-width: 600px;margin: auto;text-align: left;">
		<tbody><tr>
			<td>
				<div style="font-size:12px;font-weight:normal;color:rgba(0,0,0,0.54);margin-bottom:5px">
					Cheers - Team 
					<a href="www.artmojo.in" style="color:rgb(87,87,87)" target="_blank">ArtMojo</a> 
				</div>
			</td>
		</tr>
	</tbody>
</table>
</body>
</html>