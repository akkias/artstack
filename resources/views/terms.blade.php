@extends('layouts.app')
@section('title', 'Terms of service | Artmojo - Be creative together')
@section('ogTags')
<meta property = "og:site_name" content = "artmojo.in" />
<meta property = "og:description" content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals"/>
<meta name = "twitter:domain" value = "artmojo.in" />
<meta name = "twitter:site" content = "@artmojoin" />
<meta name = "twitter:description"  content = "ArtMojo | Community of artists | Showcase your work, Monetize it and help newbies to achieve their goals" />
<meta property = "og:type" content =  "ArtMojo:craft" />
<meta property  =  "og:title" content = "ArtMojo | Community of artists | Showcase your work and become popular" />
<meta property  =  "og:image" content = "https://artmojo.in/images/fb_banner.png" />
<meta property="fb:app_id" content="373046166427120" />
@endsection
@section('header')
@parent
<nav class="subnav clearfix" id="sticker">
	<ul class="list-inline list-unstyled clearfix">
		<li><a onclick="ga('send', 'event', 'Privacy Policy clicked')" href="/privacy-policy">Privacy Policy</a></li>
		<li class="active"><a onclick="ga('send', 'event', 'Terms of Service clicked')" href="/terms-of-service">Terms of Service</a></li>
	</ul>
</nav>
@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="terms-container">
				<h1>Terms of Service
					<small>Last update: April 2017</small></h1>
					<p>These Terms of Service apply to use of the ArtMojo website and Applications located at http://<a href="www.artmojo.in">www.artmojo.in</a> (the "Site"). The Site is the property of iVision Web Studio. Your use of the ArtMojo website http://<a href="www.artmojo.in">www.artmojo.in</a> (hereinafter referred to as “ArtMojo" or “<a href="www.artmojo.in">www.artmojo.in</a>”) and services and tools (“Services”) are governed by the following terms of service as applicable to <a href="www.artmojo.in">www.artmojo.in</a>. If you utilize <a href="www.artmojo.in">www.artmojo.in</a>, you shall be subject to the policies that are applicable to the website for such services and usage. By mere use of the website you shall be contracting with ArtMojo, and these Terms of Service constitute your binding obligations.</p>
					<p>When you use any of the services provided by <a href="www.artmojo.in">www.artmojo.in</a>, including but not limited to, (e.g., users reviews), you will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they shall be deemed to be incorporated into this Terms of Use and shall be considered as part and parcel of this Terms of Use. ArtMojo reserves the right, at its sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time. It is your responsibility to check these Terms of Use periodically for changes. Your continued use of the Site following the posting of changes will mean that you accept and agree to the changes. As long as you comply with these Terms of Use, ArtMojo grants you a personal, non-exclusive, non- transferable, limited privilege to enter and use the Site.</p>
					<p>ACCESSING, BROWSING OR OTHERWISE USING THE SITE INDICATES YOUR AGREEMENT TO ALL THE TERMS OF SERVICE IN THIS AGREEMENT, SO PLEASE READ THIS AGREEMENT CAREFULLY BEFORE PROCEEDING.</p>
					<hr>
					<h4>ACCEPTANCE AND CHANGES</h4>
					<p>Your access to and use of <a href="www.artmojo.in">www.artmojo.in</a> is subject exclusively to these Terms and Service. You will not use the Website for any purpose that is unlawful or prohibited by these Terms of Service. By using the Website you are fully accepting the terms, conditions, service, cancellations, disclaimers, and policies contained in this notice. If you do not accept these Terms of Service you must immediately stop using the Website.</p>
					<h4>USE OF THE WEBSITE <a href="www.artmojo.in">www.artmojo.in</a></h4>
					<p>You agree, undertake and confirm that your use of <a href="www.artmojo.in">www.artmojo.in</a> shall be strictly governed by the following binding principles below. Members/Users will not use this community for:</p>
					<ul>
						<li>Any practices that affect the normal operations of the community (Admins will take whatever steps are necessary to restore service)</li>
						<li>Transmitting any libelous, defamatory, or any other material that could give rise to any civil or criminal liability under the law.</li>
						<li>Personal attacks. This includes but is not limited to, destructive, abusive, defamatory communications in any form, and retaliatory attacks from personal attacks. If you need assistance, please communicate with someone from our ArtMojo Team.</li>
						<li>Destructive commentary/communications made with the intent to disrupt or attack (Trolling). This applies to any communications within this community, whether in the forums, art galleries, graffiti wall, chat, blogs, sitemail or article opinions.</li>
						<li>Advertising or linking to any publications and/or web sites that are sexually explicit, and/or pornographic in nature.</li>
						<li>No harassment will be tolerated at ArtMojo. If anyone feels as if they’re being harassed by another member, please report it to someone from our ArtMojo team. Such messages will be deleted and the member may be banned from using these functions. This applies to any communications within this community, which includes but not limited to forums, art galleries, graffiti wall, chat, blogs, sitemail or article opinions.
							<ul>
								<li> <u>Flame Baiting.</u> It can be a person unfamiliar with a certain topic/discussion on an internet forum group, saying things that would provoke an angry, teasing, or overall negative response. For example: You’re a loser if you think Poser is better than Daz. OR Everyone knows which software will be at the top next time this year and it won’t be Poser/Daz. These will purposely provoke negative reactions.</li>
								<li> <u>Trolling.</u> Purposely causing harm to others by stating destructive commentary such as “that’s an ugly photo you just uploaded.” Or, “you are horrible at creating photos, you should re-learn Poser.” If you need assistance, please communicate with someone from our ArtMojo Team.</li>
								<li> <u>Political/Religious Viewpoints.</u> Since discussions of a Religious and Political nature tend to be heated because of varying viewpoints, we feel that as an artists' forum, there is no place for these types of discussions. Insulting someone’s religion or speaking about political views in a negative manner is not tolerated anywhere on our site.</li>
								<li> <u>Disturbing the peace – at ArtMojo:</u> we will not accept any members interrupting the community with any negative comments. Here are a few examples: if a member creates a thread asking for help on something, a staff member posts a thread looking for participation say a contest and a
									member attacks them, this behavior will not be tolerated and a warning will be given. You may disagree with members, but if it’s in a vicious manner, it will not be acceptable. Also, this new addition to the TOS is in no way saying that the site is censoring, but rather, helping the forums and chat fight against trolls, and bullies.</li>
									<li> Additionally, offsite linking to other marketplaces or freestuff sites are not allowed within the forum, gallery, or forum signatures.</li>
								</ul>
							</li>
						</ul>
						<p>No Posting Unacceptable Images that has the following contents:</p>
						<ol>
							<li>Rape</li>
							<li>Torture</li>
							<li>Sexual acts</li>
							<li>Physical arousal</li>
							<li>Explicit sexual contents</li>
							<li>Genital contacts</li>
							<li>Child nudity</li>
							<li>General nudity</li>
							<li>violence etc.</li>
						</ol>
						<p>Here in ArtMojo, we maintain a Zero Tolerance on certain behaviors within the community. These include, but are not limited to the following:</p>
						<ul>
							<li>Any threats of physical harm, property damage or acts of violence toward another individual, or group of individuals.</li>
							<li>Fraudulent use of credit cards or refusing to pay for items received from The MarketPlace.</li>
							<li>Soliciting, trading or distributing products illegally (warez). This includes, but is not limited to, requests and/or distribution of computer software, digital products, software security overrides, serial numbers and/or continued use or possession of illegal products (warez).</li>
							<li>Intentional practices that affect the normal operations of the community (Admins will take whatever steps are necessary to restore service).</li>
							<li>Any member found to be impersonating a moderator or Admin of ArtMojo will have his/her membership revoked.</li>
						</ul>
						<p>Members/Users found practicing these behaviors will immediately have their membership revoked, and access to the community permanently blocked from the community including any duplicate accounts for the same person. ArtMojo considers this information private and confidential. However, there may be certain situations that necessitate otherwise. The appropriate legal authorities will be contacted, and if appropriate charges may be filed.</p>
						<h4>VIOLATIONS</h4>
						<p>Members or users that violate the rules above will be notified by a member of the ArtMojo team. Warnings will be noted in the member's record. Continual violation of the rules will result in a temporary ban. Habitual rule violations can result in a permanent ban.</p>
						<h4>WEBSITE CONTENTS</h4>
						<p>All text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, music, artwork and computer code (collectively, “Content”), including but not limited to the design, structure, selection, coordination, expression, “look and feel” and arrangement of such Content, contained on artmojo.in is owned, controlled or licensed by or to artmojo.in, and is protected by trade dress, copyright, patent and trademark laws, and various other intellectual property rights and unfair competition laws.</p>
						<p>Except as expressly provided in these Terms of Use, no part of artmojo.in and no Content may be copied, reproduced, re-published, uploaded, posted, publicly displayed, encoded, translated, transmitted or distributed in any way (including “mirroring”) to any other computer, server, website or other medium for publication or distribution or for any commercial enterprise, without artmojo.in’s express prior written consent.</p>
						<p>You may use information on artmojo.in products and services purposely made available by artmojo.in for downloading from the Site, provided that you (1) not remove any proprietary notice language in all copies of such documents, (2) use such information only for your personal, non-commercial informational purpose and do not copy or post such information on any networked computer or broadcast it in any media, (3) make no modifications to any such information, and (4) not make any additional representations or warranties relating to such documents.</p>
						<p>You shall be responsible for any artwork, notes, messages, e-mails, billboard postings, photos, drawings, profiles, opinions, ideas, images, videos, audio files or other materials or information posted or transmitted to the Sites (collectively, "Content"). We dont hold copyright of the images uploaded on the site, user will be solely responsible for the uploaded image.</p>
						<p>You grant artmojo.in the worldwide, perpetual and transferable rights in such Content. artmojo.in shall be entitled to, consistent with our Privacy Policy, use the Content or any of its elements for any type of use forever, including but not limited to promotional and advertising purposes and in any media whether now known or hereafter devised, including the creation of derivative works that may include Content you provide. You agree that any Content you post may be used by artmojo.in, consistent with our Privacy Policy and Rules of Conduct on Site as mentioned herein, and you are not entitled to any payment or other compensation for such use.</p>
						<h4>PROPRIETARY RIGHTS</h4>
						<p>You hereby acknowledge and agree that ArtMojo is the owner of highly valuable proprietary information, including without limitation, the patented compatibility matching system, compatibility profiles, medias and relationship questionnaires (collectively, "Confidential Information"). ArtMojo owns and hereby retains all proprietary rights in the Services and the Site, including but not limited to, all Confidential Information.</p>
						<p>You will not post, copy, modify, transmit, disclose, show in public, create any derivative works from, distribute, make commercial use of, or reproduce in any way any (I.) Confidential Information or (ii.) other copyrighted material, trademarks, or other proprietary information accessible via the Services, without first obtaining the prior written consent of the owner of such proprietary rights.</p>
						<h4>INTELLECTUAL PROPERTY</h4>
						<p>ArtMojo members remain the original copyright holder in all materials (software and content) provided on the community. Do not use any ArtMojo post, images or writings without expressed, written consent by the copyright holder. Use of the material inconsistent with these terms and conditions is prohibited and considered an infringement of the copyrights of the respective holders.</p>
						<h4>INTELLECTUAL PROPERTY INFRINGEMENT POLICY</h4>
						<p>It is the policy of artmojo.in to take appropriate action where necessary to uphold and recognize all relevant State, Federal and International laws in connection with material that is claimed to be infringing any trademark, copyright, patent and all or any other Intellectual Property laws. If you are an intellectual property rights owner and you believe that artmojo.in sells, offers for sale, or makes available goods and/or services that infringe your intellectual property rights, then send the following information in its entirety to the support page on the website or to provided email.</p>
						<address>
							ArtMojo<br>
							iVision Web Studio<br>
							<a href="www.artmojo.in">www.artmojo.in</a><br>
							<a href="mailto:admin@artmojo.in">admin@artmojo.in</a>
						</address>
						<h4>Information required</h4>
						<ol>
							<li>An electronic or physical signature of the person authorized to act on behalf of
								the owner of an exclusive that is allegedly infringed;</li>
								<li>A description of the allegedly infringing work or material;</li>
								<li>A description of where the allegedly infringing material is located on the site
									(product(s) URL);</li>
									<li>Information reasonably sufficient to allow us to contact you, such as your
										address, telephone number and e-mail address;</li>
										<li>A statement by you that you have a good faith belief that the disputed use of the
											material is not authorized by the copyright or other proprietary right owner, its
											agent, or the law;</li>
											<li>Identification of the intellectual property rights that you claim are infringed by
												the Website(e.g. "XYZ copyright", "ABC trademark, Reg. No. 123456, registered 1/1/04",etc);</li>
											</ol>
											<p>A statement by you that the above information and notification is accurate, and under penalty of perjury, that you are the copyright owner or authorized to act on behalf of the owner whose exclusive right is allegedly infringed.</p>
											<h4>COPYRIGHT COMPLAINT</h4>
											<p>We at ArtMojo respect the intellectual property of others. In case you feel that your work has been copied in a way that constitutes copyright infringement you can write to us via our support page on the website or send us an email via <a href="mailto:admin@artmojo.in">admin@artmojo.in</a>.</p>
											<h4>LINKS TO THIRD PARTY WEBSITES</h4>
											<p>ArtMojo Website may include links to third party websites that are controlled and maintained by others. Any link to other websites is not an endorsement of such websites and you acknowledge and agree that we are not responsible for the content or availability of any such sites.</p>
											<p>You also agree that ArtMojo will not be liable for any loss or damage of any sort incurred as the result of any such dealings, including the sharing of the information you supply to ArtMojo with advertisers or sweepstakes sponsors, or as the result of the presence of such advertisers on the Services.</p>
											<h4>COPYRIGHT</h4>
											<p>All copyright, trademarks and all other intellectual property rights in the Website and its content (including without limitation the Website design, text, graphics and all software and source codes connected with the Website) are owned by or licensed to ArtMojo or otherwise used by ArtMojo as permitted by law.</p>
											<p>We don’t hold copyright of the images uploaded on the site, user will be solely responsible for the uploaded image, and we also do not support plagiarism.</p>
											<p>In accessing the Website you agree that you will access the content solely for your personal, non-commercial use. None of the content may be downloaded, copied, reproduced, transmitted, stored, sold or distributed without the prior written consent of the copyright holder. This excludes the downloading, copying and/or printing of pages of the Website for personal, non-commercial home use only.</p>
											<h4>PRIVACY</h4>
											<p>We view protection of your privacy as a very important and critical principle. We understand clearly that you and Your Personal Information is one of our most important assets. We store and process Your Information on computers that may be protected by physical as well as reasonable technological security measures and procedures in accordance with Information Technology Act 2000 and rules there under. Our current Privacy Policy is available at <a href="https://www.artmojo.in/privacy-policy">https://www.artmojo.in/privacy-policy</a>. If you object to your Information being transferred or used in this way please do not use artmojo.in Website.</p>
											<h4>DISCLAIMERS OF WARRANTIES AND LIMITATION OF LIABILITY</h4>
											<p>The Website is provided on an AS IS and AS AVAILABLE basis without any representation or endorsement made and without warranty of any kind whether express or implied, including but not limited to the implied warranties of satisfactory quality, fitness for a particular purpose, non-infringement, compatibility, security and accuracy.</p>
											<p>To the extent permitted by law, ArtMojo will not be liable for any indirect or consequential loss or damage whatever (including without limitation loss of business, opportunity, data, profits) arising out of or in connection with the use of the Website.</p>
											<p>ArtMojo makes no warranty that the functionality of the Website will be uninterrupted or error free, that defects will be corrected or that the Website or the server that makes it available are free of viruses or anything else which may be harmful or destructive. Nothing in these Terms of Service shall be construed so as to exclude or limit the liability of ArtMojo for death or personal injury as a result of the negligence of ArtMojo or that of its employees or agents.</p>
											<h4>TRADEMARK, COPYRIGHT AND RESTRICTION</h4>
											<p>This site is controlled and operated by artmojo.in. All material on this site, including images, illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and other intellectual property rights that are owned and controlled by us or by other parties that have licensed their material to us. Material on artmojo.in web site owned, operated, licensed or controlled by us is solely for your personal, non- commercial use. You must not copy, reproduce, republish, upload, post, transmit or distribute such material in any way, including by e-mail or other electronic means and whether directly or indirectly and you must not assist any other person to do so. Without the prior written consent of the owner, modification of the materials, use of the materials on any other web site or networked computer environment or use of the materials for any purpose other than personal, non-commercial use is a violation of the copyrights, trademarks and other proprietary rights, and is prohibited. Any use for which you receive any remuneration, whether in money or otherwise, is a commercial use for the purposes of this clause.</p>
											<h4>NO SPAM POLICY</h4>
											<p>ArtMojo may immediately terminate any account which it determines, in its sole discretion, is transmitting or is otherwise connected with any 'spam' or other unsolicited bulk email. In addition, because damages are often difficult to quantify, if actual damages cannot be reasonably calculated then you agree to pay ArtMojo liquidated damages for each time a spam incident is traced to your ArtMojo account. Otherwise you agree to pay ArtMojo's actual damages, to the extent such actual damages can be reasonably calculated.</p>
											<h4>ADVERTISEMENTS AND PROMOTIONS</h4>
											<p>ArtMojo for now will not run advertisements and promotions from third parties via the Site, Services or in any manner or mode and to any extent. Your communications, activities, relationships and business dealings with any third parties advertising or promoting via the Site, including payment and delivery of related goods or services, and any other terms, conditions, warranties or representations associated with such dealings, shall be solely matters between you and such third parties. You acknowledge and agree that ArtMojo is not responsible or liable for any loss or damage of any kind incurred as the result of any such dealings or as the result of the presence of such.</p>
											<h4>TERMINATION</h4>
											<p>ArtMojo may terminate your access to all or any part of the Website at any time, with or without cause, with or without notice, effective immediately. If you wish to terminate this Agreement or your ArtMojo account (if you have one), you may simply discontinue using the Website. Notwithstanding the foregoing, if you have a VIP Services account, such account can only be terminated by ArtMojo if you materially breach this Agreement and fail to cure such breach within thirty (30) days from ArtMojos notice to you thereof; provided that, ArtMojo can terminate the Website immediately as part of a general shut down of our service. All provisions of this Agreement which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>
											<h4>NOTICES</h4>
											<p>Any notice required to be given under this Agreement must be given in writing by email, with a hard copy of the notice delivered by personal delivery or sent by a major overnight delivery service, for delivery in either case no later than the close of the second business day following the email notice. Notices will be deemed given on the first business day after the email was sent, as determined by the States Law. Notices sent to ArtMojo must be sent to admin@artmojo.in, with the subject line containing “Legal Notice”, and with the hard copy sent to the address given on the “Contact Us” page at <a href="www.artmojo.in">www.artmojo.in</a> (or successor page or site). Notices sent to the Client must be sent to the email address provided by the Client in this Agreement or to such other email address as the Client may from time to time designate by notice, and the hard copy may be sent to the address, if any, provided by the Client for notice purposes or, if the Client has not provided such an address, to any other physical address provided by the Client to ArtMojo.</p>
											<h4>INDEMNITY</h4>
											<p>You agree to indemnify and hold ArtMojo and its employees and agents harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any claims or actions brought against ArtMojo arising out of any breach by you of these Terms of Service or other liabilities arising out of your use of this Website.</p>
											<h4>SEVERANCE</h4>
											<p>If any of these Terms of Service should be determined to be invalid, illegal or unenforceable for any reason by any court of competent jurisdiction then such Term or Condition shall be severed and the remaining Terms of Service shall survive and remain in full force and effect and continue to be binding and enforceable.</p>
											<p>The parties hereby agree that each provision herein will be treated as a separate and independent clause, and the unenforceability of any one clause will in no way impair the enforceability of any of the other clauses herein. Moreover, if one or more of the provisions contained in this Agreement is for any reason held to be excessively broad as to scope, activity, subject or otherwise unenforceable at law, such provision or provisions will be construed by the appropriate judicial body by limiting or reducing it or them, so as to be enforceable to the maximum extent compatible with applicable law.</p>
											<h4>WAIVER</h4>
											<p>If you breach these Conditions of services and we take no action, we will still be entitled to use our rights and remedies in any other situation where you breach these Conditions of Service</p>
											<h4>GOVERNING LAW</h4>
											<p>These Terms of Service shall be governed by and construed in accordance with the law of India and you hereby submit to the exclusive jurisdiction of the Courts in India.</p>
											<h4>OUR CONTACT DETAILS</h4>
											<p>Please feel free to send any questions or comments (including all inquiries unrelated to copyright infringement) regarding this Site to the support page on the website or please contact us at:</p>
											<p>ArtMojo<br>
												iVision Web Studio<br>
												<a href="www.artmojo.in">www.artmojo.in</a><br>
												<a href="mailto:admin@artmojo.in">admin@artmojo.in</a></p>
											</div>
										</div>
									</div>
								</div>
								@endsection