document.addEventListener("turbolinks:load", function() {
	var guid = $('#guid').val();
	var isPosing = false;
	$('#artDescriptionTextArea').hashtags();
	$('.comment-timestamp, .timestamp').each(function(){
		var getDate = $(this).text();
		$(this).text(moment(getDate).startOf('hour').fromNow());
	});

	$('.autoheight').on('keypress', function(){
		var el = this;
		setTimeout(function(){
			el.style.cssText = 'height:auto;';
			el.style.cssText = 'height:' + el.scrollHeight + 'px';
		},0);
	});
	if($('.streamline').length){
		var streamLineContainerOffset = $('.streamline').offset().top - 30;
		var $grid = $('.streamline').masonry({
			itemSelector: '.stream',
			gutter: 16,
			percentPosition: true
		});
		$grid.imagesLoaded().progress( function() {
			$grid.masonry('layout');
		});
		$grid.one( 'layoutComplete', function() {
			$('.stream').addClass('opacity1');
		});
		dateConvertor();
		$grid.infinitescroll({
			navSelector  : ".pagination",
			nextSelector : ".pagination li:last-child a",
			itemSelector : ".stream",
			loadingImg: "/images/loader.gif",
			bufferPx: 350,
			donetext: "I think we've hit the end, Jim" ,
		},function(new_elts) {
			var elts = $(new_elts);
			$grid.imagesLoaded(function() {
				$grid.masonry('appended', elts);
				elts.find('.timestamp').each(function(){
					var getDate = $(this).text();
					$(this).text(moment(getDate).startOf('hour').fromNow());
				});
				$('.stream').addClass('opacity1');
			})
		});
	}
	$('[data-toggle="tooltip"]').tooltip();
	if(window.location.pathname.split('/')[2] === 'following' || window.location.pathname.split('/')[2] === 'followers' || window.location.pathname.split('/')[2] === 'profile'){
		$('html, body').animate({
			scrollTop: streamLineContainerOffset
		}, 200);
	}
	checkDropZone();
	//Turbolinks.enableProgressBar();
	if(location.href.indexOf('ArtMojoCreativeQuest') > -1) {
		$('#artDescriptionTextArea').val('#ArtMojoCreativeQuest')
	}

	$('#commentArea').on('keyup',function() {
		var hasText = $('#commentArea').val();
		if(hasText != '') {
			$('#commentSubmit').attr('disabled' , false);
		} else{
			$('#commentSubmit').attr('disabled' , true);
		}
	});
});
function removeAttachedPhoto(id) {
	swal({
		title: 'Are you sure?',
		text: "You want to delete this picture!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
	}).then(function() {
		swal(
			'Deleted!',
			'Your file has been deleted.',
			'success'
			);
	});
}
function openPostCreator() {
	$('html, body').animate({
		scrollTop: 0
	}, 'fast');	
	$('.compose-wrapper').fadeToggle('fast');
}

function markAllNotificationsAsRead(){
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	 // var formData = new FormData();
  //    formData.append('notifyId', notificationId);


  $.ajax({
  	url: "/markAllNotifications",
  	type: "POST",
//        data:  formData,
contentType: false,
cache: true,
processData:false,
success: function(data){

},
error: function(){
//			$("#notificationItems").html("");

}           
});
}


function toggleNotificationCenter() {
	//alert("toggle");
	event.preventDefault();
	$('.notification-center, .notification-overlay').toggleClass('enabled');
	if($('.notification-center, .notification-overlay').hasClass('enabled') && !$(".notification-item").hasClass("hidden")){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url: "/getAllNotifications",
			type: "GET",
			contentType: false,
			cache: true,
			processData:false,
			success: function(data){
				var itemsContainer = $("#notificationItems");
				if(data != 0){
					itemsContainer.html("");
					for(var i = 0; i < data.length; i++){
						var str = !data[i].read ? '<div class="notification-item commented"> ' : '<div class="notification-item "> '; 
						itemsContainer.append(str +
							' <a class="clearfix" href="' +data[i].link + '">' +  
							' <div class="avatar pull-left">' +
							' <img src="'+ data[i].user_pic +'" height="32" width = "32" class="img-circle" alt="@akshay">'+
							'</div>' +
							'<div class="desc">' +
							'<strong>'+ data[i].user_name +'</strong> '+ data[i].text + 
							'</div>' +
							'</a>'+
							'</div>');
						//markNotificationAsRead(data[i].id);
					}
					markAllNotificationsAsRead();
					if($(".noti").hasClass("has-notifications"))
						$(".noti").removeClass("has-notifications");
						$('.noti-count').hide().text('0');
					}
				else {
					itemsContainer.html("");
				}
			},
			error: function(){
				$("#notificationItems").html("");

			}           
		});

	}
	else{

	}
}
function dateConvertor() {
	$('.timestamp').each(function(){
		var getDate = $(this).text();
		$(this).text(moment(getDate).startOf('hour').fromNow());
	});
}

function readFile() {
	if (this.files && this.files[0]) {
		var FR= new FileReader();
		FR.onload = function(e) {
			document.getElementById("img").src       = e.target.result;
			document.getElementById("b64").innerHTML = e.target.result;
		};       
		FR.readAsDataURL( this.files[0] );
	}
}
function checkDropZone() {
	if($('#artStackDropZone').length) {
		filepicker.makeDropPane($('#artStackDropZone')[0], {
			multiple: false,
			extensions: ['.jpeg', '.jpg', '.gif', '.tiff', '.bmp', '.webp', '.png', '.PNG', '.JPG', '.JPEG', '.GIF'],
			dragEnter: function() {
				$("#artStackDropZone").addClass('drag-enter');
			},
			dragLeave: function() {
				$("#artStackDropZone").removeClass('drag-enter');
			},
			onStart: function(Blobs) {
			//alert(JSON.stringify(Blobs))
		},
		onSuccess: function(Blobs) {
			var artUploaded = true;
			var imgUrl = Blobs[0].url.split('/')
			var img = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:760/"+imgUrl[3]);
			var imgLGUrl = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:2048/"+imgUrl[3]);
			var imgMDUrl = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:530/"+imgUrl[3]);
			$('#fileName').val(Blobs[0].url);
			$('.image-lg-url').val(imgLGUrl);
			$('.image-md-url').val(imgMDUrl);
			$('.attached-image').show();
			$('#uploadedArt').attr('src', img);
			$('.progress').addClass('hidden');
		},
		onError: function(type, message) {
			noty({
				layout      : "topRight",
				text        : "("+type+") "+ message,
				type        : 'error',
				theme       : 'relax',
				dismissQueue: true,
				animation   : {
					open  : 'animated bounceInRight',
					close : 'animated bounceOutRight'
				}
			});
			$('.progress').addClass('hidden');
		},
		onProgress: function(percentage) {
			$("#artStackDropZone").removeClass('drag-enter')
			$('.progress').removeClass('hidden');
			$('.progress-bar').css('width', percentage+"%").text(percentage+"%");
		}
	});
	}
}

if(window.location.pathname === '/post/art') {
	var client = filestack.init('Aymg5zYafQHmriVxQiyeKz');
	function filePick(){
		event.preventDefault();
		client.pick({
			preferLinkOverStore: true,
			maxFiles: 1,
			accept: 'image/*'
		}).then(function(result) {
			if(result.filesUploaded[0].status == "Stored" || (result.filesUploaded[0].url  || result.filesUploaded[0].url !== null || result.filesUploaded[0].url !== undefined)) 
				previewArt(result.filesUploaded[0].url);

		});
	}
}

function previewArt(fileUrl) {
	//var imgUrl = event.fpfile.url.split('/')
	var imgUrl = fileUrl.split('/');
	var img = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:760/"+imgUrl[3])
	var imgLGUrl = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:2048/"+imgUrl[3]);
	var imgMDUrl = (imgUrl[0]+"/"+imgUrl[1]+"/"+imgUrl[2]+"/resize=fit:max,width:530/"+imgUrl[3]);
	$('.image-lg-url').val(imgLGUrl);
	$('#fileName').val(fileUrl);
	$('.image-md-url').val(imgMDUrl);
	$('.attached-image').show();
	$('#uploadedArt').attr('src', img);
	$('.attached-image').imagesLoaded().progress( function( instance ) {
		$('html, body').animate({
			scrollTop: $('#artDescriptionTextArea').offset().top
		}, 200);
	})
}

$('#example').barrating();