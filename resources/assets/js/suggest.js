var suggest = false;
var suggestimg = new Image();
var wid, hei;
var canvasCTX;
var suggestionPosSelected = false;
var suggestionText, suggestionTextAdded = false;
var artId;
var boxSize = 50;
var step = 1, suggestArtEnabled = false;
var xpos, ypos;


var commentMetaHeight = $('.art-details').outerHeight() + $('.user').outerHeight();
var commentsListHeight = $(window).height() - commentMetaHeight - 38;
$('.comments-and-suggestions').height(commentsListHeight);


var artworkContainerHeight = $('.artwork-container').height();
var artworkContainerWidth = $('.artwork-container').width();
$('.artwork-container .tbl,.artwork-container .tbltr,.artwork-container .tbltd').css('height', $(window).height() - 60);
$('.artwork .overlay').css('min-height', artworkContainerHeight);
suggestImgWidth = $('.tbltd').width();
suggestImgHeight = $('.tbltd').height();
suggestImgSrc = $('#suggestImgSrcContainer').attr('data-img-src');
suggestImgSrc = suggestImgSrc.replace('1680', Math.round(suggestImgWidth));
suggestImgSrc = suggestImgSrc.replace('920', Math.round(suggestImgHeight));
$('.art').attr('src', suggestImgSrc);
//console.log($('.art').attr('src'));

$('.tbltd').imagesLoaded().progress( function( instance ) {
	linkHashTags();
	$('#myCanvas').css('display', 'block');
	$('.tbltd .loader').hide();
	var imgUrl = $('.art').attr('src');
	var artIdfromImg = $('.art').attr('data-id');
	drawOnCanvas(imgUrl, artIdfromImg);
});
function linkHashTags() {
	$(".art-description").linky({
		hashtags: true,
		urls: false,
		linkTo: "artmojo"
	});	
}
function enableModalButtons(){
	$("#modalSubmit").removeClass("disabled");
	$("#modalcancel").removeClass("disabled");
}

function disableModalButtons(){
	$("#modalSubmit").addClass("disabled");
	$("#modalCancel").addClass("disabled");
}

function addArtSuggestion(artId, x, y, text){
	disableModalButtons();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var formData = new FormData();
	formData.append('artId', artId);
	formData.append('x', x);
	formData.append('y', y);
	formData.append('text', text);

	$.ajax({
		url: "/createArtSuggestion",
		type: "POST",
		data:  formData,
		contentType: false,
		cache: true,
		processData:false,
	success: function(data){
			enableModalButtons();
			if(data == 0 || data == 2){
				showNoty('error', "Could not add the suggestion, please try again!");
				// alert("Could not add the suggestion, please try again!");
			}
			else {
				hideCommentTooltip();
				clearSuggest();
				$(data).appendTo(".suggest-list");
				$("#suggestionForArt").val("")
				$(".suggestBadge").text(parseInt($(".suggestBadge").text()) + 1);
			}
		},
		error: function(){
			showNoty('error', "Internal error occured.");
		}
	});
}

var canvas = document.getElementById("myCanvas");
canvasCTX = canvas.getContext("2d");

function clearSuggest(){
	canvasCTX.drawImage(suggestimg, 0, 0, wid, hei);
	suggestionPosSelected = false;
	suggestionTextAdded = false;
	suggestionText = null;
	xpos = null;
	ypos = null;
}


function drawOnCanvas(url, artid){
	//var c = document.getElementById("myCanvas");
	//var ctx = c.getContext("2d");
	//canvasCTX = ctx;
	var c = canvas;
	var ctx = canvasCTX;
	artId = artid;
	var img = document.getElementById("artPic");
	canvas.addEventListener('mousedown', function(evt) {
		if(suggestArtEnabled){
			if(!suggestionTextAdded)
			{
				clearSuggest();
				var mousePos = getMousePos(c, evt);
				var message = 'Mouse position: ' + mousePos.x + ',' + mousePos.y;
				ctx.beginPath();
				ctx.lineWidth="4";
				ctx.strokeStyle="black";
				ctx.rect(mousePos.x - boxSize , mousePos.y - boxSize,boxSize*2,boxSize*2);
				//console.log(mousePos.x - boxSize );
				//console.log(mousePos.y - boxSize);
				//console.log(boxSize*2);
				ctx.stroke();

				ctx.beginPath();
				ctx.lineWidth="2";
				ctx.strokeStyle="white";
				ctx.rect(mousePos.x - boxSize , mousePos.y - boxSize,boxSize*2,boxSize*2);
				ctx.stroke();
				suggestionPosSelected = true;
				xpos = mousePos.x;
				ypos = mousePos.y;
				//console.log(message);


				$('.comment-tooltip').css({'top': event.pageY - 60,'left':event.pageX + 60,'opacity':'1'});
				
				setTimeout(function() {
					alert('asd')
					$('#suggestionForArt').show().focus();
				}, 0);
				resetModal();
			}
			else{
				alert("You have selected the position and suggestion, so please clear it for adding the other suggestion");
			}
		}
		else{
			//alert("Enable for adding suggestions");	
		}

	}, false);


	suggestimg.src = url;
	suggestimg.onload = function() { 
		c.width = this.width;
		c.height=this.height;
		wid = c.width;
		hei = c.height;	    
		c.style.maxHeight = this.height.toString()+"px";
		c.style.maxWidth = this.width.toString()+"px"; 
		ctx.drawImage(suggestimg, 0, 0, c.width, c.height);
		//console.log("loadinmg 2");
	}

}

function hideCommentTooltip() {
	clearSuggest();
	$('.comment-tooltip').attr('style', '');
}

function resetModal(){
	enableModalButtons();
	$("#suggestionForArt").val("");				
	$("#suggestionForArt").focus();
}

function drawBoxStroke(x, y , size){
	var c = document.getElementById("myCanvas");
	var ctx = c.getContext("2d");
	ctx.beginPath();
	ctx.lineWidth="4";
	ctx.strokeStyle="black";
	ctx.rect(x - size , y - size, size*2, size*2);
	ctx.stroke();

	ctx.beginPath();
	ctx.lineWidth="2";
	ctx.strokeStyle="white";
	ctx.rect(x - size , y - size, size*2, size*2);
	ctx.stroke();
}


function getMousePos(can, evt) {
	var rect = can.getBoundingClientRect();
	return {
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};
}

function boxSizeChanged(size){
	boxSize = size;
}

function selectSuggestPos(){
	step = 1;
}

function provideSuggest(){
	if(suggestionPosSelected){
		$('#suggestionsModal').modal();
		suggestionText = prompt("Provide your suggestion here");
		if(suggestionText != null)	
			suggestionTextAdded = true;
	}
	else{
		alert("Select position to add suggestion");
	}
}

function addSuggest(){
	var suggestionForArt = $("#suggestionForArt").val();
	if(suggestionForArt != "")
		addArtSuggestion(artId, xpos, ypos, suggestionForArt);
	else
		alert("Provide location and suggestion for the art");	
}
function enableSuggestions(){
	suggestArtEnabled = true;
	//console.log("suggestArtEnabled : " + suggestArtEnabled);
	$('.suggestion-help').fadeIn();
	$('.enable-sgst').hide();
	$('.disable-sgst').show();
}
function disableSuggestions() {
	suggestArtEnabled =  false;
	//console.log("suggestArtEnabled : " + suggestArtEnabled);
	hideCommentTooltip();
	clearSuggest();
	$('.suggestion-help').fadeOut();
	$('.enable-sgst').show();
	$('.disable-sgst').hide();
}

function artSuggestClick(x, y){
	clearSuggest();
	drawBoxStroke(x, y, 50);
}

$("#rateYo").rateYo({
    starWidth: "24px",
    ratedFill: "#ffa000"
  });



function setArtRating(artId, dd){
//  var confirmed = confirm("Are you sure you want to delete this post?")
    setTimeout(function(){ 
	    $.ajaxSetup({
	            headers: {
	              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	          });
	    	var rating = $("#rateYo").rateYo("option", "rating");
	        var formData = new FormData();
	        formData.append('artId', artId);
	        formData.append('rating', rating);
	        $.ajax({
	            url: "/setArtRating",
	            type: "POST",
	            data:  formData,
	            contentType: false,
	            cache: true,
	            processData:false,
	            success: function(data){
	              if(data != -1 || data != -0){
	              	$("#totalRatingSpan").text(data);
	              }
	              else {

	              }
	            },
	            error: function(){
	                    	
	            }           
	        });
	   
    }, 1000);
  
}


var suggestArtId = null;
function suggestPageLoaded(){
	console.log("suggestPageLoaded");
	suggestArtId = document.URL;
    if (navigator.geolocation) 
        navigator.geolocation.getCurrentPosition(showPosition);
    else
    	alert("nope");
}

function showPosition(position) {
    var Latitude =  position.coords.latitude; 
    var Longitude =  position.coords.longitude;

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var formData = new FormData();
	formData.append('suggestArtId', suggestArtId);
	formData.append('Latitude', Latitude);
	formData.append('Longitude', Longitude);

    $.ajax({
		url: "/addArtViewLocation",
		type: "POST",
		data:  formData,
		contentType: false,
		cache: true,
		processData:false,
		success: function(data){
		//	alert(data);
			if(data == 3){
				//user mot logged im
			}
		},
		error: function(){

		}           
	});
}


suggestPageLoaded();

function getArtStat(artId){
	//alert();
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	var formData = new FormData();
	formData.append('artId', artId);

    $.ajax({
		url: "/getArtStatistics",
		type: "POST",
		data:  formData,
		contentType: false,
		cache: true,
		processData:false,
		success: function(data){
			alert(JSON.stringify(data));

			if(data == 3){
				//user mot logged im
			}
		},
		error: function(){

		}           
	});
}