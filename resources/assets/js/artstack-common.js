artUploaded = false;
var guid = undefined;
var artId = undefined;
$(document).ready(function() {
  guid = $('#guid').val();
  artId = $('#artId').val();
})
function insertNewNotificationNode(artId, avatar, notifier, text) {
  var node = '<div class="notification-item commented"><a class="clearfix" href="/suggest/'+artId+'"><div class="avatar pull-left"><img src="'+avatar+'" height="32" width="32" class="img-circle" /></div><div class="desc"><strong>'+notifier+'</strong>'+text+'</div></a></div>';
  $('#notificationItems').prepend(node)
}

function buildNotificationToast(notifierName, notifierPic, artId, userId, notificationText) {
  $('.noti').addClass('has-notifications');
  $('.toast .name').text(notifierName);
  $('.toast .notification-text').text(notificationText);
  $('.toast .art-link').attr('href', '/suggest/'+artId);
  $('.toast .profile-link').attr('href', '/profile/'+userId);
  $('.toast .avatar img').attr('src', notifierPic);  
  $('.toast').addClass('active');
  var notiCount = $('.noti-count').text();
  setTimeout(function() {
    $('.toast').removeClass('active');
  }, 5000);
  insertNewNotificationNode(artId, notifierPic, notifierName, notificationText);
  if($('.noti-count').is(":visible")) {
    $('.noti-count').text(parseInt(notiCount)+1);
  }
  else {
    $('.noti-count').text(parseInt(notiCount)+1).show();
  }
}

function notification(data) {
  var userData = data.userdata[0];
  var notifierPic = '';
  if(userData.fb_pic != null) {
    var notifierPic = userData.fb_pic; 
  } else {
    var notifierPic = userData.g_pic;
  }
  buildNotificationToast(userData.name, notifierPic, data.artId, userData.id, data.text);
}

function updateSuggestionsRealTime(data) {
  if(artId === data.artId) {
    if($('.refresh-suggestions').is(":visible") == false) {
      $('.refresh-suggestions').css('display', 'block');
    }
  }
}

function postArtNotification(data){
  $('.view-new-art').addClass('active')
}

function editArt(id){
  window.location = "/edit/art/"+id;
}

function deleteArt(artId){
  //var confirmed = confirm("Are you sure you want to delete this post?")
  var confirmed = true;
  if(confirmed){
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    var formData = new FormData();
    formData.append('artId', artId);

    $.ajax({
      url: "/deleteArt",
      type: "POST",
      data:  formData,
      contentType: false,
      cache: true,
      processData:false,
      success: function(data){
        if(data == 1){
//                alert("Art deleted succesfully");
showNoty('success', "Art deleted succesfully");
window.location = "/";
}
else {
  showNoty('error', "Art could not be deleted. Please try again");
                //alert("Art could not be deleted. Please try again!");
              }
            },
            error: function(){
              showNoty('error', "Error occured. Please try again");
            }           
          });
  }
  else{

  }
  
}



function textUserFollowClick(toId,toName){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('to', toId);
  $.ajax({
    url: "/followUser",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        $("#follow"+toId).addClass("ed").removeClass('btn-twitter');
        $("#follow"+toId).html("Following");
        showNoty('success', "You have started following "+toName);
      }
      else if(data == 2){
        $("#follow"+toId).removeClass("ed").addClass('btn-twitter');
        $("#follow"+toId).html("Follow");
        showNoty('warning', "You have unfollowed "+toName);
      }
      else{
        showNoty('error', "Error occured. Please try again");
      }

    }
    ,
    error: function(){
      showNoty('error', "Error occured. Please try again");
    }           
  });
}

function userFollowClickIcon(toId,toName){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('to', toId);
  $.ajax({
    url: "/followUser",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
        $(".follow"+toId).addClass("hidden");
        showNoty('success', "You have started following "+toName);
      }
      else if(data == 2){
        $("#follow"+toId).removeClass("ed");
        $("#follow"+toId).html("Follow");
      }
      else{
        showNoty('error', "Error occured. Please try again");
      }
    }
    ,
    error: function(){
      showNoty('error', "Error occured. Please try again");
    }           
  });

}


function suggestionApproveClick(id){
  event.preventDefault();
  count = parseInt($('#love'+id).text());
  $('#love'+id).addClass('is-animating');
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('suggestionId', id);
  $.ajax({
    url: "/suggestionApprove",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
       if(data == 1){
        $('#approveSuggest'+id).hide();
      }
      else{
        showNoty('error', "Error occured. Please try again");
      }
    },
    error: function(){
      showNoty('error', "Error occured. Please try again");
    }
  });
}


function artLikeClick(id){
  event.preventDefault();
  count = parseInt($('#love'+id).text());
  $('#love'+id).addClass('is-animating');
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('artId', id);
  $.ajax({
    url: "/likeArt",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      console.log(data);
      if(data == 2){
       console.log(data);
       $('#love'+id).removeClass('is-animating').removeClass('ed');
       $('#love'+id+' .count').text(count-1);
     }
     else if(data == 1){
      $('#love'+id).removeClass('is-animating').addClass('ed');
      $('#love'+id+' .count').text(count+1);
    }
    else{
      showNoty('error', "Error occured. Please try again");
    }
  },
  error: function(){
    showNoty('error', "Error occured. Please try again");
  }
});
}

function showNoty(typeN, textN){
  noty({
    layout      : "topRight",
    text        : textN,
    type        : typeN,
    theme       : 'metroui',
    timeout     : 4000,
    progressBar: true,
    dismissQueue: true,
    animation   : {
      open  : 'animated bounceInRight',
      close : 'animated bounceOutRight'
    }
  });
}


function commentDeleteClick(id){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  var formData = new FormData();
  formData.append('commentId', id);

  $.ajax({
    url: "/deleteArtComment",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
      if(data == 1){
       $("#commentDiv"+id).remove(); 
       $(".commentBadge").text(parseInt($(".commentBadge").text()) - 1);
     }
     else{
      showNoty('error', "Error occured. Please try again");
    }
  }
  ,
  error: function(){
    showNoty('error', "Error occured. Please try again");
  }           
});
}
document.addEventListener("turbolinks:load", function() {
 $('#likeForm').submit(function(event){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "likeArt",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
       // alert(data);
     }
     ,
     error: function(){
      showNoty('error', "Error occured. Please try again");
    }           
  });

});


 $('#commentForm').submit(function(event){
  event.preventDefault();
  $('#commentSubmit').attr('disabled' , true);
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $.ajax({
    url: "/commentArt",
    type: "POST",
    data:  new FormData(this),
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
       // alert(data);
      // $('input[type="submit"]').attr('disabled' , true);
      $(data).appendTo(".art-comments-list");
      $("#commentArea").val("").css('height','41px');
      $(".commentBadge").text(parseInt($(".commentBadge").text()) + 1);
    }
    ,
    error: function(){
      showNoty('error', "Error occured. Please try again");
      $('#commentSubmit').attr('disabled' , false);
    }           
  });
});


 $('#artForm').submit(function(event){
   event.preventDefault();
   $.ajaxSetup({
     headers: {
       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     }
   });
   if($('#fileName').val() != "" && $('#fileName').val() != undefined){
     if($('#artDescriptionTextArea').val() == ""){
      showNoty('error', "Provide description and few hashtags for your post to make it more discoverable");
    }
    else{
      $.ajax({
        url: "/createArt",
        type: "POST",
        data:  new FormData(this),
        contentType: false,
        cache: true,
        processData:false,
        success: function(data){
          if(data.indexOf("/suggest/") !== 1){
              //alert("sachie");
              $('#artDescriptionTextArea').val('').keyup();
              if($("#mode").val() == "create")
              {
                $('.attached-image').hide();
              }
              analytics.track('Art has been posted by {{Auth::User()->name}}');
              showNoty('success', "Art saved succesfully");
              window.location = data;
            }
            else if(data == 0){
              showNoty('error', "Art could not be saved succesfully");
            }
            else{
              showNoty('error', "Art could not be saved succesfully");
            }
          }
          ,
          error: function(){
            showNoty('error', "Error occured, please try again");
          }           
        });
    }
  }
  else{
    showNoty('error', "Upload your art first");
  }
});
});


function upvoteSuggestionClick(suggestId){
  event.preventDefault();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  var formData = new FormData();
  formData.append('suggestId', suggestId);

    $.ajax({
    url: "/upvoteSuggestion",
    type: "POST",
    data:  formData,
    contentType: false,
    cache: true,
    processData:false,
    success: function(data){
     // alert(data);

      if(data == 1){
        $("#upvote" + suggestId).hide();
        $("#upvoteSpan" + suggestId).show().addClass('is-upvoted');
        $("#upvoteSpanCount" + suggestId).html(parseInt($("#upvoteSpanCount" + suggestId).html())+1);
      }
      else if(data == -1){
        //user mot logged im
      }
      else if(data == 2){
        //user mot logged im
      }
      else{

      }
    },
    error: function(){

    }           
  }); 
}
